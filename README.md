# README #

## What is this repository for? ##

* Qmake QPackage QManager (name it "Cube") is a c++ dependancy manager build upon **qmake** tool.
* ~~It is the missing  Qt tool.~~

* QQQ provide a workflow and conventions to create subdir project, create library and apps

* QQQ command line is largely inspired from npm

* Version: pre alpha ; work in progress

* resources for contributors: Software design [wiki](https://bitbucket.org/dallemant/qqq/wiki/Development)


## How do I get set up? ##

* Summary of set up
* * build with qt5
* * tested with QT5.2.1 and Qt5.6.1 on windows mingw

```
#!bash

qmake && make
```

* How to run tests

```
#!bash

make check
```

* Deployment instructions

```
#!bash

make install
```

* Run:

```
#!bash

cd /path/to/Run
. env.conf
# [show available commands]
qqq 

# [start demo script]
./1_build_backend.sh

qqq init my_workspace --workspace
cd my_workspace
#u can use local backend
touch qqq_backend.ini


qqq install libA --save

#done, u can create awesome app , based on libA and all its dependancies



```
## Contribution guidelines ##

* Writing tests
** add Qt unit test in tests directory
* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)