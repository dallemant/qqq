TEMPLATE = lib
CONFIG += static
TARGET = qqq


QT -= gui
CONFIG +=console
QT += network #HttpBackend

CONFIG += c++11
INCLUDEPATH += src

DESTDIR = $$BUILD_DIR/bin

OTHER_FILES += features/*
CONFIG(coverage_test) {
CONFIG+=gcov
}

SOURCES += \
  src/qqq/TAppArguments.cpp \
  src/qqq/TConsole.cpp \
  src/qqq/TCommand.cpp \
  src/qqq/TConfig.cpp \
  src/qqq/TCommandLineDeclaration.cpp \
  src/qqq/Commands/TInit.cpp \
  src/qqq/Commands/TInstall.cpp \
  src/qqq/Package/TPackage.cpp \
  src/qqq/Package/TPackageJson.cpp \
  src/qqq/Package/TLocalTree.cpp \
  src/qqq/Api/Exceptions.cpp \
  src/qqq/Api/IInstall.cpp \
  src/qqq/Api/IWorkspace.cpp \
  src/qqq/Api/IBackEnd.cpp \
  src/qqq/Subversion/TSvnInstaller.cpp \
  src/qqq/Qmake/TQmakeInstaller.cpp \
  src/qqq/Qmake/TQmakeWorkspace.cpp \
  src/qqq/Commands/TBackEnd.cpp \
  src/qqq/Package/TSemanticVersion.cpp \
    src/qqq/Local/TLocalInstaller.cpp \
    src/qqq/Qmake/TQmakeHelper.cpp \
    src/qqq/Subversion/svnwrapper.cpp \
    src/qqq/Commands/TSet.cpp \
    src/qqq/Api/installprotocolselector.cpp \
    src/qqq/Package/TUrl.cpp \
    src/qqq/Package/private/TSemanticeVersionIntersection.cpp \
    src/qqq/Package/private/TSemanticVersionChecker.cpp \
    src/qqq/HttpBackend/HttpBackend.cpp \
    src/qqq/Git/GitInstaller.cpp \
    src/qqq/Git/GitWrapper.cpp \
    src/qqq/Subversion/ExternalDecoder.cpp \
    src/qqq/Commands/LegacyWorkspaceMigration.cpp \
    src/qqq/HttpBackend/HttpBackendProtocol.cpp

HEADERS += \
    src/qqq/TAppArguments.h \
    src/qqq/TConsole.h \
    src/qqq/TCommand.h \
    src/qqq/Commands/TInit.h \
    src/qqq/TConfig.h \
    src/qqq/TCommandLineDeclaration.h \
    src/qqq/Package/TPackage.h \
    src/qqq/Package/TPackageJson.h \
    src/qqq/Commands/TInstall.h \
    src/qqq/Package/TLocalTree.h \
    src/qqq/Api/Exceptions.h \
    src/qqq/Api/IInstall.h \
    src/qqq/Api/IWorkspace.h \
    src/qqq/Api/IBackEnd.h \
    src/qqq/Subversion/TSvnInstaller.h \
    src/qqq/Qmake/TQmakeInstaller.h \
    src/qqq/Qmake/TQmakeWorkspace.h \
    src/qqq/Commands/TBackEnd.h \
    src/qqq/Package/TSemanticVersion.h \
    src/qqq/Local/TLocalInstaller.h \    
    src/qqq/Qmake/TQmakeHelper.h \
    src/qqq/Subversion/svnwrapper.h \
    src/qqq/Commands/TSet.h \
    src/qqq/Api/installprotocolselector.h \
    src/qqq/Package/TUrl.h \
    src/qqq/Package/private/semver/version2.h \
    src/qqq/Package/private/TSemanticeVersionIntersection.h \
    src/qqq/Package/private/TSemanticVersionChecker.h \
    src/qqq/HttpBackend/HttpBackend.h \
    src/qqq/Git/GitInstaller.h \
    src/qqq/Git/GitWrapper.h \
    src/qqq/Api/IDirRepositoryUrlFinder.h \
    src/qqq/Subversion/ExternalDecoder.h \
    src/qqq/Commands/LegacyWorkspaceMigration.h \
    src/qqq/HttpBackend/HttpBackendProtocol.h

RESOURCES += \
    resources/resources.qrc


