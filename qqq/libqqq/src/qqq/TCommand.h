#ifndef TCOMMAND_H
#define TCOMMAND_H

#include <functional>
#include "TConfig.h"
#include "TConsole.h"
#include "TCommandLineDeclaration.h"

///
/// \brief The TCommand interface with command line
///
class TCommand {
   public:

    ///
    /// \brief GetCommandName first positionnal argument
    virtual QString GetCommandName() const = 0;

   /// \brief Prepare : define options and positionnal arguments
    /// \param parser_p
    ///
    virtual void Prepare(TCommandLineDeclaration* parser_p) = 0;

    /// \brief Process
    /// \param config_p : access options and pos arguments
    /// \param console_p , allow provide information for user
    virtual bool Process(const TConfig& config_p, TConsole& console_p) = 0;
};

#include <functional>


///
/// \brief The TCommandHelper class; use it if you prefer functionnal over inheritance
///
class TCommandHelper : public TCommand {
    // TCommand interface
   public:
    TCommandHelper(QString name_p,
                   std::function<void(TCommandLineDeclaration*)> prepare_func_p,
                   std::function<bool(const TConfig&, TConsole&)> process_func_p);

    QString GetCommandName() const override;
    void Prepare(TCommandLineDeclaration* parser_p) override;
    bool Process(const TConfig& config_p, TConsole& console_p) override;

   private:
    QString m_name;
    std::function<void(TCommandLineDeclaration*)> m_prepare_func;
    std::function<bool(const TConfig&, TConsole& console_p)> m_process_func;
};

#endif  // TCOMMAND_H
