#ifndef TCONFIG_H
#define TCONFIG_H

#include <QStringList>
///
/// \brief The TConfig class :  QCommandLineParser getter functions wrapper
///
class TConfig {
   public:
    virtual QStringList GetPositionnalArguments() const = 0;
    virtual bool isSet(const QString& name) const = 0;
    virtual QStringList optionNames() const = 0;
    virtual QString value(const QString& optionName) const = 0;
    virtual QStringList values(const QString& optionName) const = 0;
};

#endif  // TCONFIG_H
