#include "TCommand.h"
#include "TConsole.h"
#include "TCommandLineDeclaration.h"
#include "TConfig.h"
#include "TAppArguments.h"

///
/// \brief The Config class :  QCommandLineParser getter functions wrapper
///
class Config : public TConfig {
    const QCommandLineParser& m_parser;

   public:
    Config(const QCommandLineParser& parser_p) : m_parser(parser_p) {}

    // TConfig interface
   public:
    QStringList GetPositionnalArguments() const override { return m_parser.positionalArguments(); }
    bool isSet(const QString& name) const override { return m_parser.isSet(name); }
    QStringList optionNames() const override { return m_parser.optionNames(); }
    QString value(const QString& optionName) const override { return m_parser.value(optionName); }
    QStringList values(const QString& optionName) const override { return m_parser.values(optionName); }
};

///
/// \brief The CLDeclaration class :  QCommandLineParser setter functions wrapper
///
class CLDeclaration : public TCommandLineDeclaration {
    QCommandLineParser* m_parser;

   public:
    CLDeclaration(QCommandLineParser* parser_p) : m_parser(parser_p) {}

    bool AddOption(const QString& name,
                   const QString& description,
                   const QString& valueName = QString(),
                   const QString& defaultValue = QString()) override {
        return m_parser->addOption(QCommandLineOption{name, description, valueName, defaultValue});
    }

    bool AddOption(const QStringList& names,
                   const QString& description,
                   const QString& valueName = QString(),
                   const QString& defaultValue = QString()) override {
        return m_parser->addOption(QCommandLineOption{names, description, valueName, defaultValue});
    }

    void AddPositionalArgument(const QString& name,
                               const QString& description,
                               const QString& syntax = QString()) override {
        m_parser->addPositionalArgument(name, description, syntax);
    }
};



TAppArguments::TAppArguments() : m_help(m_parser.addHelpOption()), m_version(m_parser.addVersionOption()) {
    m_parser.setApplicationDescription(
        QCoreApplication::translate("TAppArguments", "npm like tool for qmake workspace"));
}

void TAppArguments::RegisterCommand(TCommand* command_p) {
    Q_ASSERT(command_p);
    m_commands.push_back(command_p);
}

void TAppArguments::RegisterCommand(const std::vector<TCommand*>& command_p) {
    for (auto cmd : command_p) {
        m_commands.push_back(cmd);
    }
}

bool TAppArguments::Process(QStringList arguments_p, TConsole* console_p) {
    m_parser.addPositionalArgument("command", QString("The command to execute: [%1]").arg(Commands().join(" ")));

    // Call parse() to find out the positional arguments.
    m_parser.parse(arguments_p);

    const QStringList args = m_parser.positionalArguments();
    if (args.isEmpty()) {
        console_p->Msg() << "available commands are:";
        console_p->Msg() << Commands().join("\n\t");
        return true;
    }

    // consomme la commande
    m_parser.clearPositionalArguments();

    const QString command = args.first();
    for (TCommand* cmd : m_commands) {
        if (cmd->GetCommandName() == command) {
            CLDeclaration declaration{&m_parser};
            cmd->Prepare(&declaration);

            if (!m_parser.parse(arguments_p)) {
                console_p->Msg() << m_parser.errorText();
                return false;
            }

            if (IsBuiltinOptionSet(console_p)) {
                return true;
            }

            return cmd->Process(Config{m_parser}, *console_p);
        }
    }

    console_p->Msg() << QString("command '%1' is unknow").arg(command);
    return false;
}

QStringList TAppArguments::Commands() const {
    QStringList cmds;
    for (TCommand* cmd : m_commands) {
        cmds.push_back(cmd->GetCommandName());
    }
    return cmds;
}

bool TAppArguments::IsBuiltinOptionSet(TConsole* console_p) {
    if (m_parser.isSet(m_version)) {
        console_p->Msg() << QCoreApplication::applicationVersion();
        return true;
    }

    if (m_parser.isSet(m_help)) {
        QString help_text = m_parser.helpText();
        console_p->Msg() << help_text;
        return true;
    }
    return false;
}
