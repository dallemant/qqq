#ifndef TCONSOLE_H
#define TCONSOLE_H

#include <QTextStream>
#include <QString>

///
/// \brief The TConsole class I/O message with user
///
///
class TConsole {
   public:
    virtual ~TConsole(){}
    virtual QTextStream& Msg() = 0;
    virtual QTextStream& Debug() = 0;
    virtual QString Query(QString str_p) = 0;
};

class TConsoleQt : public TConsole {
    // TConsole interface
   public:
    TConsoleQt();
    ~TConsoleQt();
    TConsoleQt(QString& str_p);

    QTextStream& Msg() override;
    QTextStream& Debug() override;
    QString Query(QString str_p) override;

   private:
    QTextStream m_stream;
};

#endif  // TCONSOLE_H
