#include <QDebug>
#include "qqq/Api/Exceptions.h"
#include "TConsole.h"


TConsoleQt::TConsoleQt()
    :m_stream(stdout)
{

}

TConsoleQt::~TConsoleQt()
{
     m_stream <<endl;
}

TConsoleQt::TConsoleQt(QString &str_p)
    :m_stream(&str_p)
{

}

QTextStream &TConsoleQt::Msg()
{
    m_stream <<endl;
    return m_stream;
}

QTextStream &TConsoleQt::Debug()
{
    m_stream <<endl;
    return m_stream;
}

QString TConsoleQt::Query(QString str_p)
{
    Q_UNUSED(str_p);
    QQQ_THROW_NOT_IMPLEMENTED_FUNCTION
}
