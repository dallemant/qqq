#ifndef IINIT_H
#define IINIT_H
#include <memory>
#include <QTemporaryDir>

///
/// \brief The IWorkspace class, create necessary files for top level directory of he workspace
///
///
class IWorkspace {
   public:
    IWorkspace() = delete;
    explicit IWorkspace(QDir current_dir_p) : m_current_dir(current_dir_p) {}
    virtual ~IWorkspace() {}

    enum eOption { eCreateDirectory, eCurrentDirectory };

    virtual void CreateWorkspace(const QString& workspace_name_p, eOption opt_p) = 0;
    virtual void CreateApplication(const QString& workspace_name_p, eOption opt_p) = 0;
    virtual void CreateLibrary(const QString& workspace_name_p, eOption opt_p) = 0;

   protected:
    QDir m_current_dir;
};

class IWorkspaceCreator {
   public:
    virtual std::unique_ptr<IWorkspace> CreateWorkspaceTransaction(QDir current_dir_p) = 0;
};

#endif  // IINIT_H
