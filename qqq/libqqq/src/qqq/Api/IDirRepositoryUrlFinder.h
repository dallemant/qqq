#ifndef IDIRREPOSITORYURLFINDER_H
#define IDIRREPOSITORYURLFINDER_H

#include <initializer_list>
#include <QDir>
#include "qqq/Package/TUrl.h"

class IDirectoryRepositoryUrlFinder{

public:
    virtual Url GetRepositoryUrl(QDir dir)const=0;

};

class DirectoryRepositoryFinder: public IDirectoryRepositoryUrlFinder{
    std::vector<IDirectoryRepositoryUrlFinder*> finders;
public:
    DirectoryRepositoryFinder(std::initializer_list<IDirectoryRepositoryUrlFinder*> init_list)
    {
        for(auto finder:init_list){
            finders.push_back(finder);
        }
    }

    // IDirecoryRepositoryUrlFinder interface
public:
    Url GetRepositoryUrl(QDir dir) const override
    {
        Url url;
        for(auto finder:finders){
            try {
                url = finder->GetRepositoryUrl(dir);
                break;
            } catch (...) {
                continue;
            }
        }
        return  url;

    }
};

#endif // IDIRREPOSITORYURLFINDER_H


