#include "IBackEnd.h"
#include "installprotocolselector.h"

TInstallerProtocolSelector::TInstallerProtocolSelector(std::unique_ptr<IInstaller> next_p, const IBackEnd *backend_p)
    :IInstallerDecorator(std::move(next_p))
    ,m_backend(backend_p)
{

}

void TInstallerProtocolSelector::AddProtocol(std::unique_ptr<IInstallerProtocol> installer_p)
{
    m_installers.push_back(std::move(installer_p));
}


void TInstallerProtocolSelector::DoFetch(const TDepend &dep_p)
{
    Url url = m_backend->Resolve(dep_p);

    if(!url.url.isValid()){
        throw std::runtime_error(QString("IInstallerProtocolSelector Fetch: dependancy %1_%2 is not registered in backend").arg(dep_p.name,dep_p.version).toStdString());
    }

    for(auto& installer:m_installers){
        if(installer->CanHandle(url.protocol)){
            installer->Fetch(dep_p,url.url);
        }
    }
}

void TInstallerProtocolSelector::DoRemove(const TDepend &dep_p)
{
    Url url = m_backend->Resolve(dep_p);

    if(!url.url.isValid()){
        throw std::runtime_error(QString("IInstallerProtocolSelector Remove: dependancy %1_%2 is not registered in backend").arg(dep_p.name,dep_p.version).toStdString());
    }

    for(auto& installer:m_installers){
        if(installer->CanHandle(url.protocol)){
            installer->Remove(dep_p);
        }
    }

}

void TInstallerProtocolSelector::DoReplace(const TDepend &old_p, const TDepend &new_p)
{
    Url url = m_backend->Resolve(new_p);

    if(!url.url.isValid()){
        throw std::runtime_error(QString("IInstallerProtocolSelector DoReplace: dependancy %1_%2 is not registered in backend").arg(new_p.name,new_p.version).toStdString());
    }

    for(auto& installer:m_installers){
        if(installer->CanHandle(url.protocol)){
            installer->Replace(old_p,new_p,url.url);
        }
    }
}

void IInstallerProtocol::Replace(const TDepend &old_p, const TDepend &new_p, const QUrl &url_p)
{
    Remove(old_p);
    Fetch(new_p,url_p);
}
