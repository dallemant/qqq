#ifndef IINSTALL_H
#define IINSTALL_H
#include <memory>
#include <QTemporaryDir>

#include "qqq/Package/TPackage.h"


///
/// \brief The IInstaller class : abstract interface to Fetch, remove and replace dependancy
///
class IInstaller {
   public:
    IInstaller() = default;
    virtual ~IInstaller() {}

    virtual void Fetch(const TDepend& dep_p) = 0;
    virtual void Remove(const TDepend& dep_p) = 0;
    virtual void Replace(const TDepend& old_p,const TDepend& new_p)=0;

};

///
/// \brief The IInstallerDecorator class, chain action on dependancy
/// Fetch usage:
/// 1. is Fetch Files with concrete IInstaller
/// 2. Apply change on build files (if qmake workspace, update .pro file)
///
class IInstallerDecorator : public IInstaller {
   public:
    explicit IInstallerDecorator(std::unique_ptr<IInstaller> installer_p);

    virtual ~IInstallerDecorator() {}

    ///
    /// \brief Fetch implements decorator pattern
    virtual void Fetch(const TDepend& dep_p) override final;
    ///
    /// \brief Remove implements decorator pattern
    virtual void Remove(const TDepend& dep_p) override final;
    ///
    /// \brief Replace implements decorator pattern
    virtual void Replace(const TDepend& old_p,const TDepend& new_p) override final;


    ///
    /// \brief DoFetch : concrete action
    /// \param dep_p
    ///
    virtual void DoFetch(const TDepend& dep_p) = 0;
    virtual void DoRemove(const TDepend& dep_p) = 0;
    virtual void DoReplace(const TDepend& old_p,const TDepend& new_p);


   protected:
    ///
    /// \brief m_installer: next action
    ///
    std::unique_ptr<IInstaller> m_installer;
};

class IInstallerCreator {
   public:
    virtual ~IInstallerCreator(){}
    virtual std::unique_ptr<IInstaller> CreateInstallerTransaction() = 0;
};

#endif  // IINSTALL_H
