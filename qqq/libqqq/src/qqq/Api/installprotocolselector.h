#ifndef INSTALLPROTOCOLSELECTOR_H
#define INSTALLPROTOCOLSELECTOR_H

#include "qqq/Api/IInstall.h"
#include "qqq/Package/TUrl.h"

class IBackEnd;
class IInstallerProtocol{
public:

    virtual bool CanHandle(Url::Protocol protocole_p)const=0;

    // IInstallerDecorator interface
public:
    virtual void Fetch(const TDepend &dep_p, const QUrl& url_p)=0;
    virtual void Remove(const TDepend &dep_p)=0;
    virtual void Replace(const TDepend &old_p, const TDepend &new_p, const QUrl& url_p);
};



class TInstallerProtocolSelector: public IInstallerDecorator{

public:
    TInstallerProtocolSelector(std::unique_ptr<IInstaller> next_p, const IBackEnd* backend_p);

    void AddProtocol(std::unique_ptr<IInstallerProtocol> installer_p);

    // IInstallerDecorator interface
public:
    void DoFetch(const TDepend &dep_p);
    void DoRemove(const TDepend &dep_p);
    void DoReplace(const TDepend &old_p, const TDepend &new_p);

private:
    std::vector<std::unique_ptr<IInstallerProtocol>> m_installers;

    const IBackEnd* m_backend;
};

#endif // INSTALLPROTOCOLSELECTOR_H
