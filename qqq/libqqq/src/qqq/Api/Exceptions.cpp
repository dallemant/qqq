#include "Exceptions.h"
#include <string>

static const std::string dne= "directory not empty";
static const std::string dae= "directory already exists";
static const std::string nif= "not implemented function";

QqqException::QqqException(QString msg_p)
    :msg(msg_p)
{

}
const char *QqqException::what() const throw ()
{
    return qPrintable(msg);
}

const char *DirectoryNotEmpty::what() const throw (){
    return dne.data();
}

const char *DirectoryAlreadyExist::what() const throw ()
{
    return dae.data();
}


FileNotFound::FileNotFound(QString where, QString file_p)
    :m_file()
{
    m_file = QString("%1 : File not found : %2")
            .arg(where)
            .arg(file_p);
}

const char *FileNotFound::what() const throw ()
{
    return qPrintable(m_file);
}

QString NotImplementedFunction::What(std::string func, std::string file, int line) const
{
    return QString("Not implemented function: %1 file:%2 line:%3")
            .arg(func.data())
            .arg(file.data())
            .arg(line);
}

NotImplementedFunction::NotImplementedFunction(std::string func,std::string file, int line)
    :QqqException (What(func,file,line))
{

}
