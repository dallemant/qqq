#include "IInstall.h"

IInstallerDecorator::IInstallerDecorator(std::unique_ptr<IInstaller> installer_p)
    : m_installer(std::move(installer_p)) {}

void IInstallerDecorator::Fetch(const TDepend& dep_p) {
    DoFetch(dep_p);
    if (m_installer)
        m_installer->Fetch(dep_p);
}

void IInstallerDecorator::Remove(const TDepend& dep_p) {
    DoRemove(dep_p);
    if (m_installer)
        m_installer->Remove(dep_p);
}

void IInstallerDecorator::Replace(const TDepend &old_p, const TDepend &new_p)
{
    DoReplace(old_p,new_p);
    if (m_installer)
        m_installer->Replace(old_p,new_p);
}

void IInstallerDecorator::DoReplace(const TDepend &old_p, const TDepend &new_p)
{
    Q_ASSERT(old_p.name == new_p.name);
    Remove(old_p);
    Fetch(new_p);
}

