#include <set>
#include <QDebug>
#include "qqq/Api/Exceptions.h"
#include "qqq/Package/TSemanticVersion.h"

#include "IBackEnd.h"

const QString URL_GROUP("URL");

QSettingsBackend::QSettingsBackend(QSettings* settings_p) : m_settings(settings_p) {}

Url QSettingsBackend::Resolve(const TDepend& dep_p) const {
  Url ret;

  m_settings->beginGroup(URL_GROUP);
  m_settings->beginGroup(dep_p.name);
  m_settings->beginGroup(dep_p.version);

  ret.url = m_settings->value("url").toUrl();
  ret.protocol = static_cast<Url::Protocol>(m_settings->value("protocol").toInt());

  m_settings->endGroup();
  m_settings->endGroup();
  m_settings->endGroup();
  return ret;
}

std::vector<TDepend> QSettingsBackend::AvailableVersionFor(QString wildcard) const {
  std::vector<TDepend> ret;
  QSettings* set = m_settings;

  QRegExp rx(wildcard);
  rx.setPatternSyntax(QRegExp::Wildcard);

  set->beginGroup(URL_GROUP);
  Q_FOREACH (QString composant_name, set->childGroups()) {
      if (rx.exactMatch(composant_name)) {
          set->beginGroup(composant_name);
          Q_FOREACH (QString version, set->childGroups()) {
            ret.push_back(TDepend{composant_name,TVersion(version)});
          }
          set->endGroup();
      }
  }

  set->endGroup();
  return ret;
}

void QSettingsBackend::Register(const TDepend& dep_p, const Url& url_p) {
  QSettings* set = m_settings;
  set->beginGroup(URL_GROUP);
  set->beginGroup(dep_p.name);
  set->beginGroup(dep_p.version);
  set->setValue("url", url_p.url);
  set->setValue("protocol", url_p.protocol);
  set->endGroup();
  set->endGroup();
  set->endGroup();

  if(!IsInDictionnary(dep_p)){
      Register( TPackage{dep_p,{}}  );
  }
}

void QSettingsBackend::Register(const TPackage& package_p) {
  m_settings->beginGroup("dictionnary");
  m_settings->beginGroup(package_p.name);
  m_settings->beginGroup(package_p.version);

  m_settings->remove("");

  m_settings->beginWriteArray("deps", static_cast<int>(package_p.depends.size()));
  for (size_t i = 0; i < package_p.depends.size(); ++i) {
    m_settings->setArrayIndex(static_cast<int>(i));
    m_settings->setValue("name", package_p.depends.at(i).name);
    m_settings->setValue("version", package_p.depends.at(i).version);
  }
  m_settings->endArray();

  m_settings->endGroup();
  m_settings->endGroup();
  m_settings->endGroup();
}

std::pair<bool, TPackage> QSettingsBackend::Package(const TDepend &depend_p) const
{

    m_settings->beginGroup("dictionnary");
    if( ! m_settings->childGroups().contains(depend_p.name)){
        m_settings->endGroup();
        return std::make_pair(false,TPackage());
    }
    m_settings->beginGroup(depend_p.name);

    if( ! m_settings->childGroups().contains(depend_p.version)){
        m_settings->endGroup();
        m_settings->endGroup();
        return std::make_pair(false,TPackage());
    }
    m_settings->beginGroup(depend_p.version);

    TPackage p;
    p.name = depend_p.name;
    p.version = depend_p.version;

    int size = m_settings->beginReadArray("deps");
    for (int i = 0; i < size; ++i) {
        m_settings->setArrayIndex(i);
        TDepend dep;
        dep.name = m_settings->value("name").toString();
        dep.version = m_settings->value("version").toString();
        p.depends.push_back(dep);
    }
    m_settings->endArray();

    m_settings->endGroup();
    m_settings->endGroup();
    m_settings->endGroup();

    return std::make_pair(true,p);
}

bool QSettingsBackend::IsInDictionnary(const TDepend &dep) const
{
    return m_settings->contains(QStringList{"dictionnary",dep.name,dep.version}.join("/"));
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief IBackendImplementedResolver::AddPackageDependencies
/// \param dep_p
/// \param error_p
/// \param deps_p
///
void IBackendImplementedResolver::AddPackageDependencies(const TDepend& dep_p,
                                                         QString* error_p,
                                                         std::vector<TDepend>& deps_p) const {

  // qDebug()<<"AddPackageDependencies "<< dep_p.name<<" "<< dep_p.version;

  const auto version_type = ISemanticVersion::Type(dep_p.version);
  bool is_fixed_version =
          (version_type == ISemanticVersion::Pure) or
          (version_type == ISemanticVersion::String) or
          (version_type == ISemanticVersion::Latest);

  //only fixed version can be checked for its dependancies
  if (is_fixed_version) {

    // find package
    auto ret = Package(dep_p);

    if (ret.first == false) {
      if (error_p)
        *error_p += QString("%1_%2 not found, you have to register package first").arg(dep_p.name).arg(dep_p.version);
      return;
    }
    TPackage p = ret.second;
    Q_ASSERT((p.name == dep_p.name) && (p.version == dep_p.version));

    // add package dependencies
    deps_p.insert(deps_p.end(), p.depends.begin(), p.depends.end());

    // for debug
    //    for (const auto& dep : p.depends) {
    //        qDebug()<<"AddPackageDependencies dep:"<< dep.name<<"/"<< dep.version;
    //    }

    for (const auto& dep : p.depends) {
      AddPackageDependencies(dep, error_p, deps_p);
    }
  }else{
      //("dependencie resolver: resolve range version not implemented");
      QQQ_THROW_NOT_IMPLEMENTED_FUNCTION
  }
}

void IBackendImplementedResolver::ReduceDependencies(std::vector<TDepend>& deps_p, QString* error_p) const {
  std::vector<TDepend> ret;

  // remove duplicates
  std::set<TDepend> unique_depends;
  for (auto& dep : deps_p) {
    if (unique_depends.insert(dep).second == true) {
      ret.push_back(dep);
    }
  }

  // search for version conflicts
  bool conflicts = false;

  std::set<QString> unique_names;
  for (auto& dep : ret) {
    unique_names.insert(dep.name);
  }

  if (unique_names.size() != ret.size()) {
    //  conflict founds
    conflicts = true;
    if (error_p) {
      *error_p += QChar('\n') + QString("there is conflict(s) detected ");

      // build explicit message
      std::multimap<QString, TDepend> mmap;
      for (auto& dep : ret) {
        mmap.insert(std::make_pair(dep.name, dep));
      }
      for (const QString& name : unique_names) {
        if (mmap.count(name) > 1) {
          *error_p += QChar('\n') + QString("%1 :").arg(name);
          auto its = mmap.equal_range(name);
          for (auto it = its.first; it != its.second; ++it) {
            *error_p += QString(" %1 ").arg(it->second.version);
          }
        }
      }
    }
  }

  if (conflicts) {
    ret.clear();
  }

  deps_p = ret;
}

std::vector<TDepend> IBackendImplementedResolver::ResolveDependencies(const TDepend& dep_p, QString* error_p) const {
  std::vector<TDepend> ret;

  // qDebug()<<"ResolveDependencies "<< dep_p.name<<" "<< dep_p.version;

  // recursively add dependencies
  AddPackageDependencies(dep_p, error_p, ret);

  // clean redundant deps /check for conflicts
  ReduceDependencies(ret, error_p);

  return ret;
}

std::vector<TDepend> IBackendImplementedResolver::ResolveDependencies(const TPackage &package_p, QString *error_p) const
{
    std::vector<TDepend> ret;
    for (const auto& dep : package_p.depends) {
        ret.push_back(dep);
        // recursively add dependencies
        AddPackageDependencies(dep, error_p, ret);
    }

    // clean redundant deps /check for conflicts
    ReduceDependencies(ret, error_p);

    return  ret;
}

///@todo rename me Register if u can
void IBackendImplementedResolver::RegisterOnce(const TPackage &package_p, const Url &url_p)
{
    TDepend dep{package_p.name,package_p.version};
    Register(dep,url_p);
    Register(package_p);
}


