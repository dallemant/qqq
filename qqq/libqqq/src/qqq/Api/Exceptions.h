#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H
#include <exception>
#include <QString>

///
/// Gather Common exception of libQQQ
///

class QqqException:public std::exception{
    const QString msg;
    // exception interface
public:
    QqqException(QString msg_p);
    virtual ~QqqException()throw(){}
    const char *what() const throw () override;
};


class DirectoryNotEmpty:public std::exception{
    // exception interface
public:
    virtual ~DirectoryNotEmpty()throw(){}
    const char *what() const throw () override;
};

class DirectoryAlreadyExist:public std::exception{
    // exception interface
public:
    virtual ~DirectoryAlreadyExist()throw(){}
    const char *what() const throw () override;
};

class FileNotFound:public std::exception{
    QString m_file;
    // exception interface
public:
    FileNotFound(QString where, QString file_p);
    virtual ~FileNotFound()throw(){}
    const char *what() const throw () override;

};

class NotImplementedFunction:public QqqException{
    // exception interface
    QString What(std::string func,std::string file, int line)const;
public:
    NotImplementedFunction(std::string func,std::string file, int line);
    virtual ~NotImplementedFunction()throw(){}

};

#define QQQ_THROW_NOT_IMPLEMENTED_FUNCTION throw NotImplementedFunction(Q_FUNC_INFO,__FILE__, __LINE__);
#endif // EXCEPTIONS_H
