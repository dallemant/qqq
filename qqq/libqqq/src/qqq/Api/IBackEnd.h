#ifndef IBACKEND_H
#define IBACKEND_H
#include <memory>
#include <QSettings>
#include <QUrl>
#include "qqq/Package/TUrl.h"
#include "qqq/Package/TPackage.h"



///
/// \brief The IBackEnd class interact with backend
/// backend is responsible for :
/// - yellow page depends (get url to depend)
/// - build dependancy tree
///
class IBackEnd
{
public:
  IBackEnd() = default;
  virtual ~IBackEnd() = default;
  /// @brief associe un composant à son emplacement
  virtual void Register(const TDepend& dep_p, const Url& url_p) = 0;
  /// @brief ajoute la description d'un composant
  virtual void RegisterOnce(const TPackage& package_p, const Url& url_p) = 0;

  virtual void Register(const TPackage& package_p) = 0;

  virtual Url Resolve(const TDepend& package_p) const = 0;

  virtual std::pair<bool,TPackage> Package(const TDepend& package_p)const=0;

  virtual std::vector<TDepend> ResolveDependencies(const TDepend& package_p, QString* error_p) const = 0;
  virtual std::vector<TDepend> ResolveDependencies(const TPackage& package_p, QString* error_p) const = 0;

  virtual std::vector<TDepend>  AvailableVersionFor(QString name_p) const = 0;
};

///
/// \brief The IBackendImplementedResolver class implements Dependancy tree algorithm
///
class IBackendImplementedResolver : public IBackEnd
{
public:
  virtual ~IBackendImplementedResolver() = default;
  virtual std::vector<TDepend> ResolveDependencies(const TDepend& package_p, QString* error_p) const override;
  virtual std::vector<TDepend> ResolveDependencies(const TPackage& package_p, QString* error_p) const override;
  virtual void RegisterOnce(const TPackage &package_p, const Url &url_p) override;

private:
  void AddPackageDependencies(const TDepend& dep_p, QString* error_p, std::vector<TDepend>& deps_p) const;
  void ReduceDependencies(std::vector<TDepend>& deps_p, QString* error_p) const;


};

///
/// \brief The QSettingsBackend class: local implemnenation
/// Persitence through QSettings
///
class QSettingsBackend : public IBackendImplementedResolver
{
public:
  QSettingsBackend(QSettings* settings_p);
  virtual ~QSettingsBackend() {}
  void Register(const TPackage& package_p) override;
  void Register(const TDepend& dep_p, const Url& url_p) override;

  Url Resolve(const TDepend& package_p) const override;

  std::pair<bool,TPackage> Package(const TDepend &depend_p) const override;
  std::vector<TDepend>  AvailableVersionFor(QString wildcard) const override;



private:
  QSettings* m_settings;

  bool IsInDictionnary(const TDepend& dep)const;

};

///
/// \brief The InMemoryBackend class :  Unit test usage
///
class InMemoryBackend : public IBackendImplementedResolver
{
  std::multimap<QString, TPackage> m_dictionnary;
  std::map<TDepend, Url> m_urls;

  // IBackEnd interface
public:
  void Register(const TDepend& dep_p, const Url& url_p) override { m_urls[dep_p] = url_p; }
  void Register(const TPackage& package_p) override { m_dictionnary.insert(std::make_pair(package_p.name, package_p)); }
  Url Resolve(const TDepend& depends_p) const override
  {
    auto it = m_urls.find(depends_p);
    if (it != m_urls.end()) {
      return it->second;
    }
    return {Url::undef,{}};
  }


  std::vector<TDepend> AvailableVersionFor(QString wildcard) const override
  {
    std::vector<TDepend> v;

    QRegExp rx(wildcard);
    rx.setPatternSyntax(QRegExp::Wildcard);

    for (const auto& element : m_dictionnary) {
        if (rx.exactMatch(element.first)) {
            v.push_back(TDepend{element.first,TVersion{element.second.version}});
        }
    }
    return v;
  }

  std::pair<bool,TPackage> Package(const TDepend &depend_p) const override
  {
      auto range = m_dictionnary.equal_range(depend_p.name);

      for(auto it = range.first ; it != range.second; it++){
          if( it->second.name == depend_p.name && it->second.version == it->second.version){
              return std::make_pair(true,it->second);
          }
      }
      return std::make_pair(false,TPackage());
  }
};

#endif // BACKEND_H
