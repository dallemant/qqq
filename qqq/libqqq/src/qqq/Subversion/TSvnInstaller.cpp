#include "qqq/Api/Exceptions.h"
#include "TSvnInstaller.h"

TSvnInstaller::TSvnInstaller(ISvnWrapper *svn_p)
    :m_svn(svn_p)
{

}

bool TSvnInstaller::CanHandle(Url::Protocol protocole_p) const
{
    return protocole_p == Url::subversion;
}

void TSvnInstaller::Fetch(const TDepend &dep_p, const QUrl &url_p)
{
    if(!url_p.isValid()){
        throw std::runtime_error(QString("Subversion Fetch: dependancy %1_%2 is not registered in backend").arg(dep_p.name,dep_p.version).toStdString());
    }

    // target
    const QString target = QDir::current().absoluteFilePath(dep_p.name);
    QFileInfo ft(target);
    if (ft.exists()) {
        throw std::runtime_error(QString("Subversion Fetch: target dir %1 already exist").arg(target).toStdString());
    }
    m_svn->CheckOut(dep_p.name,url_p,QDir::current());
}

void TSvnInstaller::Remove(const TDepend &dep_p)
{
    QDir target{QDir::current().absoluteFilePath(dep_p.name)};
    if (!target.exists()) {
        // nothing - is it an error?
        return;
    }

    /// @todo save content in temp directory ; if operation fail restore it
    if (!target.removeRecursively()) {
        throw std::runtime_error(
            QString("Subversion Remove: delete [%1]  something went wrong...").arg(target.path()).toStdString());
    }
}

void TSvnInstaller::Replace(const TDepend &old_p, const TDepend &new_p, const QUrl &url_p)
{
    (void)old_p;
    (void)url_p;
    /// @todo check if about to remove is not modified, in the expected version....

    if(!url_p.isValid()){
        throw std::runtime_error(QString("Subversion Replace: dependancy %1_%2 is not registered in backend").arg(new_p.name,new_p.version).toStdString());
    }
    m_svn->Switch(url_p, QDir::current());
}

Url TSvnInstaller::GetRepositoryUrl(QDir dir) const
{
    return Url{ Url::subversion, m_svn->GetRepositoryUrl(dir) };
}
