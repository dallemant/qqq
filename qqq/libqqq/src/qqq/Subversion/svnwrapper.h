#ifndef SVNWRAPPER_H
#define SVNWRAPPER_H

#include <QDir>
#include <QUrl>
///
/// \brief The ISvnWrapper class
/// Lazy check of svn command is in the path
/// avoid unnecessary trouble if using only git or local storage
class ISvnWrapper
{
public:
    ISvnWrapper();

    virtual void CheckOut(const QString name_p, const QUrl& url_p,QDir dir_p)=0;

    virtual void Switch(const QUrl& url_p, QDir dir_p)=0;

    virtual QUrl GetRepositoryUrl(QDir dir_p)const=0;
    virtual QByteArray ReadRecursivlyExternals(QUrl dir)const=0;
};

class SvnWrapper:public ISvnWrapper{


public:
    SvnWrapper();
    SvnWrapper(QString programm_p);

    void CheckOut(const QString name_p,const QUrl &url_p, QDir dir_p)override;
    void Switch(const QUrl &url_p, QDir dir_p)override;

    virtual QUrl GetRepositoryUrl(QDir dir_p)const override;
    QByteArray ReadRecursivlyExternals(QUrl url)const override;

    void CheckProgram() const;

    QUrl DecodeUrlFromSvnInfoXml(const QByteArray& xml)const;


protected:
    QString m_programm;
    mutable bool m_is_checked;
};

#endif // SVNWRAPPER_H
