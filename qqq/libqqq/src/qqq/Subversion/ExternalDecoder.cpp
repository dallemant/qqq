#include <sstream>
#include "ExternalDecoder.h"



void ExternalDecoder::ReplaceSpecialChars(QUrl repo,QString &word)const{

    if(word.startsWith("/"))
        word.prepend(repo.toString());

    if(word.startsWith("^"))
        word.replace("^",repo.toString());
}

External ExternalDecoder::ReorderLabelUrl(QString first_word, QString second_word) const
{
    QUrl url(first_word);
    if(url.scheme().count()){
        return {url,second_word};
    }

    url = QUrl(second_word);
    if(url.scheme().count()){
        return {url,first_word};
    }

    std::stringstream stream;
    stream<<"ExternalDecoder::readExternal - cannot find url in"<<first_word.toStdString()<<" "<<second_word.toStdString();

    throw std::runtime_error(stream.str());
}

External ExternalDecoder::readExternal(QUrl repo, QString first_word, QString second_word)const{

    ReplaceSpecialChars(repo, first_word);
    ReplaceSpecialChars(repo, second_word);

    return ReorderLabelUrl(first_word,second_word);
}

std::vector<External> ExternalDecoder::Decode(QUrl repo, QTextStream &stream) const{
    std::vector<External> externals;

    QString one,two ;
    do{
        stream >> one;
        if(stream.atEnd()){
            break;
        }
        stream >> two;

        //skip directory part
        if(two == "-"){
            continue;
        }
        externals.push_back(readExternal(repo,one,two));
        one.clear();
        two.clear();

    }while(!stream.atEnd() );

    return externals;
}
