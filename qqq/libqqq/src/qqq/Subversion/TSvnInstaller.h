#ifndef TSVNINSTALLER_H
#define TSVNINSTALLER_H

#include "qqq/Api/installprotocolselector.h"
#include "qqq/Api/IDirRepositoryUrlFinder.h"
#include "qqq/Subversion/svnwrapper.h"

class TSvnInstaller : public IInstallerProtocol,public IDirectoryRepositoryUrlFinder {
   public:
    TSvnInstaller( ISvnWrapper *svn_p);
    // IInstallerProtocol interface
public:
    bool CanHandle(Url::Protocol protocole_p) const override;
    void Fetch(const TDepend &dep_p, const QUrl &url_p) override;
    void Remove(const TDepend &dep_p) override;
    void Replace(const TDepend &old_p, const TDepend &new_p, const QUrl &url_p) override;
    Url GetRepositoryUrl(QDir di) const override;


protected:
    ISvnWrapper* m_svn;

};

#endif  // TSVNINSTALLER_H
