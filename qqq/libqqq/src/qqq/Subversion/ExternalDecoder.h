#ifndef EXTERNALDECODER_H
#define EXTERNALDECODER_H

#include <QUrl>
#include <QString>
#include <QTextStream>

struct External{
    QUrl url;
    QString name;
};

struct ISvnExternalDecoder{
    virtual std::vector<External> Decode(QUrl repo,QTextStream& stream)const =0;
};

class ExternalDecoder: public ISvnExternalDecoder{

public:

   External readExternal(QUrl repo, QString first_word, QString second_word)const;

   std::vector<External> Decode(QUrl repo, QTextStream& stream)const override;
   void ReplaceSpecialChars(QUrl repo, QString& word) const;
   External ReorderLabelUrl(QString first_word, QString second_word)const;
};

#endif // EXTERNALDECODER_H
