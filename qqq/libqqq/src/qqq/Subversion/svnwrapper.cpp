#include <iostream>
#include "svnwrapper.h"
#include "qqq/Api/Exceptions.h"
#include <QProcess>
#include <QUrl>

#include <QXmlStreamReader>

ISvnWrapper::ISvnWrapper()
{
}

SvnWrapper::SvnWrapper() : m_programm("svn"), m_is_checked(false)
{
}

SvnWrapper::SvnWrapper(QString programm_p) : m_programm(programm_p), m_is_checked(false)
{
}

void
SvnWrapper::CheckOut(const QString name_p, const QUrl& url_p, QDir dir_p)
{

  CheckProgram();

  QProcess p;
  p.setProcessChannelMode(QProcess::ForwardedChannels);

  p.setProgram(m_programm);

  p.setWorkingDirectory(dir_p.path());
  p.setArguments({ "checkout", url_p.toString(), name_p });

  p.start();

  if (!p.waitForStarted()) {
    throw std::runtime_error(
      QString("Subversion Checkout, failed to start svn command:%1").arg(p.errorString()).toStdString());
  }


  if (!p.waitForFinished(30000000)) {
    throw std::runtime_error(
      QString("Subversion Checkout, operation timeout  :%2").arg(p.readAll().data()).toStdString());
  }

  if(p.exitCode() != 0){
      throw std::runtime_error(
        QString("Subversion Checkout, operation failed %1 :%2").arg(p.exitCode()).arg(p.readAll().data()).toStdString());
  }
}

void
SvnWrapper::Switch(const QUrl& url_p, QDir dir_p)
{
    (void)url_p;
    (void)dir_p;
    QQQ_THROW_NOT_IMPLEMENTED_FUNCTION
}

QUrl SvnWrapper::GetRepositoryUrl(QDir dir_p) const
{
    CheckProgram();

    QProcess p;
    p.setProcessChannelMode(QProcess::SeparateChannels);

    p.setProgram(m_programm);

    p.setWorkingDirectory(dir_p.path());
    p.setArguments(QStringList{ "info", "--xml", "." });

    p.start();

    if (!p.waitForStarted()) {
      throw std::runtime_error(
        QString("Subversion GetWorkingCopyUrl, failed to start svn command:%1").arg(p.errorString()).toStdString());
    }


    if (!p.waitForFinished(30000000)) {
      throw std::runtime_error(
        QString("Subversion GetWorkingCopyUrl, operation timeout  :%2").arg(p.readAllStandardError().data()).toStdString());
    }

    if(p.exitCode() != 0){
        throw std::runtime_error(
          QString("Subversion GetWorkingCopyUrl, operation failed %1 :%2").arg(p.exitCode()).arg(p.readAllStandardError().data()).toStdString());
    }

    return DecodeUrlFromSvnInfoXml(p.readAllStandardOutput());

}

void
SvnWrapper::CheckProgram()const
{
  if (m_is_checked)
    return;

  QProcess p;
  p.setProgram(m_programm);

  p.setArguments({ "--version", "--quiet" });
  p.setProcessChannelMode(QProcess::ForwardedChannels);
  p.start();
  if (!p.waitForStarted()) {
    throw std::runtime_error(QString("Subversion check command, failed to start command:\n %1\n%2\n  "
                                     "add it on the path or set full path with command: qqq "
                                     "set_svn_path  [path/to/svn]")
                               .arg(p.errorString())
                             .arg(m_programm)
                               .toStdString());
  }

  ;

  if (!p.waitForFinished(1000)) {
    std::runtime_error(QString("Subversion CheckProgram,svn program not found (command = %1) "
                               "add it on the path or set full path with command: qqq "
                               "set_svn_path  [path/to/svn]")
                         .arg(m_programm)
                         .toStdString());
  }

  if(p.exitCode() != 0){
      std::runtime_error(QString("Subversion check command,subversion program failed (command = %1) "
                                 "add it on the path or set full path with command: qqq "
                                 "set_svn_path  [path/to/svn]")
                           .arg(m_programm)
                           .toStdString());
  }

  m_is_checked = true;
}

QUrl SvnWrapper::DecodeUrlFromSvnInfoXml(const QByteArray &xml) const
{
    QXmlStreamReader reader(xml);
    auto path = {"info", "entry", "url"};

    auto element = [&reader](QString element){
        while (reader.readNextStartElement() && !reader.atEnd()) {
                  if (reader.name() == element){
                      return true;
                  }
        };
        return false;
    };

    bool success = true;
    for(auto e:path){
        success &= element(e);
    }

    if(reader.hasError()){
        throw std::runtime_error(QString("DecodeUrlFromSvnInfoXml error :%&").arg(reader.errorString()).toStdString());
    }

    if(success){
        return QUrl(reader.readElementText());
    }else{
        throw std::runtime_error("DecodeUrlFromSvnInfoXml  info:entry:url not find");
    }



}

QByteArray SvnWrapper::ReadRecursivlyExternals(QUrl url) const
{
    CheckProgram();

    QProcess p;
    p.setProcessChannelMode(QProcess::SeparateChannels);

    p.setProgram(m_programm);

    //p.setWorkingDirectory(dir.path());
    //svn propget svn:externals -R .

    QString url_arg;
    if(url.isLocalFile()){
        url_arg = ".";
        p.setWorkingDirectory(url.toLocalFile());
    }else{
        url_arg = url.toString();
    }
    p.setArguments(QStringList{ "propget", "svn:externals","-R" , url_arg });

    p.start();

    if (!p.waitForStarted()) {
      throw std::runtime_error(
        QString("Subversion ReadRecursivlyExternals, failed to start svn command:%1").arg(p.errorString()).toStdString());
    }

    if (!p.waitForFinished(30000000)) {
      throw std::runtime_error(
        QString("Subversion ReadRecursivlyExternals, operation timeout  :%2").arg(p.readAllStandardError().data()).toStdString());
    }

    if(p.exitCode() != 0){
        throw std::runtime_error(
          QString("Subversion ReadRecursivlyExternals, operation failed %1 :%2").arg(p.exitCode()).arg(p.readAllStandardError().data()).toStdString());
    }

    return p.readAllStandardOutput();
}
