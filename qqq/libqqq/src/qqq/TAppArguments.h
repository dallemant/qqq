#ifndef TAPPARGUMENTS_H
#define TAPPARGUMENTS_H

#include <QCommandLineParser>
#include <QStringList>

class TCommand;
class TConsole;

///
/// \brief The TAppArguments class : QCommandLineParser wrapper
///  it is a TCommand container
///  command line frame is : [app_name] [command name] [command options] [command positionnal arguments
///
///  framework inspired from:
///  qthelp://org.qt-project.qtcore.560/qtcore/qcommandlineparser.html#clearPositionalArguments
///
class TAppArguments {
   public:
    TAppArguments();

    void RegisterCommand(TCommand* command_p);
    void RegisterCommand(const std::vector<TCommand*>& command_p);

    ///
    /// \brief Process
    ///  read first positionnal argument  to detect commmand to apply
    ///  and run command
    /// \param arguments_p : QCoreApplication::arguments
    /// \param console_p : messages for user
    /// \return
    ///
    bool Process(QStringList arguments_p, TConsole* console_p);

    QStringList Commands() const;

   private:
    bool IsBuiltinOptionSet(TConsole* console_p);

    QCommandLineParser m_parser;
    std::vector<TCommand*> m_commands;

    ///
    /// \brief m_help builtin option to show help
    ///
    QCommandLineOption m_help;
    ///
    /// \brief m_version builtin option tu show application version
    ///
    QCommandLineOption m_version;
};

#endif  // TAPPARGUMENTS_H
