#ifndef TLOCALINSTALLER_H
#define TLOCALINSTALLER_H

#include "qqq/Api/installprotocolselector.h"

class TLocalInstaller: public IInstallerProtocol {
public:

    // IInstallerProtocol interface
public:
    bool CanHandle(Url::Protocol protocole_p) const override;
    void Fetch(const TDepend &dep_p, const QUrl &url_p) override;
    void Remove(const TDepend &dep_p) override;



    static bool copyRecursively(const QString &srcFilePath, const QString &tgtFilePath);
private:

};

#endif // TLOCALINSTALLER_H
