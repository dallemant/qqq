#include "TLocalInstaller.h"



bool TLocalInstaller::CanHandle(Url::Protocol protocole_p) const
{
    return protocole_p == Url::local_storage;
}

void TLocalInstaller::Fetch(const TDepend &dep_p, const QUrl &url_p)
{


    if(!url_p.isValid()){
        throw std::runtime_error(QString("Local Fetch: dependancy %1_%2 is not registered in backend").arg(dep_p.name,dep_p.version).toStdString());
    }

    if (url_p.isLocalFile()) {
        // source
        QString source = url_p.toLocalFile();
        QFileInfo fs(source);
        if (!fs.exists()) {
            throw std::runtime_error(QString("Local Fetch: repository %1 does not exist").arg(source).toStdString());
        }

        // target
        const QString target = QDir::current().absoluteFilePath(dep_p.name);
        QFileInfo ft(target);
        if (ft.exists()) {
            throw std::runtime_error(QString("Local Fetch: target dir %1 already exist").arg(target).toStdString());
        }

        // copy
        if (!copyRecursively(source, target)) {
            throw std::runtime_error(
                QString("Local Fetch: copy %1 to %2 failed").arg(source, QDir::currentPath()).toStdString());
        }
    }
}

void TLocalInstaller::Remove(const TDepend &dep_p)
{
    QDir target{QDir::current().absoluteFilePath(dep_p.name)};
    if (!target.exists()) {
        // nothing - is it an error?
        return;
    }

    /// @todo save content in temp directory ; if operation fail restore it
    if (!target.removeRecursively()) {
        throw std::runtime_error(
            QString("Local Remove: delete [%1]  something went wrong...").arg(target.path()).toStdString());
    }
}

// https://gist.github.com/ssendeavour/7324701
// https://qt.gitorious.org/qt-creator/qt-creator/source/1a37da73abb60ad06b7e33983ca51b266be5910e:src/app/main.cpp#L13-189
// taken from utils/fileutils.cpp. We can not use utils here since that depends app_version.h.
bool TLocalInstaller::copyRecursively(const QString& srcFilePath, const QString& tgtFilePath) {
    QFileInfo srcFileInfo(srcFilePath);
    if (srcFileInfo.isDir()) {
        QDir targetDir(tgtFilePath);
        targetDir.cdUp();  // what if targetDir is root??
        if (!targetDir.mkdir(QFileInfo(tgtFilePath).fileName()))
            return false;
        QDir sourceDir(srcFilePath);
        QStringList fileNames =
            sourceDir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden | QDir::System);
        foreach (const QString& fileName, fileNames) {
            const QString newSrcFilePath = srcFilePath + QLatin1Char('/') + fileName;
            const QString newTgtFilePath = tgtFilePath + QLatin1Char('/') + fileName;
            if (!copyRecursively(newSrcFilePath, newTgtFilePath))
                return false;
        }
    } else {
        if (!QFile::copy(srcFilePath, tgtFilePath))
            return false;
    }
    return true;
}

