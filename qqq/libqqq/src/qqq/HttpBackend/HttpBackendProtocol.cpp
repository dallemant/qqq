#include "HttpBackendProtocol.h"
#include <QUrlQuery>

HttpBackendProtocol::HttpBackendProtocol(const std::string& address_p, int port_p)
    :url( buildUrl(address_p, port_p) )
{

}

QUrl HttpBackendProtocol::RegisterPackageApi() const
{
    QUrl api = url;
    api.setPath("/api/package");
    return api;
}

QUrl HttpBackendProtocol::ResolveUrlApi(const TDepend &dep_p) const
{
    QUrl api = url;
    api.setPath(QString("/api/resolve/%1/%2").arg(dep_p.name).arg(dep_p.version));
    return api;
}

QUrl HttpBackendProtocol::ListDependsApi(QString pattern_p) const
{
    QUrl api = url;
    api.setPath("api/list");
    QUrlQuery query;
    query.addQueryItem("pattern",pattern_p);

    api.setQuery(query);

    return api;
}

QUrl HttpBackendProtocol::ResolvePackageApi(const TDepend &dep_p) const
{
    QUrl api = url;
    api.setPath(QString("/api/package/%1/%2").arg(dep_p.name).arg(dep_p.version));
    return api;
}

QUrl HttpBackendProtocol::RegisterUrlApi() const
{
    QUrl api = url;
    api.setPath("/api/register");
    return api;
}


QUrl HttpBackendProtocol::buildUrl(const std::string &ip, int port) const{
   QUrl url;
   url.setScheme("http");
   url.setHost(QString::fromStdString(ip));
   url.setPort(port);
   return url;
}
