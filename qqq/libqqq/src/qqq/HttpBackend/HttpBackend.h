#ifndef HTTPBACKEND_H
#define HTTPBACKEND_H
#include <QNetworkAccessManager>
#include "qqq/Api/IBackEnd.h"
#include "qqq/HttpBackend/HttpBackendProtocol.h"
class QNetworkAccessManager;
class HttpBackend:public IBackendImplementedResolver
{
public:
    HttpBackend(const std::string &address_p, int port_p);

    // IBackEnd interface
public:
    void Register(const TDepend &dep_p, const Url &url_p) override;
    void Register(const TPackage &package_p) override;
    Url Resolve(const TDepend &depend_p) const override;

    std::vector<TDepend> AvailableVersionFor(QString pattern_p) const override;
    std::pair<bool, TPackage> Package(const TDepend &depend_p) const override;

private:
    mutable QNetworkAccessManager m_manager;
    const HttpBackendProtocol m_proto;


};

#endif // HTTPBACKEND_H
