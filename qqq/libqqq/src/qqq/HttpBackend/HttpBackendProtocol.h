#ifndef HTTPBACKENDPROTOCOL_H
#define HTTPBACKENDPROTOCOL_H

#include "qqq/Package/TUrl.h"
#include "qqq/Package/TPackage.h"

class HttpBackendProtocol
{
public:
    HttpBackendProtocol(const std::string& ip, int port);

    QUrl RegisterUrlApi()const;
    QUrl RegisterPackageApi()const;
    QUrl ResolveUrlApi(const TDepend& dep_p)const;
    QUrl ListDependsApi(QString pattern_p)const;
    QUrl ResolvePackageApi(const TDepend& dep_p)const;

private:
    const QUrl url;
    QUrl buildUrl(const std::string& ip, int port)const;
};

#endif // HTTPBACKENDPROTOCOL_H
