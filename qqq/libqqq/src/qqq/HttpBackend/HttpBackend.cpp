#include <memory>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QVariantMap>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QUrlQuery>
#include <qqq/Package/TPackageJson.h>

#include "HttpBackend.h"

using UReply = std::unique_ptr<QNetworkReply>;



HttpBackend::HttpBackend(const std::string& address_p, int port_p)
    :m_proto(address_p, port_p)

{

}


void HttpBackend::Register(const TDepend &dep_p, const Url &url_p)
{
    QEventLoop loop;

    QVariantMap data;
    data["name"] = dep_p.name;
    data["version"] = dep_p.version;
    data["url"]= url_p.UrlStr();
    data["protocol"]= url_p.ProtocolStr();
    QJsonDocument jsonData = QJsonDocument::fromVariant(data);

    QNetworkRequest request(m_proto.RegisterUrlApi());
    request.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));

    auto reply = UReply(m_manager.put(request,jsonData.toJson()));

    QObject::connect(reply.get(), &QNetworkReply::finished,&loop,&QEventLoop::quit);
    loop.exec();

    if(reply->error() != QNetworkReply::NoError){
        throw std::runtime_error(qPrintable(reply->errorString()));
    }

    if(reply->isFinished()){
        QVariant contentMimeType = reply->header(QNetworkRequest::ContentTypeHeader);
        qDebug() << contentMimeType.toString();
        qDebug()<< "received "<<reply->readAll();
    }
}

void HttpBackend::Register(const TPackage &package_p)
{
    QEventLoop loop;

    auto json =PackageJson::ToJsonObject(package_p);
    QJsonDocument doc(json);

    QNetworkRequest request( m_proto.RegisterPackageApi() );
    request.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));

    auto reply = UReply(m_manager.put(request,doc.toJson()));

    QObject::connect(reply.get(), &QNetworkReply::finished,&loop,&QEventLoop::quit);
    loop.exec();

    if(reply->error() != QNetworkReply::NoError){
        throw std::runtime_error(qPrintable(reply->errorString()));
    }

    if(reply->isFinished()){
        QVariant contentMimeType = reply->header(QNetworkRequest::ContentTypeHeader);
        qDebug() << contentMimeType.toString();
        qDebug()<< "received "<<reply->readAll();
    }
}

Url HttpBackend::Resolve(const TDepend &depend_p) const
{

    QEventLoop loop;
    QNetworkRequest request( m_proto.ResolveUrlApi(depend_p));

    auto reply = UReply(m_manager.get(request));

    QObject::connect(reply.get(), &QNetworkReply::finished,&loop,&QEventLoop::quit);
    loop.exec();

    if(reply->error() != QNetworkReply::NoError){
        throw std::runtime_error(qPrintable(reply->errorString()));
    }

    if(reply->isFinished()){
        QVariant contentMimeType = reply->header(QNetworkRequest::ContentTypeHeader);

        QJsonParseError error;
        auto doc =QJsonDocument::fromJson(reply->readAll(),&error);

        if(error.error != QJsonParseError::NoError){
            std::runtime_error(qPrintable(error.errorString()));
        }

        qDebug()<< doc.toBinaryData();

        auto vmap = doc.toVariant().toMap();

        return Url{ Url::FromProtocol(vmap["protocol"].toString()), QUrl(vmap["url"].toString()) };
    }

    return {};

}

std::vector<TDepend> HttpBackend::AvailableVersionFor(QString pattern_p) const
{
    QEventLoop loop;

    QNetworkRequest request( m_proto.ListDependsApi( pattern_p ));

    auto reply = UReply(m_manager.get(request));

    QObject::connect(reply.get(), &QNetworkReply::finished,&loop,&QEventLoop::quit);
    loop.exec();

    if(reply->error() != QNetworkReply::NoError){
        throw std::runtime_error(reply->errorString().toStdString());
    }

    if(reply->isFinished()){

        QJsonParseError error;
        auto doc =QJsonDocument::fromJson(reply->readAll(),&error);

        if(error.error != QJsonParseError::NoError){
            std::runtime_error(error.errorString().toStdString());
        }

        qDebug()<< doc.toBinaryData();
        const QJsonObject obj = doc.object();

        std::vector<TDepend>ret;
        if(obj.contains("error")){
            return ret;
        }

       QJsonArray array =doc.array();

       for(QJsonValue val: array){
           auto obj =val.toObject();
           ret.push_back({obj["name"].toString(),obj["version"].toString()});

       }
       return ret;
    }

    return {};
}


std::pair<bool, TPackage> HttpBackend::Package(const TDepend &depend_p) const
{
    QEventLoop loop;

    QNetworkRequest request(m_proto.ResolvePackageApi(depend_p));

    auto reply = UReply(m_manager.get(request));

    QObject::connect(reply.get(), &QNetworkReply::finished,&loop,&QEventLoop::quit);
    loop.exec();

    if(reply->error() != QNetworkReply::NoError){
        throw std::runtime_error(reply->errorString().toStdString());
    }

    if(reply->isFinished()){

        QJsonParseError error;
        auto doc =QJsonDocument::fromJson(reply->readAll(),&error);

        if(error.error != QJsonParseError::NoError){
            std::runtime_error(error.errorString().toStdString());
        }

        qDebug()<< doc.toBinaryData();
        const QJsonObject obj = doc.object();

        if(obj.contains("error")){
            return {false, {} };
        }

        return { true, PackageJson::FromJsonObject(obj)};

    }

    return {};
}
