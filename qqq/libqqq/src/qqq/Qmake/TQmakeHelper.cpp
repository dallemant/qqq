#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QTextStream>
#include "qqq/Api/Exceptions.h"
#include "TQmakeHelper.h"

TQmakeHelper::TQmakeHelper()
{

}
QStringList TQmakeHelper::FindProFilesInDir(QDir &dir_p)  {
    QStringList ret;
    QDirIterator it(dir_p.path(), QStringList{"*.pro"}, QDir::Files | QDir::NoDotAndDotDot);

    while (it.hasNext()) {
        ret << it.next();
    }
    if (ret.empty())
        throw FileNotFound("TQmakeInstaller ", ".pro file");

    return ret;
}

QString TQmakeHelper::FindSubDirProFileInCurrentDir()  {
    QDir current = QDir::current();
    return FindSubDirProFileInDir(current);
}

QString TQmakeHelper::FindSubDirProFileInDir(QDir &dir_p)
{
    auto pro_files = FindProFilesInDir(dir_p);
    for (const QString& file : pro_files) {
        if (IsSubDirProFile(file)) {
            return file;
        }
    }
    throw FileNotFound("TQmakeInstaller ", ".pro file with subdirs template");
}

/// @todo allez voir les sources de qmake pour decoder les fichier
bool TQmakeHelper::IsVariableContains(QFile& file_p, QString var_p, QString value_p)  {
    file_p.seek(0);
    QTextStream stream(&file_p);

    while (!stream.atEnd()) {
        QString line = stream.readLine();
        // supprime les commentaires
        line = line.section('#', 0, 0);

        if (line.contains(var_p)) {
            return line.contains(value_p);
        }
    }
    return false;
}

bool TQmakeHelper::IsSubDirProFile(QString file_p)  {
    QFile file(file_p);
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }
    return IsVariableContains(file, "TEMPLATE", "subdirs");
}


std::shared_ptr<QTextStream> TQmakeHelper::CreateQMakeFile(QDir dir_p,QString file_name_p){

    auto file = std::make_shared<QFile>(dir_p.absoluteFilePath(file_name_p));

    if (!file->open(QIODevice::ReadWrite)) {
        throw std::runtime_error(
            QString("[init] cannot create project dir file %1").arg(file->fileName()).toStdString());
    }
    //file will be deleted only when stream ref count to 0
    std::shared_ptr<QTextStream> stream = std::shared_ptr<QTextStream>(file,new QTextStream(file.get()));

    return stream;
}

std::shared_ptr<QTextStream> TQmakeHelper::CreateSubdirProjectFile(QDir dir_p, QString file_name_p){

     auto str = CreateQMakeFile(dir_p, file_name_p);
    *str << "TEMPLATE = subdirs"<<endl;

    return str;
}

std::shared_ptr<QTextStream> TQmakeHelper::CreateLibraryProjectFile(QDir dir_p,QString file_name_p)
{
    auto str = CreateQMakeFile(dir_p,file_name_p);
   *str << "TEMPLATE = lib"<<endl;
   return str;
}

std::shared_ptr<QTextStream> TQmakeHelper::CreateAppProjectFile(QDir dir_p, QString file_name_p)
{
    auto str = CreateQMakeFile(dir_p,file_name_p);
   *str << "TEMPLATE = app"<<endl;
    return str;
}

void TQmakeHelper::AppendToParentSubdirProFile(QDir dir_p, QString name_p){


    QDir d = dir_p;
    if(!d.cdUp()){throw std::runtime_error("cannot cd up");}
    auto parent_pro_abs_file = TQmakeHelper::FindSubDirProFileInDir(d);

    QFile f(parent_pro_abs_file);
    if(!f.open(QIODevice::ReadWrite|QIODevice::Append)){
        throw std::runtime_error("cannot open subdir project file");
    }

    QTextStream str(&f);
    str <<endl;
    str << "SUBDIRS *= "<<name_p<<endl;
}
