#ifndef TQMAKEWORKSPACE_H
#define TQMAKEWORKSPACE_H
#include <memory>
#include "qqq/Api/IWorkspace.h"

class QTextStream;

class TQMakeWorkspace : public IWorkspace {
    // IWorkspace interface
   public:
    TQMakeWorkspace(QDir current_dir_p);
    void CreateWorkspace(const QString& workspace_name_p, eOption opt_p) override;
    void CreateApplication(const QString& name_p, eOption opt_p) override;
    void CreateLibrary(const QString& name_p, eOption opt_p) override;



    void CreatePackageJson()const;


private:
    void SetWorkingDir(const QString name_p, eOption opt_p);
    QDir m_working_dir;

};

class TQMakeWorkspaceCreator : public IWorkspaceCreator {
    // IWorkspaceCreator interface
   public:
    std::unique_ptr<IWorkspace> CreateWorkspaceTransaction(QDir current_dir_p) override;
};
#endif  // TQMAKEWORKSPACE_H
