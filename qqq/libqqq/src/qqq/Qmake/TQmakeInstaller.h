#ifndef TQMAKEINSTALLER_H
#define TQMAKEINSTALLER_H

#include "qqq/Api/IInstall.h"

class TQmakeInstaller : public IInstallerDecorator {
   public:
    using IInstallerDecorator::IInstallerDecorator;

    // IInstallerDecorator interface
   public:
    void DoFetch(const TDepend& dep_p) override;
    void DoRemove(const TDepend& dep_p) override;
    void DoReplace(const TDepend &old_p, const TDepend &new_p) override;





};

#endif  // TQMAKEINSTALLER_H
