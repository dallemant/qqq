#include <QTextStream>
#include "qqq/Api/Exceptions.h"
#include "qqq/Package/TPackageJson.h"
#include "qqq/Package/TPackage.h"
#include "TQmakeHelper.h"
#include "TQmakeWorkspace.h"

TQMakeWorkspace::TQMakeWorkspace(QDir current_dir_p) : IWorkspace(current_dir_p) {

   m_working_dir = current_dir_p;

}



void TQMakeWorkspace::CreatePackageJson() const
{
    TPackage package;
    package.name = m_working_dir.dirName();
    package.version = "0.0.0";
    PackageJson::Save(m_working_dir, package);
}

void TQMakeWorkspace::SetWorkingDir(const QString name_p, IWorkspace::eOption opt_p)
{
    m_working_dir = [&] {
        switch (opt_p) {
        case IWorkspace::eCurrentDirectory:
            // check folder is empty
            if (m_current_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries).size() > 0) {
                throw DirectoryNotEmpty{};
            }
            return m_current_dir;
        case IWorkspace::eCreateDirectory:
            // check folder does not already exists
            if (m_current_dir.cd(name_p)) {
                throw DirectoryAlreadyExist{};
            }

            if (!m_current_dir.mkpath(name_p)) {
                throw std::runtime_error(QString("[init] cannot create directory %1 in %2")
                                             .arg(name_p)
                                             .arg(m_current_dir.path())
                                             .toStdString());
            }

            return QDir(m_current_dir.path() + "/" + name_p);

        default:
            throw std::runtime_error("TQMakeWorkspace::CreateWorkspace IWorkspace::eOption unknow");
        }
    }();
}




void TQMakeWorkspace::CreateWorkspace(const QString &workspace_name_p, IWorkspace::eOption opt_p) {

    SetWorkingDir(workspace_name_p,opt_p);

    Q_ASSERT(m_working_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries).size() == 0);

    QFile qmake_from(":/qmake.conf");
    if (!qmake_from.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(
            QString("[init] cannot open resource file %1").arg(qmake_from.fileName()).toStdString());
    }


    QFile qmake_to(m_working_dir.absoluteFilePath(".qmake.conf"));
    if (!qmake_to.open(QIODevice::WriteOnly)) {
        throw std::runtime_error(QString("[init] cannot open file %1").arg(qmake_to.fileName()).toStdString());
    }

    const auto qmake_data = qmake_from.readAll();
    const auto written = qmake_to.write(qmake_data);
    if (qmake_data.size() != written) {
        throw std::runtime_error(
            QString("[init][BuildNewProject] cannot copy .qmake.conf in directory %1 (%2/%3 bytes written")
                .arg(m_working_dir.path())
                .arg(written)
                .arg(qmake_data.size())
                .toStdString());
    }

    auto stream =  TQmakeHelper::CreateQMakeFile(m_working_dir,"defines.pri");
    *stream << "##### place your global variables here";
    stream.reset();

    //create workapce .pro file
    QString proj_file = QString("%1.pro").arg(m_working_dir.dirName());
    stream = TQmakeHelper::CreateSubdirProjectFile(m_working_dir, proj_file);
    *stream << "OTHER_FILES += .qmake.conf package.json"<<endl;

    // creation de la description du package
    CreatePackageJson();


}



void TQMakeWorkspace::CreateLibrary(const QString& name_p, eOption opt_p)
{
    SetWorkingDir(name_p,opt_p);

    auto stream =  TQmakeHelper::CreateSubdirProjectFile(m_working_dir, name_p + ".pro");
    *stream << "SUBDIRS += src"<<endl;
    *stream << "SUBDIRS += tests"<<endl;
    *stream << "OTHER_FILES += package.json features/*"<<endl;
    *stream << "tests.depends = src"<<endl;

    bool mkdirs = m_working_dir.mkdir("src") &&    m_working_dir.mkdir("tests") &&    m_working_dir.mkdir("features");
    if(!mkdirs){
        throw std::runtime_error(" mkdir  src,test and features failed");
    }

    TQmakeHelper::CreateSubdirProjectFile(m_working_dir, "tests/tests.pro");

    stream = TQmakeHelper::CreateQMakeFile(m_working_dir, "features/include.pri");
    *stream << "QMAKEFEATURES += $$PWD"<<endl;
    QString prf_file =  QString("features/%1.prf").arg(name_p);
    stream = TQmakeHelper::CreateQMakeFile(m_working_dir,prf_file);
    *stream << QString("error(\"%1 :define build rules to link with %2\")").arg(m_current_dir.absoluteFilePath(prf_file),name_p)<<endl;

    TQmakeHelper::CreateLibraryProjectFile(m_working_dir,"src/src.pro");

    // creation de la description du package
    CreatePackageJson();

    try{
        //try to append this componant to parent subdir
        TQmakeHelper::AppendToParentSubdirProFile(m_working_dir,name_p);
    }catch(...){
        ///never mind
    }
}

void TQMakeWorkspace::CreateApplication(const QString& name_p, eOption opt_p)
{
    SetWorkingDir(name_p,opt_p);

    auto stream =  TQmakeHelper::CreateSubdirProjectFile(m_working_dir, name_p + ".pro");
    *stream << "SUBDIRS += src"<<endl;
    *stream << "SUBDIRS += tests"<<endl;
    *stream << "OTHER_FILES += package.json"<<endl;
    *stream << "tests.depends = src"<<endl;

    bool mkdirs = m_working_dir.mkdir("src") &&    m_working_dir.mkdir("tests") ;
    if(!mkdirs){
        throw std::runtime_error(" mkdir  src,test  failed");
    }

    TQmakeHelper::CreateSubdirProjectFile(m_working_dir,"tests/tests.pro");

    TQmakeHelper::CreateAppProjectFile(m_working_dir,"src/src.pro");

    // creation de la description du package
    CreatePackageJson();

    try{
        //try to append this componant to parent subdir
        TQmakeHelper::AppendToParentSubdirProFile(m_working_dir,name_p);
    }catch(...){
        ///never mind
    }
}

std::unique_ptr<IWorkspace> TQMakeWorkspaceCreator::CreateWorkspaceTransaction(QDir current_dir_p) {
    return std::unique_ptr<IWorkspace>(new TQMakeWorkspace(current_dir_p));
}
