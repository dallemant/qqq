#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDirIterator>
#include "qqq/Api/Exceptions.h"
#include "TQmakeHelper.h"
#include "TQmakeInstaller.h"


void TQmakeInstaller::DoFetch(const TDepend& dep_p) {

    const QString sub_dir_file = TQmakeHelper::FindSubDirProFileInCurrentDir();

    QFile file(sub_dir_file);
    if (!file.open(QIODevice::ReadWrite | QIODevice::Append)) {
        throw std::runtime_error("TQmakeInstaller::DoFetch cannot open file");
    }

    if (TQmakeHelper::IsVariableContains(file, "SUBDIRS", dep_p.name)) {
        // la varible est deja positionnee
        return;
    }

    file.seek(file.size());

    QTextStream stream(&file);
    stream << endl << "SUBDIRS *= " << dep_p.name << endl;

    /// @todo mettre à jour les dependances du composant
}

void TQmakeInstaller::DoRemove(const TDepend& dep_p) {

    const QString sub_dir_file = TQmakeHelper::FindSubDirProFileInCurrentDir();

    QFile file(sub_dir_file);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error("TQmakeInstaller::DoRemove cannot open file");
    }

    QFile file_o("temp___remove");
    if (!file_o.open(QIODevice::ReadWrite)) {
        throw std::runtime_error("TQmakeInstaller::DoRemove cannot open file");
    }

    QTextStream stream(&file);
    QTextStream stream_o(&file_o);
    while (!stream.atEnd()) {
        const QString line = stream.readLine();
        // supprime les commentaires
        const QString l = line.section('#', 0, 0);
        if (l.contains("SUBDIRS") && l.contains(dep_p.name)) {
        } else {
            stream_o << line;
        }
    }

    if (!file.remove()){
        std::runtime_error("QmakeInstaller::DoRemove remove failed");
    }

    if (!file_o.rename(sub_dir_file)) {
        std::runtime_error("QmakeInstaller::DoRemove rename failed");
    }
}


void TQmakeInstaller::DoReplace(const TDepend &old_p, const TDepend &new_p)
{
    //nothing to do
    (void)old_p;
    (void)new_p;

    /// @todo update dependancies
}
