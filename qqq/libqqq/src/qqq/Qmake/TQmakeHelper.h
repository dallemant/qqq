#ifndef TQMAKEHELPER_H
#define TQMAKEHELPER_H
#include <memory>
#include <QStringList>

class TQmakeHelper
{
public:
    TQmakeHelper();

    static bool IsSubDirProFile(QString file) ;
    static QStringList FindProFilesInDir(QDir &dir_p) ;

    static bool IsVariableContains(QFile& file_p, QString var_p, QString value_p) ;
    static QString FindSubDirProFileInDir(QDir& dir_p) ;
    static QString FindSubDirProFileInCurrentDir() ;

    static std::shared_ptr<QTextStream> CreateQMakeFile(QDir dir_p, QString file_name_p) ;
    static std::shared_ptr<QTextStream> CreateSubdirProjectFile(QDir dir_p, QString file_name_p) ;
    static std::shared_ptr<QTextStream> CreateLibraryProjectFile(QDir dir_p, QString file_name_p);
    static std::shared_ptr<QTextStream> CreateAppProjectFile(QDir dir_p, QString file_name_p);


    static void AppendToParentSubdirProFile(QDir dir_p, QString name_p);
};

#endif // TQMAKEHELPER_H
