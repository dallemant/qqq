#include "TCommand.h"

TCommandHelper::TCommandHelper(QString name_p,
                               std::function<void(TCommandLineDeclaration*)> prepare_func_p,
                               std::function<bool(const TConfig&, TConsole&)> process_func_p)
    : m_name(name_p), m_prepare_func(prepare_func_p), m_process_func(process_func_p) {}

QString TCommandHelper::GetCommandName() const {
    return m_name;
}

void TCommandHelper::Prepare(TCommandLineDeclaration* parser_p) {
    m_prepare_func(parser_p);
}

bool TCommandHelper::Process(const TConfig& config_p, TConsole& console_p) {
    return m_process_func(config_p, console_p);
}
