#include <QTextStream>
#include <QDir>
#include <QDebug>

#include "qqq/TConsole.h"
#include "qqq/Package/TPackage.h"
#include "qqq/Package/TPackageJson.h"
#include "qqq/Api/Exceptions.h"

#include "TInit.h"

TInit::TInit(IWorkspaceCreator* workspace_p, QTextStream* stream_p) : m_workspace(workspace_p), m_stream(stream_p) {
    Q_ASSERT(m_workspace);

    if (!m_stream) {
        m_stream_owner = std::unique_ptr<QTextStream>(new QTextStream(stdin));
        m_stream = m_stream_owner.get();
    }
}

void TInit::Prepare(TCommandLineDeclaration* parser_p) {
    parser_p->AddPositionalArgument("project name", "name of the project to create", "init [project name]");
    parser_p->AddOption("workspace", "create a workspace");
    parser_p->AddOption("application", "create an application ");
    parser_p->AddOption("library", "create a library");
}

bool TInit::Process(const TConfig& config_p, TConsole& console_p) {
    QDir dir = QDir::current();

    auto workspace = m_workspace->CreateWorkspaceTransaction(dir);

    auto args = config_p.GetPositionnalArguments();

    QString project_name = (args.size() >= 2) ? args.at(1) : dir.dirName();
    try {
        const IWorkspace::eOption dir_option = [&]{
            if (args.size() == 1)       return IWorkspace::eCurrentDirectory;
            else if (args.size() == 2)  return IWorkspace::eCreateDirectory;
            else{
                console_p.Msg()<< "bad number of arguments"<<endl;
                throw std::invalid_argument("bad number of arguments");
            }
        }();
        if(config_p.isSet("workspace")) workspace->CreateWorkspace(project_name, dir_option);
        else if(config_p.isSet("application")) workspace->CreateApplication(project_name, dir_option);
        else if(config_p.isSet("library")) workspace->CreateLibrary(project_name, dir_option);
        else{
            console_p.Msg()<< "choose one option : --workspace   , --application , --library";
            return false;
        }
        console_p.Msg() << QString("[init] project succesfully created");
    } catch (DirectoryNotEmpty) {
        console_p.Msg() << "[init] dir is not empty";
        console_p.Msg() << "[init] provide project name in command line";
        console_p.Msg() << "[init] or execute app in empty directory";
        return false;
    } catch (DirectoryAlreadyExist) {
        console_p.Msg() << QString("[init] project  %1 already exists in %2").arg(project_name).arg(dir.path());
        return false;
    }


    return true;
}
