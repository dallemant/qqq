#include "TSet.h"
#include <QSettings>

#include "qqq/Subversion/svnwrapper.h"
#include "qqq/Git/GitWrapper.h"

TSet::TSet()
{
    SvnPath();
    GitPath();
}

std::vector<TCommand *> TSet::Commands(){
    std::vector<TCommand*> ret;

    for (auto& cmd : m_commands) {
        ret.push_back(cmd.get());
    }
    return ret;
}

void TSet::SvnPath()
{
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddPositionalArgument("svn path","path to svn command","[svn path]");

    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        if(config_p.GetPositionnalArguments().size()==2){
            QString svn_path = config_p.GetPositionnalArguments().at(1);

            console_p.Msg()<<" check svn command: "<<svn_path;

            try{
                SvnWrapper svn (svn_path);
                svn.CheckProgram();
                console_p.Msg() << "Register command";
                QSettings settings;
                settings.setValue("SVN/path",svn_path);
                return true;

            }catch(std::runtime_error e){
                console_p.Msg() << "svn command not usable :" << svn_path<< "error: "<<e.what();
            }
        }
        return true;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("set-svn-path", prepare, process)));
}

void TSet::GitPath()
{
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddPositionalArgument("git path","path to git command","[git path]");

    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        if(config_p.GetPositionnalArguments().size()==2){
            QString git_path = config_p.GetPositionnalArguments().at(1);

            console_p.Msg()<<" check git command: "<<git_path;

            try{
                GitWrapper git (git_path);
                git.CheckProgram();
                console_p.Msg() << "Register command";
                QSettings settings;
                settings.setValue("GIT/path",git_path);
                return true;

            }catch(std::runtime_error e){
                console_p.Msg() << "git command not usable :" << git_path<< "error: "<<e.what();
            }
        }
        return true;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("set-git-path", prepare, process)));
}
