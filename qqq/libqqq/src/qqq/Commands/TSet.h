#ifndef TSET_H
#define TSET_H
#include <memory>


#include "qqq/TCommand.h"

///
/// \brief The TSet class
/// command line to set svn and git path
///
class TSet
{
public:
    TSet();

    std::vector<TCommand*> Commands();
private:
    std::vector<std::unique_ptr<TCommand>> m_commands;

   void SvnPath();
    void GitPath();
};



#endif // TSET_H
