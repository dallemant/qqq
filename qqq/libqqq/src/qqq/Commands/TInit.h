#ifndef TINIT_H
#define TINIT_H

#include <memory>

#include <QDir>

#include "qqq/TCommand.h"
#include "qqq/TConsole.h"
#include "qqq/Api/IWorkspace.h"
struct TDepend;
class QTextStream;

class TInit : public TCommand {
   public:
    TInit(IWorkspaceCreator* workspace_p, QTextStream* stream_p = nullptr);

    // TCommand interface
   public:
    QString GetCommandName() const override { return "init"; }

    void Prepare(TCommandLineDeclaration* parser_p) override;
    bool Process(const TConfig& config_p, TConsole& console_p) override;

    bool BuildNewWorkspace(QDir dir_p);

   private:
    IWorkspaceCreator* m_workspace;
    QTextStream* m_stream;
    std::unique_ptr<QTextStream> m_stream_owner;
};

#endif  // TINIT_H
