#include <QDir>
#include "qqq/Package/TPackage.h"
#include "qqq/Package/TPackageJson.h"
#include "qqq/Package/TLocalTree.h"

#include "TInstall.h"

TInstall::TInstall(IInstallerCreator* installer_p, const IBackEnd* backend_p)
    : m_installer(installer_p), m_backend(backend_p)
{
    Q_ASSERT(m_installer);

    Install();
    Add();
}

std::vector<TCommand *> TInstall::Commands()
{
    std::vector<TCommand*> ret;

    for (auto& cmd : m_commands) {
        ret.push_back(cmd.get());
    }
    return ret;
}


void TInstall::Install()
{
    auto prepare = [](TCommandLineDeclaration* parser_p) {

        parser_p->AddOption("force", "force override in case of conflict");

        /// @todo nice to have
        // parser_p->AddOption("dry-run", "working copy will not change");

    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        if(!QFile::exists(".qmake.conf")){
            console_p.Msg()<<" current dir is not a workspace (.qmake.conf not found)";
            return false;
        }


        bool force_installation = config_p.isSet("force");

        auto install = m_installer->CreateInstallerTransaction();

        auto args = config_p.GetPositionnalArguments();

        //build dep list to install
        QString error;

        // find dep to install
        TPackage p = PackageJson::Open();
        auto deps = m_backend->ResolveDependencies(p, &error);

        if(error.count()){
            console_p.Msg()<<" error during dependancy resolve "<<error;
            return true;
        }


        const TLocalTree to_install_deps(deps);
        // retrieve componants already installed in workspace
        const TLocalTree installed_deps(QDir::current());

        auto conflicts = to_install_deps.GetConflictDepends(installed_deps);
        if (conflicts.size()) {
            if (!force_installation) {
                console_p.Msg() << "following compounds have different version:";
                for (const auto& dep : conflicts) {
                    auto in_conflict_dep = installed_deps.GetDependsFromName(dep);
                    auto in_replacement_dep = to_install_deps.GetDependsFromName(dep);
                    console_p.Msg() << "installed:" << in_conflict_dep.name << " " << in_conflict_dep.version;
                    console_p.Msg() << "to install:" << in_replacement_dep.name << " " << in_replacement_dep.version << endl;
                }
                console_p.Msg() << "use --force to override them";
                return false;
            }

            // replace in conflicts dep
            for (const auto& dep : conflicts) {
                auto in_conflict_dep = installed_deps.GetDependsFromName(dep);
                console_p.Msg() << "remove in conflicts dep: " << in_conflict_dep.name << " " << in_conflict_dep.version;
                auto in_replacement_dep = to_install_deps.GetDependsFromName(dep);
                console_p.Msg() << "and replace it with dep " << in_replacement_dep.name << " "
                                << in_replacement_dep.version;
                install->Replace(in_conflict_dep, in_replacement_dep);
            }
        } else {
            console_p.Msg() << "no conflicts detected";
        }

        auto missings = to_install_deps.GetMissingDepends(installed_deps);
        console_p.Msg() << "install " << missings.size() << " new deps:";
        for (const auto& dep : missings) {
            console_p.Msg() << "install " << dep.name << " " << dep.version;
            install->Fetch(dep);
        }
        console_p.Msg() << "done";

        return true;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("install", prepare, process)));
}

void TInstall::Add()
{
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddPositionalArgument("composant name", "name of the dependancy to add in package.json.", "[componant name]");
        parser_p->AddPositionalArgument("composant version", "version of the dependancy to install.", "[componant version]");
        parser_p->AddOption("force", "modify version if dependancy is already present.");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {
        auto args = config_p.GetPositionnalArguments();
        TDepend new_dep{args.at(1), args.at(2)};
        console_p.Msg() << "add " << new_dep.name << " in package.json";
        // add new_dep in package
        TPackage p = PackageJson::Open();

        auto it = std::find_if(p.depends.begin(), p.depends.end(),[&new_dep](const TDepend& dep){
            return new_dep.name == dep.name;
        });
        if(it == p.depends.end()){
            p.depends.push_back(new_dep);
            PackageJson::Save(p);
            return true;
        }

        if( (it->version == new_dep.version)){
            console_p.Msg() << new_dep.name <<" is already set with this version";
            return true;
        }

        if(config_p.isSet("force")){
            console_p.Msg() << new_dep.name <<" change from"<<it->version <<" to "<<new_dep.version;
            p.depends.erase(it);
            p.depends.push_back(new_dep);
            PackageJson::Save(p);
            return true;
        }
        console_p.Msg()<<"version "<<it->version<<" is already set for "<<it->name<<" use --force option to modify";
        return true;

    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("add", prepare, process)));
}

