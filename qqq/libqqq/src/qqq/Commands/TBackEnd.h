#ifndef TBACKEND_H
#define TBACKEND_H

#include <memory>

#include <QDir>

#include "qqq/TCommand.h"
#include "qqq/TConsole.h"
#include "qqq/Api/IWorkspace.h"
#include "qqq/Api/IBackEnd.h"
#include "qqq/Api/IDirRepositoryUrlFinder.h"
struct TDepend;
class QTextStream;

class TBackEndCommands {
   public:
    TBackEndCommands(IBackEnd* backend_p,IDirectoryRepositoryUrlFinder*finder_p);

    // TCommand interface
   public:
    std::vector<TCommand*> Commands();

   private:
    void RegisterUrl();
    void RegisterPackage();
    void ResolveUrl();
    void ResolveDependancies();
    void Register();
    void List();

    IBackEnd* m_backend;
    IDirectoryRepositoryUrlFinder* m_finder;
    std::vector<std::unique_ptr<TCommand>> m_commands;
};

#endif  // TBACKEND_H
