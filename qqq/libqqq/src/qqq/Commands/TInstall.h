#ifndef TINSTALL_H
#define TINSTALL_H
#include <memory>
#include "qqq/TConsole.h"
#include "qqq/TCommand.h"
#include "qqq/Api/IInstall.h"
#include "qqq/Api/IBackEnd.h"

struct TDepend;

class TInstall  {
   public:
    /// @param backend_p needed to resolve package dependencies
    TInstall(IInstallerCreator* installer_p, const IBackEnd* backend_p);

    // TCommand interface
   public:
    std::vector<TCommand*> Commands();

   private:

    void Install();
    void Add();

    IInstallerCreator* m_installer;
    const IBackEnd* m_backend;
    std::vector<std::unique_ptr<TCommand>> m_commands;
};

#endif  // TINSTALL_H
