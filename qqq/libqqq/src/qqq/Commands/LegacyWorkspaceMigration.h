#ifndef LEGACYWORKSPACEMIGRATION_H
#define LEGACYWORKSPACEMIGRATION_H
#include <memory>
#include "qqq/TCommand.h"
#include "qqq/TConsole.h"
#include "qqq/Subversion/svnwrapper.h"
#include "qqq/Subversion/ExternalDecoder.h"

class LegacyWorkspaceMigration
{
    std::vector<std::unique_ptr<TCommand>> m_commands;
public:
    LegacyWorkspaceMigration(ISvnWrapper* svn, ISvnExternalDecoder *decoder);

    std::vector<TCommand*> Commands();




};

#endif // LEGACYWORKSPACEMIGRATION_H
