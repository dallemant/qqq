#include <QJsonDocument>
#include "qqq/Package/TUrl.h"
#include "qqq/Package/TPackage.h"
#include "qqq/Package/TPackageJson.h"
#include "LegacyWorkspaceMigration.h"

LegacyWorkspaceMigration::LegacyWorkspaceMigration(ISvnWrapper *svn, ISvnExternalDecoder* decoder)
{
    auto prepare = [=](TCommandLineDeclaration* parser_p) {
        parser_p->AddOption("json","generatee package.json");
        parser_p->AddPositionalArgument("url", "local or url", "url");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        QString str = config_p.GetPositionnalArguments().at(1);

        bool is_working_copy = [str](){
            QUrl r(str);
            QString s = r.scheme();
            return  r.scheme().count() == 0;
        }();

        if(is_working_copy) console_p.Msg()<<str <<" is a working copy";
        else    console_p.Msg()<<str<<" is a valid url";


        QUrl url,repo;

        if(is_working_copy){

            if(QDir::isRelativePath(str)){
                url = QUrl::fromLocalFile(QDir::current().absoluteFilePath(str));
            }else{
                url = QUrl::fromLocalFile(str);
            }

            //find out host from working copy
            QUrl repo_url = svn->GetRepositoryUrl(QDir(str));
            if(repo_url.isLocalFile()){
                console_p.Msg()<<"cannot deduce repository on local repo";
                //repo = QUrl("svn://repo/for/test/");
                return false;
            }else{
                url = repo_url;
                repo.setScheme(repo_url.scheme());
                repo.setHost(repo_url.host());
            }

        }else{
            //
            url = QUrl(str);
            repo.setScheme(url.scheme());
            repo.setHost(url.host());

        }


        console_p.Msg()<<"Legacy migration :ReadRecursivlyExternals from url:"<<url.toString();
        console_p.Msg()<<"Legacy migration :ReadRecursivlyExternals repo is:"<<repo.toString();

        auto array = svn->ReadRecursivlyExternals(url);
        QTextStream stream(&array);

        auto externals = decoder->Decode(repo,stream);


        int tag_error_counter =0;
        QString tag_error_msgs;
        QTextStream tag_error_stream(&tag_error_msgs);

        std::vector<TDepend> deps;

        for(const External& external:externals){
            QString tag;

            bool success = true;
            try{
               tag = Url::TagFromSubversionUrl(external.url);
               success = true;
            }catch(std::runtime_error e){
               tag="cannot_be_deduced";
               tag_error_counter++;
               success=false;
            }

            QString url_str = external.url.toString();
            if(success){
                console_p.Msg()<<QString("qqq backend-add-url --name=%1 --tag=%2 --url=%3 --protocol=svn").arg(external.name,tag,url_str);
                deps.push_back({external.name,tag});
            }else{
                tag_error_stream<<QString("qqq backend-add-url --name=%1 --tag=%2 --url=%3 --protocol=svn\n").arg(external.name,tag,url_str);
            }
        }
        console_p.Msg()<<"-------------------------------";

        if(tag_error_counter){
            console_p.Msg()<<tag_error_msgs;
            console_p.Msg()<<"-------------------------------";
            console_p.Msg()<<"!!!!!!  "<<tag_error_counter<<" tags cannot be deduced !!!!!!";
        }
        console_p.Msg()<<externals.size()<<" externals found";

        if(config_p.isSet("json")){
            TPackage p;
            p.name="set_package_name_here";
            p.version="0.0.0";
            p.depends = deps;
            console_p.Msg()<<"-------------------------------";
            console_p.Msg() << QJsonDocument(PackageJson::ToJsonObject(p)).toJson();


        }

        return true;

    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("externals", prepare, process)));
}

std::vector<TCommand *> LegacyWorkspaceMigration::Commands()
{
    std::vector<TCommand*> ret;

    for (auto& cmd : m_commands) {
        ret.push_back(cmd.get());
    }
    return ret;
}
