#include <QVector>
#include "qqq/TConsole.h"
#include "qqq/Package/TPackageJson.h"

#include "TBackEnd.h"

TBackEndCommands::TBackEndCommands(IBackEnd* backend_p, IDirectoryRepositoryUrlFinder *finder_p) : m_backend(backend_p),m_finder(finder_p) {
    Register();
    RegisterUrl();
    RegisterPackage();
    ResolveUrl();
    ResolveDependancies();
    List();
}

std::vector<TCommand*> TBackEndCommands::Commands() {
    std::vector<TCommand*> ret;

    for (auto& cmd : m_commands) {
        ret.push_back(cmd.get());
    }
    return ret;
}

void TBackEndCommands::RegisterUrl() {
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddOption("name", "dependancy name to add", "dependancy name");
        parser_p->AddOption("tag", "dependency version to add", "dependancy version");
        parser_p->AddOption("url", "dependency url", "dependency url");
        parser_p->AddOption("protocol", "svn, local or git ", "protocol tu use");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        bool success = true;
        if (!config_p.isSet("name")) {
            console_p.Msg() << " name not defined" << endl;
            success = false;
        }
        if (!config_p.isSet("tag")) {
            console_p.Msg() << " tag not defined" << endl;
            success = false;
        }

        TDepend dep{config_p.value("name"), config_p.value("tag")};
        if (!config_p.isSet("url")) {
            console_p.Msg() << " url not defined" << endl;
            success = false;
        }
        QUrl url(config_p.value("url"));
        if (!url.isValid()) {
            console_p.Msg() << " url not valid: " << url.errorString() << endl;
            success = false;
        }

        Url::Protocol protocol;
        if (!config_p.isSet("protocol")) {

            if(url.scheme() == "svn"){
                console_p.Msg() << " assume protocol is svn" << endl;
                protocol = Url::subversion;           
            }else{
                console_p.Msg() << " protocol not defined and cannot be deduced" << endl;
                console_p.Msg() << " please precise with protocol config [file,svn,git]" << endl;
                success = false;
            }
        }else{
            QString str_protocol = config_p.value("protocol");
            str_protocol = str_protocol.toLower();

            if((str_protocol == "file") || (str_protocol == "local")){
                protocol = Url::local_storage;
                if(! url.isLocalFile()){
                    console_p.Msg() << " url is not local but protocol is" << endl;
                    success = false;
                }
            }else if((str_protocol == "subversion") || (str_protocol == "svn")){
                protocol = Url::subversion;
            }else if(str_protocol == "git" ){
                protocol = Url::git;
            }else{
                console_p.Msg() << " protocol not recognized: use one of: file local subversion svn git" << endl;
                success = false;
            }
        }

        if (success) {
            this->m_backend->Register(dep, Url{protocol, url});
            console_p.Msg() << " done" << endl;
        }

        return success;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("backend-add-url", prepare, process)));
}

void TBackEndCommands::RegisterPackage() {
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddPositionalArgument("package.json", "file describing package");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        if (config_p.GetPositionnalArguments().size() != 2) {
            console_p.Msg() << " package.json file is not defined in command" << endl;
            return false;
        }
        TPackage p = PackageJson::Open(config_p.GetPositionnalArguments().at(1));

        this->m_backend->Register(p);
        console_p.Msg() << " done" << endl;

        return true;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("backend-add-package", prepare, process)));
}

void TBackEndCommands::ResolveUrl() {
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddOption("name", "dependancy name to resolve", "dependancy name");
        parser_p->AddOption("tag", "dependency version to resolve", "dependancy version");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        bool success = true;
        if (!config_p.isSet("name")) {
            console_p.Msg() << " name not defined" << endl;
            success = false;
        }
        if (!config_p.isSet("tag")) {
            console_p.Msg() << " tag not defined" << endl;
            success = false;
        }

        TDepend dep{config_p.value("name"), config_p.value("tag")};

        if (success) {
            Url url = this->m_backend->Resolve(dep);
            if (url.url.isValid()) {
                console_p.Msg() << "url found:";
                console_p.Msg() << QString("[%1]%2").arg(url.ProtocolStr()).arg(url.UrlStr());
            } else {
                console_p.Msg() << "depend not found";
            }
        }

        return success;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("backend-resolve-url", prepare, process)));
}

void TBackEndCommands::ResolveDependancies() {
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddOption("name", "dependancy name to resolve", "dependancy name");
        parser_p->AddOption("tag", "dependency version to resolve", "dependancy version");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        bool success = true;
        if (!config_p.isSet("name")) {
            console_p.Msg() << " name not defined" << endl;
            success = false;
        }
        if (!config_p.isSet("tag")) {
            console_p.Msg() << " tag not defined" << endl;
            success = false;
        }

        TDepend dep{config_p.value("name"), config_p.value("tag")};

        if (success) {
            QString error;
            auto deps = this->m_backend->ResolveDependencies(dep, &error);

            if (error.size()) {
                console_p.Msg() << error;
                success = false;
            } else if (deps.size()) {
                for (const auto& dep : deps) {
                    console_p.Msg() << QString("%1 # %2").arg(dep.name).arg(dep.version);
                }
            } else {
                console_p.Msg() << "no depends found";
            }
        }

        return success;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("backend-resolve-deps", prepare, process)));
}

void TBackEndCommands::Register()
{
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddOption("local-repo", "specify that repository is local.");
        parser_p->AddOption("dry-run", "doesnt register");
        parser_p->AddPositionalArgument("composant_path", "register composant at path.", "composant path");
    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        const QString package_str = [&] {
            switch (config_p.GetPositionnalArguments().count()) {
                case 2:
                    return config_p.GetPositionnalArguments().at(1);
                default:
                    return QString(".");
            }
        }();

        QDir dir = QDir::current();
        dir.setPath(package_str);
        if( dir.exists() == false){
            console_p.Msg()<<"provided composant path does not exists "<< dir.absolutePath();
            return false;
        }
        dir = QDir(dir.absolutePath());

        Url url{Url::undef,{}};


       if (config_p.isSet("local-repo")){

            QUrl qurl = QUrl::fromLocalFile(dir.absolutePath());
            if(qurl.isValid() == false){
                console_p.Msg()<<" path option is not a valid local storage url "<< qurl.toString();
                return false;
            }

             url = Url{Url::local_storage,qurl};

       }else{

           url = m_finder->GetRepositoryUrl(dir);

           if(url.url.isValid() == false){
               console_p.Msg()<<" repository url cannot be deduced (not svn nor git working copy) - if it is a local storage add --local-repo flag to command line ";
                return false;
           }
       }
       auto package = PackageJson::Open(dir);

       console_p.Msg()<<" about to register package "<< package.name<<" "<<package.version;
       console_p.Msg()<<" repo:"<< url.UrlStr();

       if(config_p.isSet("dry-run") == false){
            m_backend->RegisterOnce(package,url);
       }else {
            console_p.Msg()<<" dry-run is set; operation not done!!!";
        }


        return true;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("register", prepare, process)));


}

void TBackEndCommands::List() {
    auto prepare = [](TCommandLineDeclaration* parser_p) {
        parser_p->AddPositionalArgument("name", "dependancy name wildcard to show", "dependancy name");

    };

    auto process = [=](const TConfig& config_p, TConsole& console_p) {

        const QString pattern = [&] {
            switch (config_p.GetPositionnalArguments().count()) {
                case 2:
                    return config_p.GetPositionnalArguments().at(1);
                default:
                    return QString("*");
            }
        }();

        QRegExp rx(pattern);
        rx.setPatternSyntax(QRegExp::Wildcard);

        auto dico = m_backend->AvailableVersionFor(pattern);
        for (const auto& element : dico) {
            console_p.Msg() << QString("%1 # %2").arg(element.name).arg(element.version);
        }

        return true;
    };

    m_commands.push_back(std::unique_ptr<TCommand>(new TCommandHelper("backend-list", prepare, process)));
}
