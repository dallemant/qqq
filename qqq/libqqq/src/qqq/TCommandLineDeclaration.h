#ifndef TCOMMANDLINEDECLARATION_H
#define TCOMMANDLINEDECLARATION_H

#include <QString>

///
/// \brief The TCommandLineDeclaration class :  QCommandLineParser setter functions wrapper
///
class TCommandLineDeclaration {
   public:
    TCommandLineDeclaration();

    virtual bool AddOption(const QString& name,
                           const QString& description,
                           const QString& valueName = QString(),
                           const QString& defaultValue = QString()) = 0;
    virtual bool AddOption(const QStringList& names,
                           const QString& description,
                           const QString& valueName = QString(),
                           const QString& defaultValue = QString()) = 0;

    virtual void AddPositionalArgument(const QString& name,
                                       const QString& description,
                                       const QString& syntax = QString()) = 0;
};

#endif  // TCOMMANDLINEDECLARATION_H
