#include "qqq/Api/Exceptions.h"
#include "qqq/Git/GitWrapper.h"
#include "GitInstaller.h"

GitInstaller::GitInstaller(IGitWrapper *git)
    :m_git(git)
{

}

bool GitInstaller::CanHandle(Url::Protocol protocole_p) const
{
    return protocole_p == Url::git;
}

void GitInstaller::Fetch(const TDepend &dep_p, const QUrl &url_p)
{
    /// @todo https://stackoverflow.com/questions/20280726/how-to-git-clone-a-specific-tag/24102558


    if(!url_p.isValid()){
        throw std::runtime_error(QString("Git Clone: dependancy %1_%2 is not registered in backend").arg(dep_p.name,dep_p.version).toStdString());
    }

    // target
    const QString target = QDir::current().absoluteFilePath(dep_p.name);
    QFileInfo ft(target);
    if (ft.exists()) {
        throw std::runtime_error(QString("Git Clone: target dir %1 already exist").arg(target).toStdString());
    }
    m_git->Clone(dep_p.name,url_p, QDir::current());

    QDir d = QDir::current();
    if(!d.cd(dep_p.name)){
        throw std::runtime_error(QString("GitInstaller::Fetch dir %1 does not exist").arg(target).toStdString());
    }

    m_git->Checkout(dep_p.version, d);

}

void GitInstaller::Remove(const TDepend &dep_p)
{
    QDir target{QDir::current().absoluteFilePath(dep_p.name)};
    if (!target.exists()) {
        // nothing - is it an error?
        return;
    }

    /// @todo save content in temp directory ; if operation fail restore it
    if (!target.removeRecursively()) {
        throw std::runtime_error(
            QString("Git Remove: delete [%1]  something went wrong...").arg(target.path()).toStdString());
    }
}


void GitInstaller::Replace(const TDepend &old_p, const TDepend &new_p, const QUrl &url_p)
{
    (void)old_p;
    (void)url_p;
    /// @todo check if about to remove is not modified, in the expected version....

    if(!url_p.isValid()){
        throw std::runtime_error(QString("Git Replace: dependancy %1_%2 is not registered in backend").arg(new_p.name,new_p.version).toStdString());
    }

    m_git->Checkout(new_p.version, QDir::current());
}


Url GitInstaller::GetRepositoryUrl(QDir dir) const
{
    return {Url::git, m_git->GetRepositoryUrl(dir)};
}
