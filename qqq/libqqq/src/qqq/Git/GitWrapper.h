#ifndef GITWRAPPER_H
#define GITWRAPPER_H



#include <QDir>
#include <QUrl>
///
/// \brief The ISvnWrapper class
/// Lazy check of svn command is in the path
/// avoid unnecessary trouble if using only git or local storage
class IGitWrapper
{
public:
    IGitWrapper();

    virtual void Clone(const QString name_p, const QUrl& url_p,QDir dir_p)=0;

    virtual void Checkout(const QString& version_p, QDir dir_p)=0;

    virtual QUrl GetRepositoryUrl(QDir dir_p)const =0;
};

class GitWrapper:public IGitWrapper{


public:
    GitWrapper();
    GitWrapper(QString programm_p);

    void Clone(const QString name_p,const QUrl &url_p, QDir dir_p)override;
    void Checkout(const QString& version_p, QDir dir_p)override;
    QUrl GetRepositoryUrl(QDir dir_p) const override;

    void CheckProgram() const;

protected:
    QString m_programm;
    mutable bool m_is_checked;
};

#endif // GITWRAPPER_H
