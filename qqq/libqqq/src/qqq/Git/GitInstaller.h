#ifndef GITINSTALLER_H
#define GITINSTALLER_H

#include "qqq/Api/IDirRepositoryUrlFinder.h"
#include "qqq/Api/installprotocolselector.h"
class IGitWrapper;
class GitInstaller: public IInstallerProtocol,public IDirectoryRepositoryUrlFinder
{
public:
    GitInstaller(IGitWrapper *git);

    // IInstallerProtocol interface
public:
    bool CanHandle(Url::Protocol protocole_p) const override;
    void Fetch(const TDepend &dep_p, const QUrl &url_p) override;
    void Remove(const TDepend &dep_p) override;
    void Replace(const TDepend &old_p, const TDepend &new_p, const QUrl &url_p)override;

    // IDirecoryRepositoryUrlFinder interface
public:
    Url GetRepositoryUrl(QDir dir) const override;
protected:
    IGitWrapper* m_git;

};

#endif // GITINSTALLER_H
