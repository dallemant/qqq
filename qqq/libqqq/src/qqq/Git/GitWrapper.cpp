#include <QProcess>
#include "qqq/Api/Exceptions.h"
#include "GitWrapper.h"

IGitWrapper::IGitWrapper()
{
}

GitWrapper::GitWrapper() : m_programm("git"), m_is_checked(false)
{
}

GitWrapper::GitWrapper(QString programm_p) : m_programm(programm_p), m_is_checked(false)
{
}

void
GitWrapper::Clone(const QString name_p, const QUrl& url_p, QDir dir_p)
{

  CheckProgram();

  QProcess p;

  p.setProgram(m_programm);

  p.setWorkingDirectory(dir_p.path());
  p.setArguments({ "clone", url_p.toString(), name_p });

  p.start();

  if (!p.waitForStarted()) {
    throw std::runtime_error(
      QString("Git Clone, failed to start  command:%1").arg(p.errorString()).toStdString());
  }


  if (!p.waitForFinished(30000000)) {
    throw std::runtime_error(
      QString("Git Clone, operation timeout :%2").arg(p.exitCode()).arg(p.errorString()).toStdString());
  }
}

void
GitWrapper::Checkout(const QString& version_p, QDir dir_p)
{
    QProcess p;

    p.setProgram(m_programm);

    p.setWorkingDirectory(dir_p.path());
    p.setArguments({ "checkout", version_p });

    p.start();

    if (!p.waitForStarted()) {
      throw std::runtime_error(
        QString("Git Checkout, failed to start  command:%1").arg(p.errorString()).toStdString());
    }


    if (!p.waitForFinished(30000000)) {
      throw std::runtime_error(
        QString("Git Checkout, operation timeout :%2").arg(p.exitCode()).arg(p.errorString()).toStdString());
    }
}


QUrl GitWrapper::GetRepositoryUrl(QDir dir_p) const
{//git remote get-url orig
    CheckProgram();


    QProcess p;

    p.setProgram(m_programm);

    p.setWorkingDirectory(dir_p.absolutePath());
    //git config --get remote.origin.url
    p.setArguments({ "config", "--get", "remote.origin.url" });

    p.start();

    if (!p.waitForStarted()) {
      throw std::runtime_error(
        QString("Git GetRepositoryUrl, failed to start  command:%1").arg(p.errorString()).toStdString());
    }

    if (!p.waitForFinished(30000000)) {
      throw std::runtime_error(
        QString("Git GetRepositoryUrl, operation timeout :%2").arg(p.exitCode()).arg(p.errorString()).toStdString());
    }

    if(p.exitCode() != 0){
        throw std::runtime_error(
          QString("Git GetRepositoryUrl, operation failed %1 :%2").arg(p.exitCode()).arg(p.readAllStandardError().data()).toStdString());
    }

    return QUrl( p.readAllStandardOutput().simplified());//remove trailing char
}


void
GitWrapper::CheckProgram()const
{
  if (m_is_checked)
    return;

  QProcess p;
  p.setProgram(m_programm);

  p.setArguments({ "--version"});
  p.setProcessChannelMode(QProcess::ForwardedChannels);
  p.start();
  if (!p.waitForStarted()) {
    throw std::runtime_error(QString("Git check command, failed to start command:\n %1\n%2\n  "
                                     "add it on the path or set full path with command: qqq "
                                     "set_git_path  [path/to/git]")
                               .arg(p.errorString())
                             .arg(m_programm)
                               .toStdString());
  }

  ;

  if (!p.waitForFinished(1000)) {
    std::runtime_error(QString("Git check command,git program not found (command = %1) "
                               "add it on the path or set full path with command: qqq "
                               "set_svn_path  [path/to/svn]")
                         .arg(m_programm)
                         .toStdString());
  }


  if(p.exitCode() != 0){
      std::runtime_error(QString("Git check command,git program failed (command = %1) "
                                 "add it on the path or set full path with command: qqq "
                                 "set_svn_path  [path/to/svn]")
                           .arg(m_programm)
                           .toStdString());
  }

  m_is_checked = true;
}


