#ifndef TLOCALTREE_H
#define TLOCALTREE_H

#include <vector>
#include <QDir>
#include "qqq/Package/TPackage.h"
/// @class TLocalTree
/// @brief represente la liste de dependances installees
/// dans le repertoire
class TLocalTree {
   public:
    ///
    /// \brief TLocalTree walk first level of directories to find all package.json
    /// \param dir_p
    ///
    explicit TLocalTree(QDir dir_p);
    ///
    /// \brief TLocalTree directly inject depends
    /// \param deps_p
    ///
    explicit TLocalTree(const std::vector<TDepend>& deps_p);

    int GetCount() const;

    std::vector<TDepend> GetDepends() const;

    /// @return liste des dependances presentent dans this mais pas dans  deps_p
    std::vector<TDepend> GetMissingDepends(const TLocalTree& deps_p) const;

    /// @return liste des dependances presentent dans this et dans deps_p
    /// dont les version sont differents
    std::vector<QString> GetConflictDepends(const TLocalTree& deps_p) const;

    TDepend GetDependsFromName(const QString& name_p) const;

   private:
    void Sort();
    void Sort(std::vector<TDepend>&) const;
    // parcours des repertoire de dir sans descendre dans les sous-repertoire
    // et construit la liste des dependance de niveau 1
    void Walk(QDir dir_p);

    std::vector<TDepend> m_depends;
};

#endif  // TLOCALTREE_H
