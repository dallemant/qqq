#ifndef TPACKAGEJSON_H
#define TPACKAGEJSON_H
#include <QDir>
#include <QString>
#include <QJsonObject>
struct TPackage;

namespace PackageJson {
static const QString FileName("package.json");

void Save(const TPackage& package_p);
void Save(const QDir& dir_p, const TPackage& package_p);
QJsonObject ToJsonObject(const TPackage& package_p);

TPackage Open();
TPackage Open(const QDir& dir_p);
TPackage Open(const QString& file_p);
TPackage Open(QFile &file_p);

TPackage FromJsonObject(const QJsonObject& object_p);
}

#endif  // TPACKAGEJSON_H
