
#include <stdexcept>
#include <QStringList>
#include "private/semver/version2.h"
#include "private/TSemanticeVersionIntersection.h"
#include "private/TSemanticVersionChecker.h"

#include "qqq/Api/Exceptions.h"
#include "TSemanticVersion.h"


const char* TSemanticVersion::VERSION_MAX_CONST_CHAR{"999.999.999"};
const QString TSemanticVersion::VERSION_MAX_STR(VERSION_MAX_CONST_CHAR);

const TSemanticVersion TSemanticVersion::Latest{"latest"};
const TSemanticVersion TSemanticVersion::Origin{"0.0.0"};

ISemanticVersion::~ISemanticVersion() {}


struct TSemanticVersion::TSemVerPrivate {
  QString version;
  semver::version semver = semver::version("0.0.0");
};

std::unique_ptr<ISemanticVersion> ISemanticVersion::FromString(const QString string_p) {
  QStringList splitted = string_p.split(" - ");
  SemanticVersionChecker c;

  switch (splitted.size()) {
    case 1: {
      QString version = string_p;
      c.RemoveSurroundingBlanks(version);
      c.CheckNotEmpty(version);
      c.CheckMinimalSize(version);
      c.CheckNoSpace(version);
      c.CheckMyMagicNumberforLatest(version);
      if (version == "latest") {
        return std::unique_ptr<ISemanticVersion>(new TSemanticVersion(TSemanticVersion::Latest));
      }
      SemanticVersionChecker::ePrefix prefix;
      QString pure_version = c.DecodePrefix(version, prefix);
      switch (prefix) {
        case SemanticVersionChecker::no_prefix:
          return std::unique_ptr<ISemanticVersion>(new TSemanticVersion(pure_version));
        case SemanticVersionChecker::gte:
        case SemanticVersionChecker::gt:
          return std::unique_ptr<ISemanticVersion>(
              new TSemanticVersionRange(TSemanticVersion(pure_version), TSemanticVersion::Latest));
        case SemanticVersionChecker::lte:
        case SemanticVersionChecker::lt:
          return std::unique_ptr<ISemanticVersion>(
              new TSemanticVersionRange(TSemanticVersion::Origin, TSemanticVersion(pure_version)));
      }
    } break;
    case 2:
      return std::unique_ptr<ISemanticVersion>(
          new TSemanticVersionRange(TSemanticVersion(splitted.at(0)), TSemanticVersion(splitted.at(1))));
    default:
      throw std::invalid_argument(QString("ISemanticVersion::FromString invalid syntax: %1")
                                  .arg(string_p)
                                  .toStdString());
  }

  throw std::runtime_error("ISemanticVersion::FromString internale error");
}

void TSemanticVersion::Check() {

  QString& version = m->version;
  SemanticVersionChecker c;
  c.RemoveSurroundingBlanks(version);
  c.CheckNotEmpty(version);
  c.CheckMinimalSize(version);
  c.CheckNoSpace(version);

  c.CheckMyMagicNumberforLatest(version);

  if (IsLatest()) {
    m->semver = VERSION_MAX;
  } else {
    SemanticVersionChecker::ePrefix prefix;
    QString pure_version = c.DecodePrefix(version, prefix);
    if(prefix != SemanticVersionChecker::no_prefix){
        throw std::runtime_error(QString("TSemanticVersion try to build fixed version from range: %1")
                                 .arg(version)
                                 .toStdString());
    }
    c.CheckSemVer(pure_version, &m->semver);
    c.CheckNotGreeterThanLatest(&m->semver);
  }
}
std::unique_ptr<ISemanticVersion> ISemanticVersion::Instersection(std::vector<const ISemanticVersion*> version_p) {
  // deal with unique_ptr hell
  std::vector<std::unique_ptr<ISemanticVersion> > cache;

  auto next = version_p.back();
  version_p.pop_back();

  for (auto element : version_p) {
    auto ret = next->Intersection(element);
    next = ret.get();
    cache.push_back(std::move(ret));
  }

  auto last = std::move(cache.back());
  cache.pop_back();
  return last;
}

ISemanticVersion::eType ISemanticVersion::Type(QString str)
{
    try{
        auto version =ISemanticVersion::FromString(str);
        if(version->IsRange()){
            return Range;
        }
        if(version->IsLatest()){
            return Latest;
        }
        return Pure;
    }catch(std::exception ){
        return String;
    }
}



TSemanticVersion::TSemanticVersion(QString str_p) : m{new TSemVerPrivate} {
  m->version = str_p;
  Check();
}

TSemanticVersion::TSemanticVersion(const TSemanticVersion& version_p) : m{new TSemVerPrivate()} {
  *m = *version_p.m;
}

TSemanticVersion& TSemanticVersion::operator=(const TSemanticVersion& other_p) {
  *m = *other_p.m;
  return *this;
}

TSemanticVersion::~TSemanticVersion() {}

QString TSemanticVersion::ToString() const {
  return m->version;
}

bool TSemanticVersion::IsRange() const {
  return false;
}

bool TSemanticVersion::IsLatest() const {
  return m->version == "latest";
}

std::unique_ptr<ISemanticVersion> TSemanticVersion::Intersection(const ISemanticVersion* version_p) const {
  return version_p->DispatchIntersection(this);
}

std::unique_ptr<ISemanticVersion> TSemanticVersion::DispatchIntersection(const TSemanticVersion* version_p) const {
  TSemanticIntersection inter;
  return inter.Intersection(version_p, this);
}

std::unique_ptr<ISemanticVersion> TSemanticVersion::DispatchIntersection(const TSemanticVersionRange* range_p) const {
  TSemanticIntersection inter;
  return inter.Intersection(this, range_p);
}

bool TSemanticVersion::operator==(const TSemanticVersion& version_p) const {
  return m->version == version_p.m->version;
}

bool TSemanticVersion::operator<(const TSemanticVersion& version_p) const {
  if (IsRange() || version_p.IsRange()) {
    throw std::invalid_argument("TSemanticVersion::operator < with range not implemented");
  }

  return m->semver < version_p.m->semver;
}

bool TSemanticVersion::operator>(const TSemanticVersion& version_p) const {
  if (IsRange() || version_p.IsRange()) {
    throw std::invalid_argument("TSemanticVersion::operator > with range not implemented");
  }

  return m->semver > version_p.m->semver;
}

bool TSemanticVersion::operator<=(const TSemanticVersion& version_p) const {
  if (IsRange() || version_p.IsRange()) {
    throw std::invalid_argument("TSemanticVersion::operator <= with range not implemented");
  }

  return (m->semver < version_p.m->semver) || (m->semver == version_p.m->semver);
}

bool TSemanticVersion::operator>=(const TSemanticVersion& version_p) const {
  if (IsRange() || version_p.IsRange()) {
    throw std::invalid_argument("TSemanticVersion::operator >= with range not implemented");
  }

  return (m->semver > version_p.m->semver) || (m->semver == version_p.m->semver);
}

////////////////////////////////////////////////////////////////////////////////
/// \brief The TSemanticVersionRange::TSemVerRangePrivate struct
///
struct TSemanticVersionRange::TSemVerRangePrivate {
  TSemanticVersion from_p;
  TSemanticVersion to_p;
};

TSemanticVersionRange::TSemanticVersionRange(const TSemanticVersion& from_p, const TSemanticVersion& to_p)
    : m(new TSemVerRangePrivate{from_p, to_p}) {
  Set(from_p, to_p);
}

void TSemanticVersionRange::Set(const TSemanticVersion& from_p, const TSemanticVersion& to_p) {
  m->from_p = from_p;
  m->to_p = to_p;

  if (m->from_p == m->to_p) {
    throw std::invalid_argument(
        qPrintable(QString("TSemanticVersionRange %1 == %2").arg(from_p.ToString(), to_p.ToString())));
  }

  if (m->from_p > m->to_p) {
    throw std::invalid_argument("TSemanticVersionRange min > max");
  }
}

TSemanticVersionRange::TSemanticVersionRange(const TSemanticVersionRange& range_p) {
  *m = *range_p.m;
}

TSemanticVersionRange& TSemanticVersionRange::operator=(const TSemanticVersionRange& range_p) {
  *m = *range_p.m;
  return *this;
}

TSemanticVersionRange::~TSemanticVersionRange() {}

QString TSemanticVersionRange::ToString() const {
  if (From() == TSemanticVersion::Origin && To() == TSemanticVersion::Latest) {
    return QString("any");
  }

  else if (From() == TSemanticVersion::Origin) {
    return QString("<%1").arg(To().ToString());

  } else if (To() == TSemanticVersion::Latest) {
    return QString(">%1").arg(From().ToString());
  }

  return QString("%1 - %2").arg(m->from_p.ToString()).arg(m->to_p.ToString());
}

TSemanticVersion TSemanticVersionRange::From() const {
  return m->from_p;
}

TSemanticVersion TSemanticVersionRange::To() const {
  return m->to_p;
}

std::unique_ptr<ISemanticVersion> TSemanticVersionRange::Intersection(const ISemanticVersion* version_p) const {
  return version_p->DispatchIntersection(this);
}

std::unique_ptr<ISemanticVersion> TSemanticVersionRange::DispatchIntersection(
    const TSemanticVersionRange* range_p) const {
  TSemanticIntersection inter;
  return inter.Intersection(range_p, this);
}

std::unique_ptr<ISemanticVersion> TSemanticVersionRange::DispatchIntersection(const TSemanticVersion* version_p) const {
  TSemanticIntersection inter;
  return inter.Intersection(version_p, this);
}
