#include "TPackage.h"

bool TPackage::operator==(const TPackage& other_p) const {
    return (name == other_p.name) && (version == other_p.version) && (depends == other_p.depends);
}

bool TPackage::operator!=(const TPackage& other_p) const {
    return !operator==(other_p);
}

bool TDepend::operator==(const TDepend& other_p) const {
    return (name == other_p.name) && (version == other_p.version);
}

bool TDepend::operator!=(const TDepend& other_p) const {
    return !operator==(other_p);
}

bool TDepend::operator<(const TDepend &other_p) const
{
    if(name < other_p.name){
        return true;
    }else if( name > other_p.name){
        return false;
    }else{
        //names are equals
        return  version < other_p.version;
    }
}

TPackage::TPackage(const TDepend &dep_p, std::vector<TDepend> depends_p)
    :name(dep_p.name)
    ,version(dep_p.version)
    ,depends(depends_p)
{

}

TPackage::TPackage(const QString &name_p, const TVersion &version_p, std::vector<TDepend> depends_p)
    :TPackage({name_p,version_p},depends_p)
{

}
