#include "TUrl.h"
#include "TSemanticVersion.h"
#include <QStringList>

QString ToVersion(QString url, QString separator) {

  QStringList splitted = url.split(separator);
  if (splitted.count() != 2) {
    return {};
  }

  QString slashed_version = splitted.at(1);

  return "_" + slashed_version.split("/", QString::SkipEmptyParts).join("_");
}

std::pair<bool, int> PeggedNumber(const QString& str){
    QStringList splitted = str.split("@");
    if (splitted.count() != 2 ) {
      return std::make_pair(false,0);
    }
    bool success = false;
    int rev = splitted.at(1).toInt(&success);

    return std::make_pair(success,rev);
}

QString Url::TagFromSubversionUrl(QUrl url) {
  if (!url.isValid()) {
    throw std::runtime_error("Url::TagFromSubversionUrl - url is invalid");
  }

  QString str = url.toString();

  int trunk = str.count("/trunk");
  int tag = str.count("/tags/");
  int branch = str.count("/branches/");

  if (trunk + tag + branch != 1) {
    throw std::runtime_error(
        QString("Url::TagFromSubversionUrl - cannot deduce tag from %1")
            .arg(str)
            .toStdString()
            .data());
  }

  if (str.endsWith("/")) {
    str.resize(str.size() - 1);
  }

  if (trunk) {
    auto option_int = PeggedNumber(str);
    if(option_int.first){
        return QString("latest@%1").arg(option_int.second);
    }else{
      return "latest";
    }
  }

  if (tag) {
    QString str_tag = str.split("/tags/").at(1);

    try {
      TSemanticVersion version(str_tag);
      return str_tag;
    } catch (...) {
      return "tags" + ToVersion(str, "/tags");
    }
  }

  if (branch) {
    return "branches" + ToVersion(str, "/branches");
  }

  throw std::runtime_error("Url::TagFromSubversionUrl - unreacheable line !!!");
}
