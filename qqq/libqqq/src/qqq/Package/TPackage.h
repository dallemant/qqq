#ifndef TPACKAGE_H
#define TPACKAGE_H


#include <vector>

#include <QString>

using TVersion = QString;

struct TDepend {
    QString name;
    TVersion version;

    bool operator==(const TDepend& other_p) const;
    bool operator!=(const TDepend& other_p) const;
    bool operator<(const TDepend& other_p) const;
};

struct TPackage {
    TPackage() = default;
    TPackage(const TDepend& dep_p,std::vector<TDepend> depends_p);
    TPackage(const QString& name_p, const TVersion& version_p, std::vector<TDepend> depends_p);
    QString name;
    TVersion version;
    std::vector<TDepend> depends;

    bool operator==(const TPackage& other_p) const;
    bool operator!=(const TPackage& other_p) const;
};

#endif  // TPACKAGE_H
