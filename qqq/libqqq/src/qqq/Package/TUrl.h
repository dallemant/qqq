#ifndef TURL_H
#define TURL_H
#include <QUrl>
struct Url
{
  enum Protocol
  {
    undef,
    local_storage,
    subversion,
    git
  };
  Protocol protocol ;
  QUrl url;

  QString ProtocolStr()const{
      switch(protocol){
      case local_storage: return "file";
      case subversion: return "subversion";
      case git: return "git";
      case undef:
      default:
          return "undef";
      }
  }

  static Protocol FromProtocol(const QString str){
      if(str == "file"){
          return local_storage;
      }else if (str == "subversion"){
          return subversion;
      }else if (str == "git"){
          return git;
      }else{
          return undef;
      }
  }



  QString UrlStr()const{ return url.toString();}
  bool operator ==(const Url& other_p)const{
      return (protocol == other_p.protocol) && (url == other_p.url);
  }

  static QString TagFromSubversionUrl(QUrl url);

};

#endif // TURL_H
