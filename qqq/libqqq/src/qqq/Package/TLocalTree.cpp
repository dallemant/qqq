#include <algorithm>
#include <stdexcept>
#include <QDirIterator>
#include "TPackageJson.h"
#include "TLocalTree.h"

TLocalTree::TLocalTree(QDir dir_p) {
    if (!dir_p.exists()) {
        throw std::runtime_error(QString("TLocalTree::TLocalTree(%1) n'existe pas").arg(dir_p.path()).toStdString());
    }
    Walk(dir_p);
}

TLocalTree::TLocalTree(const std::vector<TDepend>& deps_p) {
    m_depends = deps_p;
    Sort();
}

int TLocalTree::GetCount() const {
    return static_cast<int>(m_depends.size());
}

std::vector<TDepend> TLocalTree::GetDepends() const {
    return m_depends;
}

std::vector<TDepend> TLocalTree::GetMissingDepends(const TLocalTree& deps_p) const {
    std::vector<TDepend> out(m_depends.size());

    auto last =
        std::set_difference(m_depends.begin(), m_depends.end(), deps_p.m_depends.begin(), deps_p.m_depends.end(),
                            out.begin(), [](const TDepend a, const TDepend b) { return a.name < b.name; });
    out.resize(std::distance(out.begin(), last));
    return out;
}

std::vector<QString> TLocalTree::GetConflictDepends(const TLocalTree& deps_p) const {
    auto intersect = [](const std::vector<TDepend>& a, const std::vector<TDepend>& b) {
        std::vector<TDepend> out(a.size());
        auto last = std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), out.begin(),
                                          [](const TDepend a_, const TDepend b_) { return a_.name < b_.name; });
        out.resize(std::distance(out.begin(), last));
        return out;
    };

    // liste des deps de this presentes dans deps_p
    auto a = intersect(m_depends, deps_p.m_depends);
    // liste des deps de deps_p presentes dans  this
    auto b = intersect(deps_p.m_depends, m_depends);

    // set_difference contract
    Sort(a);
    Sort(b);

    std::vector<TDepend> out(a.size());

    auto last = std::set_difference(a.begin(), a.end(), b.begin(), b.end(), out.begin(),
                                    [](const TDepend a, const TDepend b) { return a.version < b.version; });
    out.resize(std::distance(out.begin(), last));

    std::vector<QString> ret;
    for (const auto& dep : out) {
        ret.push_back(dep.name);
    }

    return ret;
}

TDepend TLocalTree::GetDependsFromName(const QString& name_p) const {
    auto it = std::find_if(m_depends.begin(), m_depends.end(), [name_p](const TDepend& dep) {
        return dep.name == name_p;
    });

    if (it != m_depends.end()) {
        return *it;
    } else {
        return {{}, {}};
    }
}

void TLocalTree::Sort() {
    Sort(m_depends);
}

void TLocalTree::Sort(std::vector<TDepend>& dep_p) const {
    std::sort(dep_p.begin(), dep_p.end(),
              [](const TDepend& depA, const TDepend& depB) { return depA.name < depB.name; });
}

void TLocalTree::Walk(QDir dir_p) {
    QDirIterator it(dir_p);
    while (it.hasNext()) {
        QString d = it.next();
        try {
            TPackage p = PackageJson::Open(QDir{d});
            m_depends.push_back(TDepend{p.name, p.version});

        } catch (std::runtime_error /*e*/) {
            // lecture du package echoue
        }
    }

    Sort();
}
