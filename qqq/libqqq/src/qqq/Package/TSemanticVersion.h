#ifndef TSEMANTICVERSION_H
#define TSEMANTICVERSION_H
#include <memory>
#include <vector>
#include <QString>

class TSemanticVersionRange;
class TSemanticVersion;

///
/// \brief The ISemanticVersion class
///
class ISemanticVersion {
   public:
    virtual ~ISemanticVersion();
    virtual QString ToString() const = 0;    

    virtual bool IsRange() const = 0;
    virtual bool IsLatest() const = 0;

    ///
    /// \brief FromString create version from string
    /// \param string_p
    /// \return
    ///
    static std::unique_ptr<ISemanticVersion> FromString(const QString string_p);
    static std::unique_ptr<ISemanticVersion> Instersection(std::vector<const ISemanticVersion*> version_p);

    enum eType{
        Pure,//1.2.3
        Range,//1.2.3 - 3.4.5
        Latest,//latest
        String//branch/feature
    };

    static eType Type(QString str);


    public:
    virtual std::unique_ptr<ISemanticVersion> Intersection(const ISemanticVersion* version_p) const = 0;
    virtual std::unique_ptr<ISemanticVersion> DispatchIntersection(const TSemanticVersion* version_p) const = 0;
    virtual std::unique_ptr<ISemanticVersion> DispatchIntersection(const TSemanticVersionRange* version_p) const = 0;

};

using UISemanticVersion = std::unique_ptr<ISemanticVersion>;
using UISemanticVersionVector = std::vector<std::unique_ptr<ISemanticVersion>>;
using ISemanticVersionConstVector = std::vector<const ISemanticVersion*>;




///
/// \brief The TSemanticVersion class
///
class TSemanticVersion : public ISemanticVersion {
   public:


    TSemanticVersion(QString str_p);
    TSemanticVersion(const TSemanticVersion& version_p);
    TSemanticVersion& operator=(const TSemanticVersion& other_p);
    virtual ~TSemanticVersion();
    virtual QString ToString() const override;
    bool IsRange() const override;
    virtual bool IsLatest() const override;


    virtual std::unique_ptr<ISemanticVersion> Intersection(const ISemanticVersion* version_p) const override;

    bool operator==(const TSemanticVersion& version_p) const;
    bool operator<(const TSemanticVersion& version_p) const;
    bool operator>(const TSemanticVersion& version_p) const;
    bool operator<=(const TSemanticVersion& version_p) const;
    bool operator>=(const TSemanticVersion& version_p) const;

    static const char*VERSION_MAX_CONST_CHAR;
    static const QString VERSION_MAX_STR;
    static const TSemanticVersion Latest;
    static const TSemanticVersion Origin;

   public:
    virtual std::unique_ptr<ISemanticVersion> DispatchIntersection(const TSemanticVersion* version_p) const override;
    virtual std::unique_ptr<ISemanticVersion> DispatchIntersection(const TSemanticVersionRange* range_p) const override;

   private:
    void Check();
    struct TSemVerPrivate;
    std::unique_ptr<TSemVerPrivate> m;
};

///
/// \brief The TSemanticVersionRange class
///
class TSemanticVersionRange : public ISemanticVersion {
   public:
    TSemanticVersionRange(const TSemanticVersion& from_p, const TSemanticVersion& to_p);
    TSemanticVersionRange(const TSemanticVersionRange& range_p);
    TSemanticVersionRange& operator=(const TSemanticVersionRange& range_p);
    virtual ~TSemanticVersionRange();
    virtual QString ToString() const override;
    bool IsRange() const override{return true;}
    virtual bool IsLatest() const override{return false;}

    struct TSemVerRangePrivate;
    std::unique_ptr<TSemVerRangePrivate> m;

    TSemanticVersion From() const;
    TSemanticVersion To() const;

    virtual std::unique_ptr<ISemanticVersion> Intersection(const ISemanticVersion* version_p) const override;

    // ISemanticVersion interface
   public:
    std::unique_ptr<ISemanticVersion> DispatchIntersection(const TSemanticVersionRange* range_p) const override;
    std::unique_ptr<ISemanticVersion> DispatchIntersection(const TSemanticVersion* version_p) const override;

protected:
    void Set(const TSemanticVersion& from_p, const TSemanticVersion& to_p);
};

using UISemanticVersionRange = std::unique_ptr<TSemanticVersionRange>;

#endif  // TSEMANTICVERSION_H
