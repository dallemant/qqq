#include <initializer_list>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <QDir>
#include "qqq/Package/TPackage.h"
#include "TPackageJson.h"

void PackageJson::Save(const TPackage& package_p) {
    Save(QDir::current(), package_p);
}

void PackageJson::Save(const QDir& dir_p, const TPackage& package_p) {
    auto json = ToJsonObject(package_p);

    QJsonDocument doc(json);

    auto filepath = dir_p.absoluteFilePath(FileName);

    QFile pack_file(filepath);
    if (!pack_file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        throw std::runtime_error(QString(" TPackageJSon::Save :cannot open file %1 : %2")
                                     .arg(filepath)
                                     .arg(pack_file.errorString())
                                     .toStdString());
    }

    const auto bytes = doc.toJson();
    const auto written = pack_file.write(bytes);
    if (written != bytes.size()) {
        throw std::runtime_error(QString(" TPackageJSon::Save :cannot write in file %1 : %2/%3 bytes written")
                                     .arg(filepath)
                                     .arg(written)
                                     .arg(bytes.size())
                                     .toStdString());
    }
}

TPackage PackageJson::Open() {
    return Open(QDir::current());
}

TPackage PackageJson::Open(const QDir& dir_p) {
    QString filename = dir_p.absoluteFilePath(FileName);

    return Open(filename);
}

TPackage PackageJson::Open(const QString& file_p) {
    QFile f(file_p);
    return Open(f);
}

TPackage PackageJson::Open(QFile& file_p) {
    if (!file_p.exists()) {
        throw std::runtime_error(QString(" Open::Open : file %1 from working dir %2 does not exist")
                                     .arg(file_p.fileName())
                                     .arg(QDir::currentPath())
                                     .toStdString());
    }

    if (!file_p.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(QString(" TPackageJSon::Open :cannot open file %1 : %2")
                                     .arg(file_p.fileName())
                                     .arg(file_p.errorString())
                                     .toStdString());
    }

    QJsonParseError error;
    auto doc = QJsonDocument::fromJson(file_p.readAll(), &error);

    if (error.error != QJsonParseError::NoError) {
        throw std::runtime_error(QString(" TPackageJSon::Open :parse json error %1 : %2")
                                     .arg(file_p.fileName())
                                     .arg(error.errorString())
                                     .toStdString());
    }

    return FromJsonObject(doc.object());
}

TPackage PackageJson::FromJsonObject(const QJsonObject& object_p) {
    TPackage pack;

    pack.name = object_p["name"].toString();
    pack.version = object_p["version"].toString();

    const QJsonObject deps = object_p["dependencies"].toObject();

    auto it = deps.begin();
    for (; it != deps.end(); it++) {
        pack.depends.push_back(TDepend{it.key(), it.value().toString()});
    }
    return pack;
}

QJsonObject PackageJson::ToJsonObject(const TPackage& package_p) {
    QJsonObject json;

    QJsonObject deps;
    for (const auto& dep : package_p.depends) {
        deps.insert(dep.name, dep.version);
    }

    json["dependencies"] = deps;
    json["version"] = package_p.version;
    json["name"] = package_p.name;
    return json;
}
