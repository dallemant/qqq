#include "TSemanticeVersionIntersection.h"
#include "qqq/Api/Exceptions.h"

std::unique_ptr<ISemanticVersion> TSemanticIntersection::Intersection(const TSemanticVersion* version_p,
                                                                      const TSemanticVersionRange* range_p) const {
  Q_ASSERT(!version_p->IsRange());

  if (*version_p < range_p->From()) {
    throw std::invalid_argument("no intersection");
  }

  if (*version_p > range_p->To()) {
    throw std::invalid_argument("no intersection");
  }

  return std::unique_ptr<ISemanticVersion>(new TSemanticVersion(*version_p));
}

std::unique_ptr<ISemanticVersion> TSemanticIntersection::Intersection(const TSemanticVersion* left_p,
                                                                      const TSemanticVersion* right_p) const {
  Q_ASSERT(!left_p->IsRange());
  Q_ASSERT(!right_p->IsRange());

  if (*left_p == *right_p) {
    return std::unique_ptr<ISemanticVersion>(new TSemanticVersion(*left_p));
  }
  throw std::invalid_argument("no intersection");
}

std::unique_ptr<ISemanticVersion> TSemanticIntersection::Intersection(const TSemanticVersionRange* left_p,
                                                                      const TSemanticVersionRange* right_p) const {
  if (left_p->To() < right_p->From()) {
    throw std::invalid_argument("no intersection");
  }
  if (right_p->To() < left_p->From()) {
    throw std::invalid_argument("no intersection");
  }
  TSemanticVersion from = (left_p->From() > right_p->From()) ? left_p->From() : right_p->From();
  TSemanticVersion to = (left_p->To() < right_p->To()) ? left_p->To() : right_p->To();

  if (from == to) {
    return std::unique_ptr<ISemanticVersion>(new TSemanticVersion(from));
  } else {
    return std::unique_ptr<ISemanticVersion>(new TSemanticVersionRange(from, to));
  }
}
