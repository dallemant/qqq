#ifndef TSEMANTICEVERSIONINTERSECTION_H
#define TSEMANTICEVERSIONINTERSECTION_H
#include <memory>
#include "../TSemanticVersion.h"

///
/// \brief The TSemanticIntersection struct
///
struct TSemanticIntersection {
    std::unique_ptr<ISemanticVersion> Intersection(const TSemanticVersion* version_p,
                                                   const TSemanticVersionRange* range_p) const;
    std::unique_ptr<ISemanticVersion> Intersection(const TSemanticVersion* left_p,
                                                   const TSemanticVersion* right_p) const;
    std::unique_ptr<ISemanticVersion> Intersection(const TSemanticVersionRange* left_p,
                                                   const TSemanticVersionRange* right_p) const;
};

#endif // TSEMANTICEVERSIONINTERSECTION_H
