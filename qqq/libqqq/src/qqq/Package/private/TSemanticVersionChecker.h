#ifndef TSEMANTICVERSIONCHECKER_H
#define TSEMANTICVERSIONCHECKER_H

#include "../TSemanticVersion.h"
#include "semver/version2.h"

static const semver::version VERSION_MAX("999.999.999");

///
/// \brief The SemanticVersionChecker struct
///
struct SemanticVersionChecker {
    enum ePrefix { no_prefix, gt, gte, lt, lte, /*tilde, caret */};


    void RemoveSurroundingBlanks(QString& version_p) { version_p = version_p.simplified(); }

    void CheckNotEmpty(const QString version_p);

    void CheckMinimalSize(const QString version_p);

    void CheckNoSpace(const QString version_p);

    void CheckMyMagicNumberforLatest(QString &version_p);

    void CheckNotGreeterThanLatest(semver::version* semver_p);

    QString DecodePrefix(const QString& version_p, ePrefix& prefix_p);

    void CheckSemVer(const QString& pure_version_p, semver::version* semver_p);
};
#endif // TSEMANTICVERSIONCHECKER_H
