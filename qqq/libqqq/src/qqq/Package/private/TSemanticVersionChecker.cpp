#include "TSemanticVersionChecker.h"


void SemanticVersionChecker::CheckNotEmpty(const QString version_p) {
    if (version_p.isEmpty()) {
        throw std::invalid_argument("empty string");
    }
}

void SemanticVersionChecker::CheckMinimalSize(const QString version_p) {
    // minimal semver is 5 (x.y.z)
    if (version_p.size() < 5) {
        throw std::invalid_argument(
                    qPrintable(QString("SemanticVersionChecker::too short string : %1").arg(version_p)));
    }
}

void SemanticVersionChecker::CheckNoSpace(const QString version_p) {
    if (version_p.contains(" ")) {
        throw std::invalid_argument("contains spaces");
    }
}

///
/// \brief SemanticVersionChecker::CheckSemVer
/// \param pure_version_p
///
void SemanticVersionChecker::CheckMyMagicNumberforLatest(QString& version_p){
    if(version_p ==TSemanticVersion::VERSION_MAX_STR){
        version_p = "latest";
    }
}

void SemanticVersionChecker::CheckNotGreeterThanLatest(semver::version *semver_p){
    static const semver::version max{"999.999.999"};

    if(*semver_p > max ){
        throw std::invalid_argument(QString(" version greater than %1 is not allowed").arg(TSemanticVersion::VERSION_MAX_STR).toStdString());
    }
}

void SemanticVersionChecker::CheckSemVer(const QString& pure_version_p, semver::version* semver_p) {
    *semver_p = semver::version(pure_version_p.toStdString());
    if(!semver_p->isValid()){
        throw std::invalid_argument(QString(" version invalid %1").arg(pure_version_p).toStdString());
    }
}

QString SemanticVersionChecker::DecodePrefix(const QString &version_p, ePrefix &prefix_p) {
    // extract prefix if present
    int prefix_size = 0;
    QChar first_char = version_p[0];
    QChar second_char = version_p[1];
    if (!first_char.isDigit()) {
        prefix_size = 1;
        switch (version_p[0].toLatin1()) {
        case '>':
            if (second_char == '=') {
                prefix_size = 2;
                prefix_p = gte;
            } else {
                prefix_p = gt;
            }
            break;
        case '<':
            if (second_char == '=') {
                prefix_size = 2;
                prefix_p = lte;
            } else {
                prefix_p = lt;
            }
            break;
        /*case '~':
            prefix_p = TSemanticVersion::tilde;
            break;
        case '^':
            prefix_p = TSemanticVersion::caret;
            break;*/
        default:
            throw std::invalid_argument("bad prefix nor digit nor > >= < <= ");
        }
    }else{
        prefix_p = no_prefix;
    }
    return version_p.mid(prefix_size);
}
