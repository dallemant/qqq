TEMPLATE = subdirs

SUBDIRS += \
    libqqq tests qt5_deploy demo \
    console_app

tests.depends = libqqq
console_app.depends = libqqq
