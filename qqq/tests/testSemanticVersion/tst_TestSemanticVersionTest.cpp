#include <QRegExp>
#include <QString>
#include <QtTest>

#include <qqq/Package/TSemanticVersion.h>
#include <qqq/Package/private/TSemanticeVersionIntersection.h>
#include <qqq/Package/TUrl.h>
class TestSemanticVersionTest : public QObject {
  Q_OBJECT

 public:
  TestSemanticVersionTest();

 private Q_SLOTS:

  void testSemVer_data();
  void testSemVer();

  void testComp_data();
  void testComp();

  void testIntersectionRangeHelper_data();
  void testIntersectionRangeHelper();

  void testIntersectionRangeAndFixedHelper_data();
  void testIntersectionRangeAndFixedHelper();

  void testIntersection_data();
  void testIntersection();

  void testSvnTag_data();
  void testSvnTag();

  void testInvalidSvnTag_data();
  void testInvalidSvnTag();
};

TestSemanticVersionTest::TestSemanticVersionTest() {}

void TestSemanticVersionTest::testSemVer_data() {
  QTest::addColumn<QString>("version_str");
  QTest::addColumn<bool>("is_valid");

  auto NewRow = [](QString str, bool is_valid) { QTest::newRow(qPrintable(str)) << str << is_valid; };

  NewRow("1.2.3", true);
  NewRow("0.0.0", true);
  NewRow(">1.2.3", true);
  NewRow("<1.2.3", true);
  NewRow(">=1.2.3", true);
  NewRow("<=1.2.3", true);


  NewRow("<=1.2.3", true);
  NewRow("  <=1.2.3  ", true);
  NewRow("1.2.3-rc1", true);
  NewRow("1.2.3 - 4.5.6", true);
  NewRow(" 1.2.3 - 4.5.6  ", true);
  NewRow(" latest  ", true);

  NewRow("1.2", false);
  NewRow("1.2.x", false);
  NewRow("text", false);
  NewRow("text andspace", false);
  NewRow("4.5.6 - 1.2.3", false);
  NewRow(" 4.5.6 - 4.5.6", false);
  NewRow("~1.2.3", false);
  NewRow("^1.2.3", false);
}

void TestSemanticVersionTest::testSemVer() {
  QFETCH(QString, version_str);
  QFETCH(bool, is_valid);
  if (is_valid) {
    auto v = ISemanticVersion::FromString(version_str);
  } else {
#if QT_VERSION >= 0x050300
    QVERIFY_EXCEPTION_THROWN(ISemanticVersion::FromString(version_str), std::invalid_argument);
#endif
  }
}

void TestSemanticVersionTest::testIntersection_data() {
  QTest::addColumn<QStringList>("versions");
  QTest::addColumn<QString>("result");
  QTest::addColumn<bool>("exists");

  QTest::newRow("included") << QStringList{"1.0.0 - 4.0.0", "2.0.0 - 3.0.0"} << "2.0.0 - 3.0.0" << true;
  QTest::newRow("intersect") << QStringList{"1.0.0 - 3.0.0", "2.0.0 - 4.0.0"} << "2.0.0 - 3.0.0" << true;
  QTest::newRow("intersect limit") << QStringList{"1.0.0 - 3.0.0", "3.0.0 - 4.0.0"} << "3.0.0" << true;
  QTest::newRow("no intersect") << QStringList{"1.0.0 - 2.0.0", "3.0.0 - 4.0.0"} << "0.0.0" << false;

  QTest::newRow("intersect + fixed ") << QStringList{"1.0.0 - 3.0.0", "2.0.0 - 4.0.0", "2.3.4"} << "2.3.4" << true;

  QTest::newRow("intersect with semi_interval") << QStringList{">2.0.0","1.0.0 - 4.0.0"}<<"2.0.0 - 4.0.0" << true;

  QTest::newRow("intersect range with latest") << QStringList{">2.0.0","latest"}<<"latest" << true;

  QTest::newRow("intersect opposite semi range ") << QStringList{">2.0.0","<3.0.0"}<< "2.0.0 - 3.0.0" << true;
}

void TestSemanticVersionTest::testIntersection() {
  QFETCH(QStringList, versions);
  QFETCH(bool, exists);
  QFETCH(QString, result);

  UISemanticVersion expect_semver = ISemanticVersion::FromString(result);
  UISemanticVersionVector vect;
  ISemanticVersionConstVector vect_raw;
  for (QString& str : versions) {
    auto u = ISemanticVersion::FromString(str);
    vect_raw.push_back(u.get());
    vect.push_back(std::move(u));
  }

  if (exists) {
    QCOMPARE(ISemanticVersion::Instersection(vect_raw)->ToString(), expect_semver->ToString());
  } else {
#if QT_VERSION >= 0x050300
    QVERIFY_EXCEPTION_THROWN(ISemanticVersion::Instersection(vect_raw), std::exception);
#endif
  }
}



void TestSemanticVersionTest::testComp_data() {
  QTest::addColumn<QString>("left");
  QTest::addColumn<QString>("right");
  QTest::addColumn<int>("result");

  QTest::newRow("<") << "1.0.0"
                     << "2.0.0" << -1;
  QTest::newRow("<") << "1.0.0"
                     << "latest" << -1;
  QTest::newRow(">") << "2.0.0"
                     << "1.0.0" << 1;
  QTest::newRow(">") << "latest"
                     << "1.0.0" << 1;
  QTest::newRow("==") << "2.0.0"
                      << "2.0.0" << 0;
  QTest::newRow("==") << "latest"
                      << "latest" << 0;

 // QTest::newRow("< rc")<<"1.0.0-rc1"<<"1.0.0"<< -1;
 // QTest::newRow("> rc")<<"2.0.0"<<"2.0.0-rc"<< 1;
}

void TestSemanticVersionTest::testComp() {
  QFETCH(QString, left);
  QFETCH(QString, right);
  QFETCH(int, result);

  auto v_l = ISemanticVersion::FromString(left);
  auto v_r = ISemanticVersion::FromString(right);

  switch (result) {
    case -1:
      QVERIFY(left < right);
      QVERIFY((left <= right));
      QVERIFY(!(left >= right));
      QVERIFY(!(left > right));
      QVERIFY(!(left == right));
      break;
    case 0:
      QVERIFY(left == right);
      QVERIFY((left <= right));
      QVERIFY((left >= right));
      QVERIFY(!(left > right));
      QVERIFY(!(left < right));
      break;
    case 1:
      QVERIFY(left > right);
      QVERIFY((left >= right));
      QVERIFY(!(left <= right));
      QVERIFY(!(left < right));
      QVERIFY(!(left == right));
      break;
  }
}


void TestSemanticVersionTest::testIntersectionRangeAndFixedHelper_data() {
  QTest::addColumn<QString>("left");
  QTest::addColumn<QString>("right");
  QTest::addColumn<QString>("result");
  QTest::addColumn<bool>("exists");


  QTest::newRow("no intersect with fixed") << "1.0.0"
                                           << "3.0.0 - 4.0.0"
                                           << "" << false;
  QTest::newRow("no intersect with latest") << "latest"
                                           << "3.0.0 - 4.0.0"
                                           << "" << false;
  QTest::newRow("intersect with fixed") << "2.0.0"
                                        << "1.0.0 - 4.0.0"
                                        << "2.0.0" << true;




}
void TestSemanticVersionTest::testIntersectionRangeAndFixedHelper() {
  QFETCH(QString, left);
  QFETCH(QString, right);
  QFETCH(QString, result);
  QFETCH(bool, exists);

  TSemanticVersion v_l(left);
  TSemanticVersionRange v_r(right.split(" - ").at(0), right.split(" - ").at(1));
  // qDebug()<<"left: "<<v_l.ToString()<<" from::"<<v_l.From().ToString()<<" to: "<< v_l.To().ToString();
  // qDebug()<<"right: "<<v_r.ToString()<<" from::"<<v_r.From().ToString()<<" to: "<< v_r.To().ToString();

  TSemanticIntersection intersect_helper;

  if (exists) {
    auto ret = intersect_helper.Intersection(&v_l, &v_r);
    QCOMPARE(ret->ToString(), result);
  } else {
#if QT_VERSION >= 0x050300
    QVERIFY_EXCEPTION_THROWN(intersect_helper.Intersection(&v_l, &v_r), std::exception);
#endif
  }
}


void TestSemanticVersionTest::testIntersectionRangeHelper_data() {
  QTest::addColumn<QString>("left");
  QTest::addColumn<QString>("right");
  QTest::addColumn<QString>("result");
  QTest::addColumn<bool>("exists");

  QTest::newRow("intersect") << "1.0.0 - 3.0.0"
                             << "2.0.0 - 4.0.0"
                             << "2.0.0 - 3.0.0" << true;

  QTest::newRow("included") << "1.0.0 - 4.0.0"
                            << "2.0.0 - 3.0.0"
                            << "2.0.0 - 3.0.0" << true;
  QTest::newRow("intersect limit") << "1.0.0 - 3.0.0"
                                   << "3.0.0 - 4.0.0"
                                   << "3.0.0" << true;
  QTest::newRow("no intersect") << "1.0.0 - 2.0.0"
                                << "3.0.0 - 4.0.0"
                                << "" << false;
}
void TestSemanticVersionTest::testIntersectionRangeHelper() {
  QFETCH(QString, left);
  QFETCH(QString, right);
  QFETCH(QString, result);
  QFETCH(bool, exists);

  TSemanticVersionRange v_l(left.split(" - ").at(0), left.split(" - ").at(1));
  TSemanticVersionRange v_r(right.split(" - ").at(0), right.split(" - ").at(1));
  // qDebug()<<"left: "<<v_l.ToString()<<" from::"<<v_l.From().ToString()<<" to: "<< v_l.To().ToString();
  // qDebug()<<"right: "<<v_r.ToString()<<" from::"<<v_r.From().ToString()<<" to: "<< v_r.To().ToString();

  TSemanticIntersection intersect_helper;

  if (exists) {
    auto ret = intersect_helper.Intersection(&v_l, &v_r);
    QCOMPARE(ret->ToString(), result);
  } else {
#if QT_VERSION >= 0x050300
    QVERIFY_EXCEPTION_THROWN(intersect_helper.Intersection(&v_l, &v_r), std::exception);
#endif
  }
}


void TestSemanticVersionTest::testSvnTag_data()
{
    QTest::addColumn<QUrl>("url");
    QTest::addColumn<QString>("tag");

    QTest::newRow("trunk")<<QUrl("svn://repo/aa/bb/trunk")<<"latest";
    QTest::newRow("trunk extended")<<QUrl("svn://repo/aa/bb/trunk/Des/libDes")<<"latest";
    QTest::newRow("trunk pegged")<<QUrl("svn://repo/aa/bb/trunk@640")<<"latest@640";

    QTest::newRow("tag")<<QUrl("svn://repo/aa/bb/tags/1.2.3")<<"1.2.3";
    QTest::newRow("tag extended")<<QUrl("svn://repo/aa/bb/tags/1.2.3/Des/libDes")<<"tags_1.2.3_Des_libDes";

    QTest::newRow("branch")<<QUrl("svn://repo/aa/bb/branches/newFeature")<<"branches_newFeature";
    QTest::newRow("branch pegged")<<QUrl("svn://repo/aa/bb/branches/newFeature@640")<<"branches_newFeature@640";
}

void TestSemanticVersionTest::testSvnTag()
{
    QFETCH(QUrl,url);
    QFETCH(QString,tag);

    QCOMPARE(Url::TagFromSubversionUrl(url),tag);

}

void TestSemanticVersionTest::testInvalidSvnTag_data()
{
    QTest::addColumn<QUrl>("url");

    QTest::newRow("empty url")<<QUrl();
    QTest::newRow("tag and branch mixed ")<<QUrl("svn://repo/aa/tags/branches/newFeature");
    QTest::newRow("tag and trunk mixed ")<<QUrl("svn://repo/aa/tags/trunk/newFeature");
    QTest::newRow("branch and trunk mixed ")<<QUrl("svn://repo/aa/trunk/branches/newFeature");
}

void TestSemanticVersionTest::testInvalidSvnTag()
{
    QFETCH(QUrl,url);

    QVERIFY_EXCEPTION_THROWN(Url::TagFromSubversionUrl(url), std::runtime_error);

}

QTEST_APPLESS_MAIN(TestSemanticVersionTest)

#include "tst_TestSemanticVersionTest.moc"
