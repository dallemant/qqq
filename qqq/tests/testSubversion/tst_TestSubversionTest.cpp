#include <QString>
#include <QtTest>
#include <QDebug>
#include <QProcessEnvironment>
#include <qqq/Subversion/svnwrapper.h>
#include <qqq/Subversion/ExternalDecoder.h>

class TestSubversionTest : public QObject
{
    Q_OBJECT

public:
    TestSubversionTest();

private Q_SLOTS:

    void initTestCase();
    void cleanupTestCase();

    void decodeSvnInfoXml();
    void testSvnWrapper();
    void getAllExternals();
    void testUrl();
    void testUrl_data();
    void decodeExternals_data();
    void decodeExternals();


private:
    QTemporaryDir dir;
    QUrl repo_url ;
};



TestSubversionTest::TestSubversionTest()
{

    const std::string create_repo_str=
R"_(mkdir import_project
touch import_project/main.cpp
svnadmin create svn_repo
svn import $(pwd)/import_project file://$(pwd)/svn_repo -m"initial import"
exit 0)_";


    //QProcess::execute()
    QDir d(dir.path());
    QString repo = d.absoluteFilePath("svn_repo");
    QString import_project = d.absoluteFilePath("import_project");
    QVERIFY(d.mkdir("import_project"));


    //svnadmin create svn_rep
    QVERIFY(QProcess::execute("svnadmin",QStringList{"create",repo})==0);

    //create file to import
    QVERIFY(QProcess::execute("touch",QStringList{d.absoluteFilePath("import_project/main.cpp")})==0);



    //import file in project
    repo_url=QUrl::fromLocalFile(repo);
    QVERIFY(QProcess::execute("svn",QStringList{"import",
                                                repo_url.toString(),
                                                "-m",
                                                "initial import",
                                                d.absoluteFilePath("import_project")})==0);

}

void TestSubversionTest::initTestCase()
{

}

void TestSubversionTest::cleanupTestCase()
{

}

void TestSubversionTest::testSvnWrapper()
{
    {
    SvnWrapper svn("svnbadpath");
    #if QT_VERSION >= 0x050300
    QVERIFY_EXCEPTION_THROWN(svn.CheckProgram(),std::runtime_error);
    #endif
    }

    SvnWrapper svn("svn");
    svn.CheckProgram();

    QDir d(dir.path());

        QVERIFY(!d.exists("my_proj"));
        QUrl url = this->repo_url;
        QVERIFY(url.isValid());
        qDebug()<<"repo url is: "<< url.toString();
        svn.CheckOut("my_proj", url , d);
        QVERIFY(d.exists("my_proj"));

        QCOMPARE( svn.GetRepositoryUrl(d.absoluteFilePath("my_proj")), url );
}



void TestSubversionTest::decodeExternals_data(){
    QTest::addColumn<QString>("line");
    QTest::addColumn<QUrl>("expect_url");
    QTest::addColumn<QString>("expect_name");
    QTest::addColumn<bool>("expect_is_throw");

  //  QTest::newRow("caret")<<". - ^/Messenger/branches/AddIrridiumChannel Messenger"<<QUrl("svn://repo/Messenger/branches/AddIrridiumChannel")<<"Messenger"<<false;
  //  QTest::newRow("head")<<"Apps - CommBridge svn://130.145.60.59/SEI_PROJECTS/CommBridge/trunk"<<QUrl("svn://130.145.60.59/SEI_PROJECTS/CommBridge/trunk")<<"CommBridge"<<false;
    QTest::newRow("inversed ")<<"Apps - svn://130.145.60.59/SEI_PROJECTS/CommBridge/trunk CommBridge"<<QUrl("svn://130.145.60.59/SEI_PROJECTS/CommBridge/trunk")<<"CommBridge"<<false;
}

void TestSubversionTest::decodeExternals()
{
    QFETCH(QString, line);
    QFETCH(QUrl,expect_url);
    QFETCH(QString,expect_name);
    QFETCH(bool, expect_is_throw);
    QTextStream stream(&line);

    ExternalDecoder decoder;

    bool is_throwed=false;
    try{
    auto ext =decoder.Decode(QUrl("svn://repo"), stream);
    QVERIFY(ext.size() ==1);
    QCOMPARE(ext.front().name,expect_name);
    QCOMPARE(ext.front().url,expect_url);
    }catch(std::runtime_error e){
        QVERIFY2(expect_is_throw,e.what());
        is_throwed = true;
    }

    QCOMPARE(is_throwed,expect_is_throw);
}


void TestSubversionTest::getAllExternals()
{
    /**
      commande a lancer:    svn propget svn:externals -R .
      */


     QString r= R"_(. - ^/Messenger/branches/AddIrridiumChannel Messenger

Apps - CommBridge svn://130.145.60.59/SEI_PROJECTS/CommBridge/trunk
DesBash svn://eca_subversion/SEI_PROJECTS/DesScript/DesBash/tags/1.0.0
Mediator svn://130.145.60.59/SEI_LIB_DES/Des/tags/4.4.4/apps/Mediator-005.1.05.120
StartScript svn://eca_subversion/SEI_PROJECTS/QMakeTools/create_start_script/trunk

Libs - libAclScriptable svn://eca_subversion/SEI_PROJECTS/libScript/tags/1.3.0/libAclScriptable
libDes svn://130.145.60.59/SEI_LIB_DES/Des/tags/4.4.4/libs/libDes-005.1.05.500
libDeuScriptable svn://eca_subversion/SEI_PROJECTS/libScript/tags/1.3.0/libDeuScriptable
libQtScriptable svn://eca_subversion/SEI_PROJECTS/libScript/tags/1.3.0/libQtScriptable
libVeh svn://130.145.60.59/SEI_LIB_DES/Veh/tags/3.1.2/libs/libVeh-005.1.05.700
qt5_deploy svn://130.145.60.59/SEI_PROJECTS/ThirdPartyLibs/Qt5/tags/1.1.2)_";

    ExternalDecoder decoder;

    QTextStream stream(&r);
    auto vect = decoder.Decode(QUrl("svn://repo"),stream);
    QCOMPARE(vect.size(), static_cast<size_t>(11));



}


void TestSubversionTest::decodeSvnInfoXml()
{
    SvnWrapper svn("svn");
    svn.CheckProgram();

    QByteArray array("<info><entry><url>file:///tmp</url></entry></info");

    QCOMPARE(svn.DecodeUrlFromSvnInfoXml(array), QUrl("file:///tmp"));

}


    void TestSubversionTest::testUrl_data()
    {
        QTest::addColumn<QString>("text");

        QTest::newRow("semantic version")<<"1.2.3";
        QTest::newRow("svn")<<"svn://repo_name/project_name/trunk";
        QTest::newRow("svn")<<"svn://repo_name/project_name/tags/1.2.3";
        QTest::newRow("local file")<<"file:c:/my_dir/file.log";
        QTest::newRow("local dir")<<"file:c:/my_dir";
    }

    void TestSubversionTest::testUrl()
    {
        QFETCH(QString,text);
        QUrl url(text);
        QVERIFY(url.isValid());
        qDebug()<< QString("%1 :islocalfile: %2 scheme:%3 host: %4 path: %5")
                   .arg(url.toDisplayString())
                   .arg(url.isLocalFile())
                   .arg(url.scheme())
                   .arg(url.host())
                   .arg(url.path());
        if(url.isLocalFile()){
            qDebug()<<" toLocalfile="<<url.toLocalFile();
        }
    }

QTEST_APPLESS_MAIN(TestSubversionTest)

#include "tst_TestSubversionTest.moc"
