#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T12:46:20
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_TestSubversionTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_TestSubversionTest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
include(../common.pri)


