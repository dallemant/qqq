#include <QString>
#include <QtTest>

#include <qqq/Api/IBackEnd.h>
#include <qqq/Package/TPackage.h>

class TestDependancyResolverTest : public QObject {
  Q_OBJECT

 public:
  TestDependancyResolverTest();


  /// @todo
  void testCanResolveRangeAndFixed();
 private Q_SLOTS:

  void testCanResolveFixed();
  void testCanResolveExtendedTrunk();
};

TestDependancyResolverTest::TestDependancyResolverTest() {}

namespace dataset {
const TDepend libDes{"libDes", "latest"};
const TDepend libVeh{"libVeh", "latest"};
const TDepend libDesGreater{"libDes", ">4.0.0"};
const TDepend libVehGreater{"libVeh", ">3.0.0"};
const TDepend libDesFixed{"libDes", "4.1.0"};
const TDepend libVehFixed{"libVeh", "3.1.0"};
const TDepend AgentA{"AgentA", "latest"};
const TDepend AgentB{"AgentB", "latest"};
const TDepend AgentC{"AgentC", "trunk/tutu"};

const TPackage p_libDes{libDes, {}};

}

void TestDependancyResolverTest::testCanResolveRangeAndFixed() {
  namespace ds = dataset;

  const TPackage p_libVeh{ds::libVeh, {ds::libDesGreater}};
  const TPackage p_AgentA{ds::AgentA, {ds::libDesGreater, ds::libVehGreater}};
  const TPackage p_AgentB{ds::AgentB, {ds::libDesGreater, ds::libVehGreater}};

  InMemoryBackend back_end;

  back_end.Register(ds::p_libDes);
  back_end.Register(p_libVeh);
  back_end.Register(p_AgentA);
  back_end.Register(p_AgentB);

  const TDepend dep_workspace{"workspace_xxx", "latest"};
  const TPackage p_workspace{dep_workspace, {ds::AgentA, ds::AgentB, ds::libDesFixed, ds::libVehFixed}};

  back_end.Register(p_workspace);

  QString error;
  auto deps = back_end.ResolveDependencies(dep_workspace,&error);
  QVERIFY2(deps.size() == 4,qPrintable(error));
}

void TestDependancyResolverTest::testCanResolveFixed() {
    namespace ds = dataset;
    const TPackage p_libDesFixed{ds::libDesFixed, {}};
    const TPackage p_libVeh{ds::libVehFixed, {ds::libDesFixed}};
    const TPackage p_AgentA{ds::AgentA, {ds::libDesFixed, ds::libVehFixed}};
    const TPackage p_AgentB{ds::AgentB, {ds::libDesFixed, ds::libVehFixed}};

    InMemoryBackend back_end;

    back_end.Register(p_libDesFixed);
    back_end.Register(p_libVeh);
    back_end.Register(p_AgentA);
    back_end.Register(p_AgentB);

    const TDepend dep_workspace{"workspace_xxx", "latest"};
    const TPackage p_workspace{dep_workspace, {ds::AgentA, ds::AgentB, ds::libDesFixed, ds::libVehFixed}};

    back_end.Register(p_workspace);

    QString error;
    auto deps = back_end.ResolveDependencies(dep_workspace,&error);
    QVERIFY2(deps.size() == 4,qPrintable(error));
}

void TestDependancyResolverTest::testCanResolveExtendedTrunk()
{
    namespace ds = dataset;


    const TPackage p_AgentC{ds::AgentC, {}};

    InMemoryBackend back_end;

    back_end.Register(p_AgentC);

    const TDepend dep_workspace{"workspace_xxx", "latest"};
    const TPackage p_workspace{dep_workspace, {ds::AgentC}};

    back_end.Register(p_workspace);

    QString error;
    auto deps = back_end.ResolveDependencies(p_workspace,&error);
    QVERIFY2(error.count() == 0, qPrintable(error));
    QCOMPARE(deps, {ds::AgentC});
}

QTEST_APPLESS_MAIN(TestDependancyResolverTest)

#include "tst_TestDependancyResolverTest.moc"
