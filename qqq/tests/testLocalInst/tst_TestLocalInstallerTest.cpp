#include <QString>
#include <QtTest>

#include <qqq/Local/TLocalInstaller.h>
#include <qqq/Package/TPackageJson.h>

class TestLocalInstallerTest : public QObject
{
  Q_OBJECT

public:
  TestLocalInstallerTest();

private Q_SLOTS:
  void testCase1();
};

TestLocalInstallerTest::TestLocalInstallerTest()
{
}

void
TestLocalInstallerTest::testCase1()
{
  QTemporaryDir temp_dir;
  QDir orig{ QDir::current() };
  QVERIFY(QDir::setCurrent(temp_dir.path()));

  QDir repo(temp_dir.path()), rlibA(temp_dir.path()), workspace(temp_dir.path());

  // prepare repo and workspace
  QVERIFY(repo.mkdir("repo"));
  QVERIFY(repo.cd("repo"));
  QVERIFY(repo.exists());

  QVERIFY(rlibA.mkpath("repo/libA/src"));
  QVERIFY(rlibA.cd("repo/libA"));
  QVERIFY(rlibA.exists());
  QVERIFY(workspace.mkpath("workspace/other_dir"));
  QVERIFY(workspace.cd("workspace"));
  QVERIFY(workspace.exists());

  // create libA package.json
  TDepend libA{ "libA", "1.0.0" };
  TPackage p{ libA, {} };
  PackageJson::Save(rlibA, p);

  TDepend libBadUrl{ "bad_url", "1.0.0" };
  TDepend libNotRegistered{ "not_registered", "1.0.0" };

  // create libA source files
  {
    QFile f(rlibA.absoluteFilePath("src/file.cpp"));
    QVERIFY(f.open(QIODevice::ReadWrite));
    QVERIFY(f.write("#include<iostream>\nint main(){\nstd::cout<<\"hello world\"<<std::endl;\n}"));
  }
  QVERIFY(QFile::exists(rlibA.absoluteFilePath("src/file.cpp")));

  // urls def
   Url url_libA{ Url::local_storage, QUrl::fromLocalFile(rlibA.absolutePath()) };
   Url url_libBadUrl{ Url::local_storage, QUrl::fromLocalFile("/a/b/c/d") };

  QVERIFY(QDir::setCurrent(workspace.absolutePath()));
  // be careful, qdir a relative to current dir... need to rebuild them
  workspace.cd(temp_dir.path() + "workspace");

  // start test
  TLocalInstaller installer;
#if QT_VERSION >= 0x050300
  QVERIFY_EXCEPTION_THROWN(installer.Fetch(libBadUrl,url_libBadUrl.url), std::runtime_error);

#endif
  QVERIFY(installer.CanHandle(url_libA.protocol));
  installer.Fetch(libA,url_libA.url);

  const QString loc_p = workspace.absoluteFilePath("libA/package.json");
  const QString loc_s = workspace.absoluteFilePath("libA/src/file.cpp");
  QVERIFY2(QFile::exists(loc_p), qPrintable(loc_p));
  QVERIFY2(QFile::exists(loc_s), qPrintable(loc_s));

  // no effect ; how to check?
  installer.Remove(libBadUrl);
  installer.Remove(libNotRegistered);

  installer.Remove(libA);
  // check libA deleted
  QVERIFY(!QDir(workspace.path() + "/libA").exists());
  // check workspace still here
  QVERIFY(workspace.exists());
  QVERIFY(workspace.cd("other_dir"));

  QDir::setCurrent(orig.path());
}

QTEST_APPLESS_MAIN(TestLocalInstallerTest)

#include "tst_TestLocalInstallerTest.moc"
