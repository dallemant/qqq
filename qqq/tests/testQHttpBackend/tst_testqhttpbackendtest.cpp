#include <QString>
#include <QtTest>
#include <QProcess>
#include <QUrlQuery>
#include <qqq/HttpBackend/HttpBackend.h>


class TestQHttpBackendTest : public QObject
{
    Q_OBJECT

public:
    TestQHttpBackendTest();


    void testDictionnary();
private Q_SLOTS:
    void initTestCase();
     void testHttpBackendUrlRegisterAndResolve();
    void cleanupTestCase();

    void sql_query_handle_star_char(){
        QUrlQuery q;
        q.addQueryItem("pattern","lib*");

        QCOMPARE(q.query(), QString("pattern=lib*"));
    }

private:
    QProcess m_server;

    const int m_port{ 8902};
    const std::string m_ip{ "127.0.0.1" };
};

TestQHttpBackendTest::TestQHttpBackendTest()
{
}

void TestQHttpBackendTest::initTestCase()
{
    /// Start Python server
    m_server.setProgram("python");
    auto script =QFINDTESTDATA("http_server.py");
    QVERIFY(script.size());
    m_server.setArguments(QStringList{script, QString::number(m_port), QString::fromStdString(m_ip) });

    m_server.setProcessChannelMode(QProcess::ForwardedChannels);
    m_server.start();
    QVERIFY(m_server.waitForStarted(100));

    QTest::qWait(100);
    QCOMPARE(m_server.state(), QProcess::Running);

}

void TestQHttpBackendTest::cleanupTestCase()
{
    /// Stop Python server
    m_server.kill();
    QVERIFY(m_server.waitForFinished(2000));
}


void TestQHttpBackendTest::testHttpBackendUrlRegisterAndResolve()
{
    //QBENCHMARK{
    HttpBackend be(m_ip,m_port);

    TDepend depend{"libA","2.0.0"};
    Url url{Url::subversion,QUrl("svn://repo/tags/2.0.0")};

    try {
        be.Register(depend, url);
    } catch (...) {
        QVERIFY(0);
    }



    //auto ret = be.Resolve(depend);
    //QVERIFY(ret == url);    //}
}

namespace dataset {
const TDepend libDes{"libDes", "latest"};
const TDepend libVeh{"libVeh", "latest"};
const TDepend libDesGreater{"libDes", ">4.0.0"};
const TDepend libVehGreater{"libVeh", ">3.0.0"};
const TDepend libDesFixed{"libDes", "4.1.0"};
const TDepend libVehFixed{"libVeh", "3.1.0"};
const TDepend AgentA{"AgentA", "latest"};
const TDepend AgentB{"AgentB", "latest"};
const TDepend AgentB_tag{"AgentB", "1.2.3"};
const TPackage p_libDes{libDes, {}};

}

void TestQHttpBackendTest::testDictionnary()
{
    namespace ds = dataset;
    const TPackage p_libVeh{ds::libVeh, {ds::libDesGreater}};
    const TPackage p_AgentA{ds::AgentA, {ds::libDesGreater, ds::libVehGreater}};
    const TPackage p_AgentB{ds::AgentB, {ds::libDesGreater, ds::libVehGreater}};
    const TPackage p_AgentB_tag{ds::AgentB_tag, {ds::libDesGreater, ds::libVehGreater}};

    HttpBackend be(m_ip,m_port);
    for(const auto& package:{ds::p_libDes, p_libVeh, p_AgentA, p_AgentB, p_AgentB_tag}){
        be.Register(package);
    }

    for(const auto& dep:{ds::libDes, ds::libVeh, ds::AgentA, ds::AgentB, ds::AgentB_tag}){
        auto ret = be.Package(dep);
        QVERIFY2(ret.first, QString("%1 %2 not found").arg(dep.name,dep.version).toStdString().c_str() );
    }

}



QTEST_MAIN(TestQHttpBackendTest)

#include "tst_testqhttpbackendtest.moc"
