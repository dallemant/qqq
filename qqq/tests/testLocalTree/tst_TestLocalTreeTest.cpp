#include <QString>
#include <QtTest>
#include <QTemporaryDir>
#include <qqq/Package/TLocalTree.h>
#include <qqq/Package/TPackageJson.h>

class TestLocalTreeTest : public QObject
{
    Q_OBJECT

public:
    TestLocalTreeTest();

private Q_SLOTS:
    void testCase1();
    void testMissingDeps();
    void testConflictDeps();

private:

    QDir CreateFakeWorkspace(const QTemporaryDir& temp_dir)
    {
        CreateDepend( temp_dir, "projectA", "1.2.3");
        CreateDepend( temp_dir, "projectB", "4.5.6");

        QDir d(temp_dir.path());
        d.mkdir("not_a_project");

        return d;
    }
    void CreateDepend(const QTemporaryDir& temp_dir_p, QString name_p,TVersion version_p){
        QDir d(temp_dir_p.path());
        d.mkdir(name_p);
        d.cd(name_p);

        TPackage p;
        p.name = name_p;
        p.version = version_p;

        PackageJson::Save(d,p);
    }


};

TestLocalTreeTest::TestLocalTreeTest()
{
}


void TestLocalTreeTest::testCase1()
{
    //init
    QTemporaryDir temp_dir;
    QDir d = CreateFakeWorkspace(temp_dir);

    TLocalTree tree(d);
    QCOMPARE( tree.GetCount(), 2);
    auto deps = tree.GetDepends();
    QVERIFY( deps.size() == 2);



    QVERIFY(deps[0].name == "projectA");
    QVERIFY(deps[1].name == "projectB");

}

void TestLocalTreeTest::testMissingDeps()
{
    QTemporaryDir temp_dir;
    QDir d = CreateFakeWorkspace(temp_dir);

    TLocalTree tree(d);

    auto v = tree.GetDepends();
    v.pop_back();
    TLocalTree sparse_tree(v);

    auto missing_depends = tree.GetMissingDepends(sparse_tree);

    QVERIFY(missing_depends.size() == 1);
    QCOMPARE(missing_depends[0].name , QString("projectB"));

}

void TestLocalTreeTest::testConflictDeps()
{
    QTemporaryDir temp_dir;
    QDir d = CreateFakeWorkspace(temp_dir);

    TLocalTree tree(d);

    auto v = tree.GetDepends();
    v[1].version =  "7.8.9";
    TLocalTree conflict_tree(v);

    auto conflict_depends = tree.GetConflictDepends(conflict_tree);

  //  QVERIFY(conflict_depends.size() == 1);
    QCOMPARE(conflict_depends[0] , QString("projectB"));
}


QTEST_APPLESS_MAIN(TestLocalTreeTest)

#include "tst_TestLocalTreeTest.moc"
