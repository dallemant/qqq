#-------------------------------------------------
#
# Project created by QtCreator 2016-11-07T22:26:51
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_TestLocalTreeTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_TestLocalTreeTest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include(../common.pri)
