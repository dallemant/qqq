TEMPLATE = subdirs

SUBDIRS += \
    testCommandline \
    testPackageJson \
    testSubversion \
    testLocalTree \
    testQSettingsBackend \
    testQmakeInstaller \
    testLocalInst \
    testSemanticVersion \
    testDependancyResolver \
    testQHttpBackend \
    testGit

