#include <QString>
#include <QtTest>
#include <qqq/TAppArguments.h>
#include <qqq/Commands/TInit.h>
#include <qqq/Commands/TInstall.h>
#include <qqq/TConsole.h>
#include <qqq/Package/TPackage.h>
#include <qqq/Package/TPackageJson.h>
#include <qqq/Api/IWorkspace.h>
#include <qqq/Api/IBackEnd.h>
#include <qqq/Qmake/TQmakeWorkspace.h>

class TestCommandlineTest : public QObject {
    Q_OBJECT

   public:
    TestCommandlineTest();

   private Q_SLOTS:
    void testCase1_data();
    void testCase1();

    void testInstall();
};

TestCommandlineTest::TestCommandlineTest() {}

void TestCommandlineTest::testCase1_data() {
    QTest::addColumn<QStringList>("args");

    QTest::newRow("empty") << QStringList{"qqq"};
    QTest::newRow("help") << QStringList{"qqq", "-h"};
    QTest::newRow("init") << QStringList{"qqq","--workspace", "init"};
    QTest::newRow("init MyWorkspace") << QStringList{"qqq","--workspace" ,"init", "myWorkspace"};
}

void TestCommandlineTest::testCase1() {
    QFETCH(QStringList, args);
    QDir orig = QDir::current();

    // repertoire temporaire
    QTemporaryDir temp;
    QVERIFY(QDir::setCurrent(temp.path()));

    TAppArguments reader;
    QString str_in_console;
    TConsoleQt console(str_in_console);

    TQMakeWorkspaceCreator workspace_creator;
    TInit init(&workspace_creator);
    reader.RegisterCommand(&init);

    QVERIFY2(reader.Process(args, &console), str_in_console.toStdString().data());

    QVERIFY(QDir::setCurrent(orig.path()));
}

class TDummyInstaller : public IInstaller {
    QTemporaryDir m_temp_dir;
    // IInstaller interface
   public:
    TDummyInstaller(std::vector<TDepend>& fetched, std::vector<TDepend>& removed)
        : IInstaller(), m_fetched(fetched), m_removed(removed) {}

    void Fetch(const TDepend& dep_p) override { m_fetched.push_back(dep_p); }
    void Remove(const TDepend& dep_p) override { m_removed.push_back(dep_p); }
    void Replace(const TDepend& old_p,const TDepend& new_p) override {Remove(old_p);Fetch(new_p); }


    std::vector<TDepend>& m_fetched;
    std::vector<TDepend>& m_removed;

    void Clear() {
        m_fetched.clear();
        m_removed.clear();
    }
};

class TDummyInstallerCreator : public IInstallerCreator {
    // TInstallerCreator interface
   public:
    std::unique_ptr<IInstaller> CreateInstallerTransaction() override {
        auto raw = new TDummyInstaller(m_fetched, m_removed);
        return std::unique_ptr<IInstaller>(raw);
    }

    std::vector<TDummyInstaller*> m_installers;

    std::vector<TDepend> m_fetched;
    std::vector<TDepend> m_removed;

    void Clear() {
        m_fetched.clear();
        m_removed.clear();
    }
};


///
/// \brief TestCommandlineTest::testInstall
/// \todo faire des cas avec conflit, avec libCommune....
void TestCommandlineTest::testInstall() {
    QStringList args{"qqq", "install"};
    QDir orig = QDir::current();

    // repertoire temporaire
    QTemporaryDir temp;
    QVERIFY(QDir::setCurrent(temp.path()));

    //prepare
    QString str_in_console;
    TConsoleQt console(str_in_console);

    TDummyInstallerCreator ic;
    InMemoryBackend back_end;
    TInstall install(&ic,&back_end);

    TAppArguments reader;
    reader.RegisterCommand(install.Commands());

    QVERIFY2(!reader.Process(args, &console),qPrintable(str_in_console));
    ic.Clear();


    //dataset
    TDepend libB{"bbb", "1.2.3"};
    TDepend libC{"ccc", "1.2.3"};
    TPackage package{"aaa","0.0.0",{libB, libC}};

    //load backend
    back_end.Register(TPackage{libC,{}});
    back_end.Register(TPackage{libB,{libC}});
    back_end.Register(package);

    //create package.json in temp dir
    PackageJson::Save(package);

    //create .qmake.conf to fake workspace dir
    QFile f(".qmake.conf");
    QVERIFY(f.open(QIODevice::ReadWrite));
    f.close();

    //launch install command
    QVERIFY2(reader.Process(args, &console),qPrintable(str_in_console));
    qDebug() << str_in_console;

    //check result
    auto found_deps = QVector<TDepend>::fromStdVector(ic.m_fetched);
    QCOMPARE(found_deps.size(), 2);
    QVERIFY(found_deps.contains(libB));
    QVERIFY(found_deps.contains(libC));

    QVERIFY(ic.m_removed.empty());

    QVERIFY(QDir::setCurrent(orig.path()));
}

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(resources);
    QCoreApplication app(argc, argv);
    app.setAttribute(Qt::AA_Use96Dpi, true);
    TestCommandlineTest tc;
    #if QT_VERSION >= 0x050300
    QTEST_SET_MAIN_SOURCE_PATH
    #endif
    return QTest::qExec(&tc, argc, argv);
}

#include "tst_TestCommandlineTest.moc"
