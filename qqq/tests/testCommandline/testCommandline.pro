#-------------------------------------------------
#
# Project created by QtCreator 2016-09-05T22:05:52
#
#-------------------------------------------------


QT       += testlib

QT       -= gui

TARGET = tst_TestCommandlineTest
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += tst_TestCommandlineTest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include(../common.pri)


