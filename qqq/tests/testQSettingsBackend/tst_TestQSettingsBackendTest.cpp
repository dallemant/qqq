#include <QString>
#include <QtTest>
#include <QTemporaryDir>

#include <qqq/Api/IBackEnd.h>
#include <qqq/Package/TSemanticVersion.h>

#include <qqq/Package/TUrl.h>

class TestQSettingsBackendTest : public QObject {
    Q_OBJECT
public:
    TestQSettingsBackendTest();
private Q_SLOTS:

    void testBackendApi();
    void testDictionnary();
    void package_not_found_if_not_registered();

    void testDepResolver_data();
    void testDepResolver();

public:


};

TestQSettingsBackendTest::TestQSettingsBackendTest() {}

namespace dataset {
const TDepend libA{"libA", "1.0.0"};
const TDepend libA1{"libAA", "1.0.0"};
const TDepend libA2{"libAA", "2.0.0"};
const TDepend libB{"libB", "1.0.0"};
const TDepend libC{"libC", "latest"};
const TVersion v1{"1.0.0"};
const TVersion v2{"2.0.0"};
const QString workspace{"workspace"};
const QString exeA{"exeA"};
const QString exeB{"exeB"};
const Url url1{Url::subversion, QUrl("svn://repo/libA/tags/1.0.0")};
const Url url2{Url::subversion, QUrl("svn://repo/libA/tags/2.0.0")};
const TDepend lib_absent{"lib_not_present", "0.0.0"};
}
void TestQSettingsBackendTest::testBackendApi() {
    // dataset
    namespace ds = dataset;

    // auto remove settings file
    QTemporaryDir temp_dir;
    QDir d(temp_dir.path());

    {
        QSettings settings(d.absoluteFilePath("settings.ini"), QSettings::IniFormat);
        QSettingsBackend be(&settings);

        QVERIFY(be.Resolve(ds::lib_absent).url.isValid() == false);

        be.Register(ds::libA1, ds::url1);
        be.Register(ds::libA2,ds::url2);
    }
    //  unload/reload setttings
    {
        QSettings settings(d.absoluteFilePath("settings.ini"), QSettings::IniFormat);
        QSettingsBackend be(&settings);
        QVERIFY(be.Resolve(ds::lib_absent).url.isValid() == false);
        QVERIFY(be.Resolve(ds::libA1) == ds::url1);
        QVERIFY(be.Resolve(ds::libA2) == ds::url2);

        auto libA_versions = QVector<TDepend>::fromStdVector(be.AvailableVersionFor(ds::libA1.name));
        QVERIFY(libA_versions.size() == 2);
        QVERIFY(libA_versions.contains(ds::libA1));
        QVERIFY(libA_versions.contains(ds::libA2));
    }
}

void TestQSettingsBackendTest::testDictionnary() {
    // dataset
    namespace ds = dataset;

    // create auto remove settings file
    QTemporaryDir temp_dir;
    QDir d(temp_dir.path());
    {
        QSettings settings(d.absoluteFilePath("settings.ini"), QSettings::IniFormat);
        QSettingsBackend be(&settings);

        // create Dictionnary 1 packqge 2 versions
        TPackage p;

        // register libs with no dependencies
        for (auto& dep : {ds::libA, ds::libB, ds::libC}) {
            p.name = dep.name;
            p.version = dep.version;
            p.depends.clear();
            be.Register(p);
        }

        p.name = ds::exeA;
        p.version = ds::v1;
        p.depends = {ds::libA, ds::libB};
        be.Register(p);

        p.version = ds::v2;
        p.depends.push_back(ds::libC);
        be.Register(p);
    }
    //  unload/reload setttings
    {
        QSettings settings(d.absoluteFilePath("settings.ini"), QSettings::IniFormat);
        QSettingsBackend be(&settings);

        // all composants are registered
        for(TDepend dep : {ds::libA, ds::libB, ds::libC, TDepend{ds::exeA, ds::v1}, TDepend{ds::exeA, ds::v2} }){
            auto ret = be.Package(dep);
            QVERIFY2(ret.first,QString("%1 %2 not found").arg(dep.name,dep.version).toStdString().c_str());
        }

        //libs does not have deps
        for(TDepend dep : {ds::libA, ds::libB, ds::libC }){
            auto ret = be.Package(dep);
            QVERIFY2(ret.first,QString("%1 %2 not found").arg(dep.name,dep.version).toStdString().c_str());

            QVERIFY(ret.second.depends.empty());
        }

        //exe have deps
         std::vector<TDepend>expected_deps{ds::libA, ds::libB};
        {
            auto ret = be.Package(TDepend{ds::exeA, ds::v1});
            QVERIFY2(ret.first, QString("%1 %2 not found").arg(ds::exeA, ds::v1).toStdString().c_str());
            QVERIFY(ret.second.depends.size() == 2);
            QCOMPARE(ret.second.depends, expected_deps);
        }
         {
         auto ret = be.Package(TDepend{ds::exeA, ds::v2});
         QVERIFY2(ret.first, QString("%1 %2 not found").arg(ds::exeA, ds::v2).toStdString().c_str());
         QVERIFY(ret.second.depends.size() == 3);
         expected_deps.push_back(ds::libC);
         QCOMPARE(ret.second.depends, expected_deps);
         }
    }
}

void TestQSettingsBackendTest::package_not_found_if_not_registered(){
    // dataset
    namespace ds = dataset;

    // create auto remove settings file
    QTemporaryDir temp_dir;
    QDir d(temp_dir.path());

    QSettings settings(d.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    QSettingsBackend be(&settings);

    auto dico = be.Package(ds::libA);
    QVERIFY(dico.first == false);

}
void BuildDictionnary(QSettingsBackend& be_p) {
    namespace ds = dataset;
    // create Dictionnary 1 packqge 2 versions
    TPackage p;

    // register libs with no dependencies
    for (auto& dep : {ds::libA, ds::libB, ds::libC}) {
        p.name = dep.name;
        p.version = dep.version;
        p.depends.clear();
        be_p.Register(p);
    }

    // register app A
    p.name = ds::exeA;
    p.version = ds::v1;
    p.depends = {ds::libA, ds::libB};
    be_p.Register(p);

    // register app B
    p.name = ds::exeB;
    p.version = ds::v1;
    p.depends.push_back(ds::libC);
    be_p.Register(p);
};

void AddConflictsToDictionnary(QSettingsBackend& be_p) {
    namespace ds = dataset;
    // register different libA
    TPackage p;
    p.name = ds::libA.name;
    p.version = "1.2.3";
    p.depends.clear();
    be_p.Register(p);

    // register workspace
    p.name = ds::workspace;
    p.version = ds::v1;
    p.depends = {{ds::exeA, ds::v1}, {ds::exeB, ds::v1}};
    be_p.Register(p);
};

void TestQSettingsBackendTest::testDepResolver_data() {
    QTest::addColumn<QString>("test_title");
    QTest::addColumn<int>("test_number");
    QTest::addColumn<unsigned int>("nb_depends_expected");

    QTest::newRow("test 1") << "workspace with simple versions and no conflicts" << 1 << 5u;
    QTest::newRow("test 2") << "workspace with simple versions and 1 conflict" << 2 << 0u;
}

void TestQSettingsBackendTest::testDepResolver() {
    QFETCH(QString, test_title);
    QFETCH(int, test_number);
    QFETCH(unsigned int, nb_depends_expected);

    namespace ds = dataset;

    // create auto remove settings file
    QTemporaryDir temp_dir;
    QDir d(temp_dir.path());
    QSettings settings(d.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    QSettingsBackend be(&settings);

    qDebug() << test_title;

    switch (test_number) {
        case 1: {
            BuildDictionnary(be);

            // register workspace
            TPackage p;
            p.name = ds::workspace;
            p.version = ds::v1;
            p.depends = {{ds::exeA, ds::v1}, {ds::exeB, ds::v1}};
            be.Register(p);
        } break;
        case 2: {
            BuildDictionnary(be);
            AddConflictsToDictionnary(be);

            // register workspace with conflict
            TPackage p;
            p.name = ds::workspace;
            p.version = ds::v1;
            p.depends = {{ds::exeA, ds::v1}, {ds::exeB, ds::v1}, {ds::libA.name, TVersion("1.2.3")}};
            be.Register(p);
        } break;
        default:
            QVERIFY2(0, "unhandled switchcase");
    }

    // Resolve
    QString error;
    auto deps = be.ResolveDependencies({ds::workspace, ds::v1}, &error);

    // Print result
    if (deps.size()) {
        qDebug() << "-------workspace dependencies are ---------------";
    }
    for (const auto& dep : deps) {
        qDebug() << "dep:" << dep.name << "/" << dep.version;
    }
    if (error.size()) {
        qDebug() << "error msg is:" << error;
    }

    // Check result
    QCOMPARE(deps.size(), static_cast<size_t>(nb_depends_expected));
}





QTEST_APPLESS_MAIN(TestQSettingsBackendTest)

#include "tst_TestQSettingsBackendTest.moc"
