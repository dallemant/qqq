#-------------------------------------------------
#
# Project created by QtCreator 2016-11-29T22:18:06
#
#-------------------------------------------------

QT       += testlib

QT       -= gui
QT += network

TARGET = tst_TestQSettingsBackendTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_TestQSettingsBackendTest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include(../common.pri)
