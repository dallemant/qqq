#include <QString>
#include <QtTest>
#include <QTemporaryDir>
#include <qqq/Package/TPackage.h>
#include <qqq/Package/TPackageJson.h>

class TestPackageJsonTest : public QObject
{
    Q_OBJECT

public:
    TestPackageJsonTest();

private Q_SLOTS:
    void testCase1();
};

TestPackageJsonTest::TestPackageJsonTest()
{
}

void TestPackageJsonTest::testCase1()
{
    QTemporaryDir temp;
    QDir tempdir(temp.path());


    TPackage p;
    p.name = "my_workspace";
    p.version = "1.2.3";
    p.depends.push_back( TDepend{ "libA", "1.0.0"});
    p.depends.push_back( TDepend{ "libC", "2.0.0"});

    //sauvegarde
    PackageJson::Save(tempdir, p);
    //lecture
    auto q = PackageJson::Open(tempdir);

    QVERIFY(p == q);

    q.version = "1.2.4";
    QVERIFY(p != q);

}

QTEST_APPLESS_MAIN(TestPackageJsonTest)

#include "tst_TestPackageJsonTest.moc"
