#-------------------------------------------------
#
# Project created by QtCreator 2016-11-05T21:42:23
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_TestPackageJsonTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_TestPackageJsonTest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include(../common.pri)
