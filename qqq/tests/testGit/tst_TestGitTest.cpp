#include <QString>
#include <QtTest>
#include <QFile>
#include <QProcess>
#include <qqq/Git/GitWrapper.h>

class TestGitTest : public QObject
{
    Q_OBJECT

public:
    TestGitTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testGitWrapper_checkout_branch();

    private:

    QTemporaryDir temp;
    QUrl git_repo;
};

TestGitTest::TestGitTest()
{
}

void TestGitTest::initTestCase()
{
    const std::string create_repo_str=
R"_(
git init --bare test_repo.git
cd test_repo.git/
git config receive.denyCurrentBranch ignore
cd -
git clone test_repo.git/ work
cd work
echo 123 > afile.txt
git add .
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git commit -m "add file"
git push origin master

git checkout -b my_branch
echo 123 > bfile.txt
git add bfile.txt
git commit -m "add second file"
git push origin my_branch
cd -
rm -rf work
exit 0
    )_";

    QDir d(temp.path());
    git_repo = QUrl::fromLocalFile(d.absoluteFilePath("test_repo.git"));

    QVERIFY(QDir::setCurrent(temp.path()));
    QString filename(d.absoluteFilePath("create_repo.sh"));
    {
        QFile create_repo(filename);
        QVERIFY2(create_repo.open(QIODevice::ReadWrite),qPrintable(create_repo.errorString()));
        QCOMPARE(create_repo.write(create_repo_str.data()) , static_cast<qint64>(create_repo_str.size()));
    }

    QCOMPARE(QProcess::execute("bash",QStringList{"-e", filename }),0);// add "-x" param to see each executed bash command

    QVERIFY( QFile::exists(git_repo.toLocalFile()));

}

void TestGitTest::cleanupTestCase()
{
}


void TestGitTest::testGitWrapper_checkout_branch()
{
    {
        GitWrapper git("gitbadpath");
        #if QT_VERSION >= 0x050300
        QVERIFY_EXCEPTION_THROWN(git.CheckProgram(),std::runtime_error);
        #endif
    }

    GitWrapper git("git");
    git.CheckProgram();


    QVERIFY(!QDir(".").exists("my_proj"));
    git.Clone("my_proj", git_repo , QDir("."));
    QVERIFY(QDir(".").exists("my_proj"));

    QDir d(QDir(".").absoluteFilePath("my_proj"));

    QVERIFY(d.exists("afile.txt"));
    QVERIFY(!d.exists("bfile.txt"));

    QCOMPARE(git.GetRepositoryUrl(d), git_repo);

    git.Checkout("my_branch",d);

    QVERIFY(d.exists("afile.txt"));
    QVERIFY(d.exists("bfile.txt"));

}

QTEST_APPLESS_MAIN(TestGitTest)

#include "tst_TestGitTest.moc"
