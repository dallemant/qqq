#include <QString>
#include <QtTest>
#include <QTemporaryDir>
#include <qqq/Qmake/TQmakeInstaller.h>

class TestQmakeInstallerTest : public QObject {
    Q_OBJECT

   public:
    TestQmakeInstallerTest();

   private Q_SLOTS:
    void testCase1();
};

TestQmakeInstallerTest::TestQmakeInstallerTest() {}

void TestQmakeInstallerTest::testCase1() {
    QDir olddir = QDir::current();
    QTemporaryDir temp;
    QDir::setCurrent(temp.path());

    {
        QFile f("proj.pro");
        QVERIFY(f.open(QIODevice::ReadWrite));
        QTextStream str(&f);
        str << "TEMPLATE=subdirs";
    }
    QDir d;
    d.mkdir("sub_proj");

    TQmakeInstaller qmake((std::unique_ptr<IInstaller>()));
    qmake.DoFetch({"sub_proj", "1.0.0"});
    qmake.DoRemove({"sub_proj", "1.0.0"});

    QDir::setCurrent(olddir.path());
}

QTEST_APPLESS_MAIN(TestQmakeInstallerTest)

#include "tst_TestQmakeInstallerTest.moc"
