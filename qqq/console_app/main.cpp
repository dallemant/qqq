#include <QCoreApplication>
#include <QSettings>
#include <QTemporaryDir>
#include <QTimer>
#include <iostream>

#include <qqq/Commands/TBackEnd.h>
#include <qqq/Commands/TInit.h>
#include <qqq/Commands/TInstall.h>
#include <qqq/Commands/TSet.h>
#include <qqq/Commands/LegacyWorkspaceMigration.h>
#include <qqq/TAppArguments.h>
#include <qqq/TConsole.h>

#include <qqq/Api/IInstall.h>
#include <qqq/Api/IWorkspace.h>
#include <qqq/Api/installprotocolselector.h>

#include <qqq/Local/TLocalInstaller.h>
#include <qqq/Qmake/TQmakeInstaller.h>
#include <qqq/Qmake/TQmakeWorkspace.h>
#include <qqq/Subversion/TSvnInstaller.h>
#include <qqq/Subversion/svnwrapper.h>
#include <qqq/Subversion/ExternalDecoder.h>
#include <qqq/Git/GitInstaller.h>
#include <qqq/Git/GitWrapper.h>

class TDummyInstaller : public IInstallerDecorator
{

  // IInstaller interface
public:
  using IInstallerDecorator::IInstallerDecorator;

  void DoFetch(const TDepend& dep_p) override
  {
    QDir d;
    d.mkdir(dep_p.name);
    d.cd(dep_p.name);

    QFile pro(d.absoluteFilePath(dep_p.name) + ".pro");
    if (!pro.open(QIODevice::ReadWrite)) {
      throw std::runtime_error("TDummyInstaller::Fetch cant create pro file");
    }

    QTextStream str(&pro);
    str << "TEMPLATE=aux" << endl;
  }
  void DoRemove(const TDepend& dep_p) override
  {
    QDir d;
    if (!d.cd(dep_p.name)) {
      throw std::runtime_error("TDummyInstaller::Remove cd to dir");
    }
    if (!d.removeRecursively()) {
      throw std::runtime_error("TDummyInstaller::Remove failed");
    }
  }
};

struct LocalInstallerCreator : public IInstallerCreator
{
  LocalInstallerCreator(const IBackEnd* backend_p, ISvnWrapper* svn_p, IGitWrapper* git_p) : m_backend(backend_p), m_svn(svn_p),m_git(git_p) {}
  // TInstallerCreator interface
  std::unique_ptr<IInstaller> CreateInstallerTransaction() override
  {
    // register qmake installer
    std::unique_ptr<IInstaller> installer{ nullptr };
    installer = std::unique_ptr<IInstaller>(new TQmakeInstaller(std::move(installer)));

    // register protocol selector and its available protocole
    auto proto_chooser =
      new TInstallerProtocolSelector(std::move(installer), m_backend);
    proto_chooser->AddProtocol(std::unique_ptr<IInstallerProtocol>(new TLocalInstaller));
    proto_chooser->AddProtocol(std::unique_ptr<IInstallerProtocol>(new TSvnInstaller(m_svn)));
    proto_chooser->AddProtocol(std::unique_ptr<IInstallerProtocol>(new GitInstaller(m_git)));

    installer = std::unique_ptr<IInstaller>(proto_chooser);
    return installer;
  }

private:
  const IBackEnd* m_backend;
  ISvnWrapper* m_svn;
  IGitWrapper* m_git;
};

int
main(int argc, char* argv[])
{
  Q_INIT_RESOURCE(resources);
  QCoreApplication a(argc, argv);
  QCoreApplication::setApplicationName("qqq");

  QCoreApplication::setOrganizationName("tazypower");
  QCoreApplication::setApplicationVersion("1.0");

  QSettings::setDefaultFormat(QSettings::IniFormat);
  QSettings settings;
//  std::cout<<"QSettings filename is "<<settings.fileName().toStdString();
//  std::cout<<"QSettings all keys is"<<settings.allKeys().join(" \n ").toStdString();

  QString svn_program = settings.value("SVN/path", "svn").toString();
  SvnWrapper svn(svn_program);

  QString git_program = settings.value("GIT/path", "git").toString();
  GitWrapper git(git_program);

  TAppArguments arg_parser;
  TConsoleQt console;

  TQMakeWorkspaceCreator workspace_creator;
  TInit init(&workspace_creator);

  QDir d;
  QSettings* back_end_settings = nullptr;
  std::unique_ptr<QSettings> local_settings;
  if(!d.exists("qqq_backend.ini")){
      back_end_settings = &settings;
  }else{
      std::cout<<" qqq_backend.ini found in "<< d.absoluteFilePath("qqq_backend.ini").toStdString();
      local_settings.reset( new QSettings(d.absoluteFilePath("qqq_backend.ini"), QSettings::IniFormat));
      back_end_settings = local_settings.get();
  }



  QSettingsBackend qset_backend(back_end_settings);
  LocalInstallerCreator installer_creator(&qset_backend, &svn, &git);
  TInstall install(&installer_creator, &qset_backend);
  arg_parser.RegisterCommand(&init);
  arg_parser.RegisterCommand(install.Commands());

  //@todo rework install decorator to not be forced to instanciates this objects twice
  TSvnInstaller svn_url_finder(&svn);
  GitInstaller git_url_finder(&git);
  DirectoryRepositoryFinder url_finder{&svn_url_finder,&git_url_finder};


  TBackEndCommands back_end_commands(&qset_backend,&url_finder);
  arg_parser.RegisterCommand(back_end_commands.Commands());

  TSet set;
  arg_parser.RegisterCommand(set.Commands());

  ExternalDecoder svn_external_decoder;
  LegacyWorkspaceMigration leg_workspace_migration(&svn,&svn_external_decoder);

  arg_parser.RegisterCommand(leg_workspace_migration.Commands());




  // QCommandLineParser constraints, helpText() needs event loop
  a.processEvents();
  arg_parser.Process(a.arguments(), &console);
  a.processEvents();

  if(local_settings){
      local_settings->sync();
  }
  return 0;
}

