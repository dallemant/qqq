echo "show libA url:"
qqq backend-resolve-url --name=libA --tag=0.0.0

echo "show libB url:"
qqq backend-resolve-url --name=libB --tag=0.0.0

echo "show libC url:"
qqq backend-resolve-url --name=libC --tag=0.0.0

echo "===="
echo "list dependencies..."
echo "all:"
qqq backend-list
echo "----"
echo "exact name"
qqq backend-list libA
echo "----"
echo "wildcard lib*"
qqq backend-list lib*
echo "----"
echo "wildcard lib[AB]"
qqq backend-list lib[AB]
echo "====== done ====="
