. env.conf
rm -rf repo

mkdir repo


cd repo/
PWD=$(pwd)


if [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    echo "mingw host need to rework it for qqq"
    # add this to replace all slash with backslash : -e 's/\//\\/g'
    PWD=$(sed -e 's/^\///' -e 's/^./\0:/' <<< $PWD)
fi

echo "create dummy libA, libB, libC in repo..."
qqq init libA --library
qqq init libB --library
qqq init libC --library

echo "_________________________________"
echo "start building local backend..."
echo "backend-add-url is mandatory to next step"
#qqq backend-add-url --name=libA --tag=0.0.0 --url=file:$PWD/libA --protocol=file
#qqq backend-add-url --name=libB --tag=0.0.0 --url=file:$PWD/libB --protocol=file
#qqq backend-add-url --name=libC --tag=0.0.0 --url=file:$PWD/libC --protocol=file

####      dont forget to synchronize  version in package.json , and tag in backend command
#echo " add lib in svn"
#qqq backend-add-url --name=libDes --tag=0.0.0 --protocol=svn --url=svn://eca_subversion/SEI_LIB_DES/Des/trunk/libs/libDes-005.1.05.500
#qqq backend-add-package package.json
# if working copy is available  :create package.json in libDes dir, commit than:
#cd to libDes
#qqq register



#qqq backend-list


echo "===="
echo "add  libA dependencies in package.json..."
cd libA
qqq add libB 0.0.0
qqq add libC 0.0.0
echo "===="
echo "add  libB dependencies in package.json..."
cd ../libB
qqq add libC 0.0.0
echo "===="
echo "end building local back-end..."
cd ..
#qqq backend-add-package ./libA/package.json
#qqq backend-add-package ./libB/package.json
#qqq backend-add-package ./libC/package.json

qqq register libA --local-repo
qqq register libB --local-repo
qqq register libC --local-repo




echo "==== done ===="

