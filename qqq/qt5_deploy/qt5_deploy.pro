TEMPLATE=aux
CONFIG -= debug_and_release debug_and_release_target

OTHER_FILES = readme_qt5_deploy.txt

exists($$INSTALL_RUNDIR){
    unix:QMAKE_INSTALL_PROGRAM=cp -af	 
    unix:QMAKE_INSTALL_FILE=cp -af
    win32:QMAKE_COPY=xcopy /y
	
    DEPLOYED_LIBS = icu*

#todo: trouver un moyen de simplifier pour eviter des copier coller

#en mode release ou sous unix
CONFIG(release, debug|release)|unix{
    DEPLOYED_LIBS += Qt5Core.*

    contains(QT_DEPLOY_CONFIG, gui):DEPLOYED_LIBS +=   Qt5Gui.*  Qt5Widgets.* Qt5DBus.* ## Dbus pour libxcb
    contains(QT_DEPLOY_CONFIG, svg):DEPLOYED_LIBS +=   Qt5Svg.*
    contains(QT_DEPLOY_CONFIG, opengl):DEPLOYED_LIBS +=   Qt5OpenGL.*
    contains(QT_DEPLOY_CONFIG, printsupport):DEPLOYED_LIBS +=   Qt5PrintSupport.*
    contains(QT_DEPLOY_CONFIG, script):DEPLOYED_LIBS +=   Qt5Script.*
    contains(QT_DEPLOY_CONFIG, network):DEPLOYED_LIBS +=   Qt5Network.*
    contains(QT_DEPLOY_CONFIG, dbus):DEPLOYED_LIBS +=   Qt5DBus.*
    contains(QT_DEPLOY_CONFIG, xml):DEPLOYED_LIBS +=   Qt5Xml.*
}

#en mode debug et sous windows
CONFIG(debug, debug|release):win32{
    DEPLOYED_LIBS += Qt5Cored.*

    contains(QT_DEPLOY_CONFIG, gui):DEPLOYED_LIBS +=   Qt5Guid.*  Qt5Widgetsd.* Qt5DBusd.* ## Dbus pour libxcb
    contains(QT_DEPLOY_CONFIG, svg):DEPLOYED_LIBS +=   Qt5Svgd.*
    contains(QT_DEPLOY_CONFIG, opengl):DEPLOYED_LIBS +=   Qt5OpenGLd.*
    contains(QT_DEPLOY_CONFIG, printsupport):DEPLOYED_LIBS +=   Qt5PrintSupportd.*
    contains(QT_DEPLOY_CONFIG, script):DEPLOYED_LIBS +=   Qt5Scriptd.*
    contains(QT_DEPLOY_CONFIG, network):DEPLOYED_LIBS +=   Qt5Networkd.*
    contains(QT_DEPLOY_CONFIG, dbus):DEPLOYED_LIBS +=   Qt5DBusd.*
    contains(QT_DEPLOY_CONFIG, xml):DEPLOYED_LIBS +=   Qt5Xmld.*
}


    unix{
        DEPLOYED_LIBS= $$join(DEPLOYED_LIBS," lib" , lib)
        DEPLOYED_LIBS=$$split(DEPLOYED_LIBS," ")
    }

    win32: DEPLOYED_LIBS+= libgcc*  libwinpthread* libstdc*



    #ajoute le path a chaque fichier
    unix:SRC_PATH="$$[QT_INSTALL_LIBS]/"
    win32:SRC_PATH="$$[QT_INSTALL_BINS]/"
    #message("SRC_PATH $$SRC_PATH")
    DEPLOYED_LIBS=$$join(DEPLOYED_LIBS," $$SRC_PATH" , $$SRC_PATH)
    DEPLOYED_LIBS=$$split(DEPLOYED_LIBS," ")
    #message($$DEPLOYED_LIBS)


    unix{
        #lecture du nom de la libstdc++ utilisée
        STD_LIB = $$system($$QMAKE_CC --print-file-name=libstdc++.so)
        #extraction du repertoire "canonique"
        STD_LIB = $$system(dirname `readlink -f  $$STD_LIB`)
        DEPLOYED_LIBS += $$STD_LIB/libstdc++*


        contains(QT_DEPLOY_CONFIG, gui):DEPLOYED_LIBS += /usr/lib/libX11-xcb.so*
    }

  #message($$DEPLOYED_LIBS)

    qtlibs.files = $$DEPLOYED_LIBS
    qtlibs.path = $$INSTALL_LIBDIR

    platform.files = $$[QT_INSTALL_PLUGINS]/platforms/*
    platform.path = $$INSTALL_LIBDIR/platforms

    imgfmt.files = $$[QT_INSTALL_PLUGINS]/imageformats/*
    imgfmt.path = $$INSTALL_LIBDIR/imageformats




   INSTALLS += qtlibs platform imgfmt


}
