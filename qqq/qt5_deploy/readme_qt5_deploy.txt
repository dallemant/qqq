permet de deployer les dependances � Qt dans INSTALL_RUNDIR/Libs

Comment utiliser:

Ajouter dans un *.pro de type subdir :
SUBDIRS += chemin/vers/qt5_deploy.pro

Cela suffit a ajouter la dependance a QtCore.

Dans depends.pri ajouter par exemple:

QT_DEPLOY_CONFIG = gui network

pour dependre des modules gui et network.

Enfin lancer la commande make install.
